---
layout: post
title: '[Rezension] Serenity Amber Carter: Mapmaker Malique – Iriliquium'
date: 2019-02-18
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Mapmaker-Malique.jpg", desc: 'Mapmaker Malique von Serenity A. Carter, vor einem Bücherregal'}]
---

Ich hatte im Oktober sehr viel mehr Glück als üblich – und habe ein Buch bei einer dieser wunderbaren Verlosungen über Twitter gewonnen! Ich habe mich sehr gefreut, denn ansonsten hätte es wohl noch etwas gedauert, bis ich mir „Mapmaker Malique: Iriliquium“ gekauft hätte. Auch so bin ich erst 2019 zum Lesen gekommen, aber das Warten war es wert!

Der Roman folgt Malique, einem 16-jährigen Kartenmacher nach seiner beendeten Ausbildung auf seiner ersten großen Reise durch die Galaxien. Malique erlebt dabei nicht nur einige unerwartete Dinge, sondern zeigt durch seinen Arbeitsalltag auch ganz nebenbei sowohl die Sci-Fi-Aspekte des Settings auf einer großen Raumstation und auf anderen Planeten als auch die Fantasy-Aspekte, als er mit einer Magierin zusammenarbeitet. 

Das Setting, die Idee dieser Kombination und die Umsetzung davon waren für mich klar ein großer Pluspunkt des Buches. Ich mochte und mag praktisch alles daran, wie die interstellare Zusammenarbeit aufgebaut ist, ich mag die Reaktionen auf gemeinsame Arbeitsumfelder und es gibt in diesem Buch eine Version des Internets. Finally. Alle Orte sind unglaublich immersiv beschrieben, ich könnte einige sofort zeichnen – und trotzdem wird dann doch alle paar Kapitel ein großes Sci-Fi-Trope einfach zerstört. So frustrierend das bei einigen Dingen für mich auch war, so sehr sollte es für das Buch an sich sprechen. Auch die Entwicklung der Figuren hat mir sehr gut gefallen. Anfängliche Schwierigkeiten wurden eben nicht sofort überwunden, sondern legten sich erst Stück für Stück und durch gemeinsame Erlebnisse. 

Ein paar Aspekte habe ich allerdings auch zu kritisieren. Ganz am Anfang kommen Maliques Mutter und sein Stiefvater kurz vor, und mir ist klar, dass sie unsympathisch sein sollen, aber das durch klassistische Beschreibungen zu regeln, hat für mich nicht funktioniert. Natürlich erzählt ein Jugendlicher, der da keine Weitsicht haben muss – aber ich glaube, das hätte man anders regeln können. Außerdem nervt Malique als Erzähler in seinen ersten Tagen auf der Raumstation doch sehr, wenn er jede einzelne Frau, die ihm begegnet, und auch seine beiden Teamkolleginnen Kryo und Eunuvea, durchgehend übel sexistisch beschreibt. Ich war froh, als da noch Character Development kam, der Bogen davon hat mir auch sehr gefallen, aber es war doch sehr viel an diesen Kommentaren und Gedanken. Der Erzählstil ist meistens spannend, fällt allerdings auch ab und an etwas in Umgangssprache ab – und teilweise allwissende Passagen haben mich eher verwirrt, als mir mehr Einblick zu geben.

Auch der Anfang des Zusammenwachsens als Team konnte mich nicht ganz überzeugen – Kryo und Eunuvea als (awesome) Teamkolleginnen von Malique gingen mir doch etwas zu schnell von „Oh, ist der niedlich und unerfahren“ zu „Er ist Teamleiter und wir folgen seinen Befehlen“ über. Dass sie dann noch regelmäßig (ausdrücklich nur deshalb) als toll beschrieben werden, weil sie sich nicht wie stereotypische Frauen verhalten, hat mich gestört. Ihre Handlungen an sich mochte ich aber sehr – abgesehen von zu viel eigenem Betonen auf die Außenwirkung und die eigene Unsicherheit fand ich die beiden genauso badass und bewundernswert, wie ich sie mir nach einem kurzen Blick auf die Website des Romans erhofft hatte. Und dass da auch noch ein größerer Plot um eine der beiden auftaucht, als man ihm am wenigsten erwartet, war natürlich auch toll!

Ich habe die Beschreibungen der anderen Planeten geliebt. Ein einziger bleibt mir als etwas seltsam in Erinnerung – er enthielt eine Art Liebeszauber, die wundersamerweise nur auf Männer wirkte und mit ästhetisch stark femininen Elementen wirkte. Gerade in einer Story, in der tatsächlich mal nicht nur heterosexuelle und -romantische Figuren vorkommen, hatte ich da noch auf etwas mehr Überraschung gehofft. Andere Planeten hielten aber das Versprechen von vielseitigen Terrains, Reaktionen der Figuren, tatsächlich auch mal etwas Arbeit (die mir für die Dreidimensionalität eines Romans doch recht wichtig ist) – und es gab Piraten! In Space! Und beinahe ein „Ihr seid meine letzte Hoffnung“-Zitat! (Stellt euch jetzt vor, wie ich das begeistert rufe.)

Auch das Ende war für mich ein sehr guter Aspekt dieses Romans. Die Handlung wurde auf mehreren Ebenen verdichtet, neue Figuren wurden eingebracht und gaben dem anfänglichen Setting noch eine höhere Ebene, und alle Fäden wurden zusammengeführt. 

Deshalb freue ich mich trotz ein paar Schwächen auf den nächsten Teil!

[[Hier](https://www.mapmaker-malique.de/#info) findet ihr die Content Notice, die die Autorin angibt, meine sind: Mobbing, psychischer Missbrauch, Gewalt, Gore; in der Erzählweise: Misogynie, Ableismus]

