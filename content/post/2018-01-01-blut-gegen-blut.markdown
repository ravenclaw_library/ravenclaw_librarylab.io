---
layout: post
title:  "[Rezension] Benjamin Spang: Blut gegen Blut"
date:   2001-01-01 09:42:14 -0300
categories: TischDerBibliothekarin
published: false
---

Ich mag Vampire. Und ich mag Horror. Und früher mochte ich es mal, mich extrem vom Twilight-Hype abzugrenzen und über glitzernde Vampire zu lachen. Dann fiel mir irgendwann auf, dass das eigentlich nur von meiner internalisierten Misogynie ausgelöst wurde, und hey, die Baseball-Szene bei Gewitter zu Muse ist schon ein echt tolles Setting, trotz der Platzierung im gefühlt meistgehassten Film des Universums. 

Schlagen wir einen Bogen zum Buch! Fazit – Vampire sind toll. Und auch wenn das Marketing von Benjaming Spang doch ziemlich in die Richtung davon geht, die eigenen Vampire von den ungeliebten glitzernden abzugrenzen – es sind halt immer noch coole, düstere Gestalten. Und dann gibt es auch noch coole Werwölfe, eine Feindschaft dazwischen, aber dazu einen politischen Plot daneben und eine Community von Menschen, die endlich mal etwas von diesen Wesen weiß, die da ihr Unwesen treiben. Und ein Steampunkt-Setting! Zeppeline! Das Worldbuilding ist damit schon ziemlich überzeugend, und ich habe mir schnell den ersten Band zugelegt, nachdem ich davon erfahren hatte.

Dazu kommen wundervolle Charaktere. Die beiden Hauptfiguren sind toll geschriebene Frauen (das geht tatsächlich, daran hatte ich nur noch begrenzt geglaubt, nachdem ich fast nur sexsüchtigen und übermäßig eifersüchtigen Figuren begegnet war, wenn Männer ausschließlich Frauen als Hauptfiguren besetzten), und ich hatte großen Spaß dabei, ihnen auf ihrem Weg durch den Plot zu folgen. Die Spannung entwickelt sich wirklich gut, das Mysterium der Herkunft von Katharina baut sich immer weiter auf und wird schließlich gekonnt aufgelöst. Dabei ist sie eine spannende Figur außerhalb von Klischees – als direkt als handwerklich begabt eingeführte Mechanikerin, die sichtlich in Strukturen von Maschinen denkt und Probleme auch sofort so löst, statt dauernd jemanden um Hilfe bitten zu müssen.

Die zweite Hauptfigur Helena bleibt eine starke Kriegerin ohne den im Fantasy-Genre schon fast üblichen plötzlichen Schwenk zur Sesshaftigkeit, hat keine Anfälle von Schwäche und muss sich auch nicht öfter retten lassen als alle anderen. Auch die Unterschiede zwischen dem Figurenaufbau haben die Interaktionen zwischen den beiden Hauptfiguren und das Buch für mich sehr lohnenswert gemacht. Nur beim Bösewicht wurde etwas in misogyne und Sexarbeiter*innen-feindliche Sprache verfallen, allerdings wurde diese Figur auch wirklich als unsympathisch dargestellt.

Die einzige Schwäche des Buches war für mich eine nicht ganz fließende Sprache: Es gab einige Formulierungen, die in Umgangssprache abglitten, und ein paar Dialoge, die noch besser geschliffen sein könnten und mich so etwas stolpern ließen. Aber ich habe den Roman nur einmal aus der Hand gelegt (ich musste aus einem Bus aussteigen) und dann nicht wieder losgelassen, bis ich es durchgelesen hatte. 

Falls ihr euch also auf Spannung in einem tollen Setting und geleitet von tollen weiblichen Figuren freuen wollt: Lest „Blut gegen Blut“! Ein Blick auf die Website und das Twitter-Profil des Autors lohnt sich auch und bietet auf verschiedenen Kanälen verbreitete Hintergrundinfos und Interviews, auch zur Crowdfunding-Aktion, die das Buch ermöglicht hat. Übrigens wurde die Aktion zum zweiten Buch gerade erfolgreich abgeschlossen. Ihr könnt euch also sogar auf eine Fortsetzung freuen.

