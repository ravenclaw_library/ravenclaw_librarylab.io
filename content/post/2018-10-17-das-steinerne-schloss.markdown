---
title: '[Rezension] Anna Weydt: Das steinerne Schloss'
date: 2018-10-17
tags: ["Bücher", "Rezensionen"]
bigimg: [
{src: "/img/Das-steinerne-schloss.jpg", desc: "Cover von Das steinerne Schloss, auf einem Kindle vor einem Bücherregal"},
{src: "/img/Das-Steinerne-Schloss-goodies.jpg", desc: "Cover von Das steinerne Schloss auf einem Kindle, neben einem Brief, der aus der Geschichte zu stammen scheint, einem Briefumschlag mit Siegel und einem passenden Lesezeichen"},
{src: "/img/Das-Steinerne-Schloss-Tweet.jpg", desc: "Tweet: Bild zeigt das Cover von Das steinerne Schloss vor einer Wand und neben einem passenden Lesezeichen, Text: Der Plan heute: lesen!"}
]
---

*Vielen Dank an die [Autorin](https://annaweydt.de/) für das Rezensionsexemplar!*

Ich hatte in den letzten Wochen die großartige Möglichkeit, mit meinem ersten Rezensionsexemplar zu arbeiten! Schon der Brief mit dem Download-Link war unglaublich schön gestaltet und mit Goodies versehen – alles sah wundervoll aus und hat mich sofort in die Welt der Geschichte hineingezogen. Das alles war so umwerfend, dass die ersten Wochen danach leider erst einmal nur exzessives begeistertes Anstarren des Covers auf meinem Kindle enthielten, bevor ich mich endlich an den Beginn gewagt habe. Aber dann sog mich die Geschichte auch schon ein!

Die Protagonistin Charlie stolpert zunächst relativ normal durch ihren Alltag – sie besucht ihren Großvater im Pflegeheim, begegnet dort anderen Menschen und ihrer unangenehmen Mutter, die sie nur über ihre nicht perfekten Leistungen im Studium zu definieren scheint. Dagegen ist Wilhelm, Charlies Großvater, ein ganz anderer Mensch. Die Figur wird sofort freundlich und zugewandt präsentiert, klar die einzige echte Bezugsperson von Charlie. Doch er warnt sie schon in seinem ersten Auftritt vor etwas, und seltsame Dinge nehmen ihren Lauf – und alles scheint davon abzuhängen, dass Charlie den von Wilhelm erhaltenen Talisman nicht um den Hals trägt.

Zunächst in schnellen Schritten, dann etwas langsamer entfaltet sich dann die Handlung in dieser Urban Fantasy. Dabei wird sich Zeit gelassen mit einer Einführung und Charlie nicht einfach aus einem nur der Vollständigkeit halber grob skizzierten Leben gerissen, sondern immer zwischen den Konflikten aus beiden Welten hin- und hergeworfen. Natürlich sind Dämonen, die nach dem eigenen Leben trachten, etwas wichtiger als der möglicherweise ausbleibende Unterhalt der Eltern im Studium. Durch die Verwicklung eines Dozenten in einen Kampf werden die beiden Thematiken aber erstaunlich intuitiv verbunden und bleiben das auch. Das hat mir sehr gut gefallen und war definitiv mal etwas anderes. 

Die Handlung folgt dabei teilweise schon einigen Tropes aus der Fantastik, durchbricht die meisten davon aber schnell wieder. Da ist Erik, der als typischer Bad Boy auftritt und eine Weile so bleibt, sich dann aber erstaunlich gut aus dieser Rolle löst und immer wieder anders erscheint – und außerdem einmal nicht nur für irgendjemanden nach der Auserwählten sucht, sondern selbst tief in den Hauptplot verwickelt ist. Da ist Wilhelm, der als Großvater zwar den Wink zum größeren Konflikt gibt, das aber nicht aus dem Jenseits heraus oder in einem Testament – und sich auch noch aktiv in die Handlung einbringt. Ein erster Flug von Charlie auf einem Greifen macht ihr keine Angst, sondern lässt sie das erste gute Gefühl genießen, das sie an einem katastrophalen Tag voller böser Überraschungen hat. Und das Worldbuilding… Jetzt würde ich spoilern, aber es ist *so* eine tolle Konstruktion!

Ein paar wenige Kritikpunkte sammelte das Buch trotzdem. Etwa in einer Szene am Anfang, in der Charlie unter einen Schlafzauber gestellt und dann von Erik durchsucht wird, was doch sehr übergriffig ist. In einem Dialog wird Charlie dann für eine Sexarbeiterin gehalten, was dazu führt, dass diese Möglichkeit als unglaublich schrecklich und die Bezeichnung als unfassbare Beleidigung dargestellt wird. Das war die genaue Bezeichnung zwar auch, aber das Konzept ist keine, und es war ein wirklich unnötiger Beigeschmack. Außerdem wird zwischendurch die Möglichkeit eines Verhältnisses zwischen Charlie und ihrem Dozenten angedeutet und es wird in keiner Weise reflektiert, was das für ein problematisches Gefälle in Privilegien der beiden zur Folge hätte.

Viele Aspekte haben mir aber auch extrem gut gefallen. Allem voran die Einbettung der Kapitel in Verse aus Konrad von Würzburgs „Der Welt Lohn“. Als Studentin in Deutscher Literatur habe ich die Dichtung gelesen und bearbeitet, und es hat schon anfangs großen Spaß gemacht, die Aspekte der Handlung zu entdecken, die sich mit Versen verbinden ließen. Dann wurde sogar noch aufgelöst, dass die Versdichtung in der Geschichte als Nacherzählung referenzierter Events vorkommt – das fand ich wirklich gut gemacht und eingebaut. Allerdings hätte ich Charlie da fast auch Deutsche Literatur studieren lassen und nicht Englisch – aber vielleicht wäre das auch zu unrealistisch perfekt gewesen.

Außerdem hatte ich großen Spaß an Referenzen zu Fandoms. Die kamen erstaunlich oft vor, und an gut gewählten Stellen. Auch Erik brachte sich damit gut ein, und Charlie bekam dadurch noch eine weitere Ebene an Glaubwürdigkeit. In einer Situation von großer Enttäuschung wurde Charlie dafür Raum gegeben – das fehlt mir an anderen Romanen oft und hat mich sehr gefreut. Und in einer Sexszene gab es expliziten, also mit einer Zustimmung ausgesprochenen Konsens! Das ist nun wirklich kaum in der Literatur zu finden und war eine wundervolle Überraschung! Und hey, die Welt erinnert an Supernatural und die Protagonistin heißt Charlie – Count me in!

Damit kann ich abschließend nur empfehlen, das Buch zu lesen, wenn ihr Urban Fantasy mögt. Und gerade, wenn euch die Grundstruktur der vielen Bestseller ansprechen, euch die unzähligen Klischees aber ganz schön nerven. 

[Content Notice für das Buch: Emotionaler Missbrauch durch Eltern, Sexarbeiter\*innen-Shaming, übergriffiges Verhalten (ungewollte nicht sexuelle Berührung)]

