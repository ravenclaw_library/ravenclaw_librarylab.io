---
layout: post
title:  '[Rezension] Maggie Stiefvater: The Scorpio Races'
date:   2018-04-22
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/TheScorpioRaces_cover.jpg", desc: 'The Scorpio Races von Maggie Stiefvater, vor einem Bücherregal'}]
---

Ich liebe Maggie Stiefvater.

Ihre Werwolf-Trilogie ("Shiver" bzw. "Nach dem Sommer" und die Folgebände) habe ich mit etwa 15 geschenkt bekommen und verschlungen. Dann bekam ich zum 18. Geburtstag den ersten Band ihres "Raven Cycles", "The Raven Boys" ("Wen der Rabe ruft") – ich habe so schnell mit Band zwei und drei aufgeholt, dass ich mit den anderen Lesenden in ein globales Fiebern auf den letzten Band hin geraten bin.

"The Scorpio Races" lag eine Weile herum. Nicht nur, weil ich nur spontan Faszination aufbringen konnte – der Klappentext sprach abgesehen von vage gehaltenen Gefahren eher von einem gewöhnlichen Pferdeabenteuer. Außerdem geriet ich nach einer Leseflaute durch viel Lesen im Studium in die nächste Flaute ohne Lesen an sich, als ich ein Semester lang nicht studierte und auch alles andere Papier liegen ließ.

Nach einem mühevollen Start ins Lesejahr 2018 war "The Scorpio Races" mitten im Juni der Versuch einer Versöhnung mit meinem Leseverhalten. Ich beschloss, dass Maggie Stiefvater mir selbst eine Pferdegeschichte verkaufen durfte. Und das Buch wurde eines der ersten, das mir keine Schwierigkeiten machte. Teilweise muss ich wieder ins Lesen hineingekommen sein, aber die Story ist trotzdem von Anfang an einfach wunderbar und mitreißend.
Es geht nämlich gar nicht um eine reine Pferdegeschichte.

Ja, die titelgebenden Scorpio Races sind ein Rennen, und bisher sind nur Männer dabei gewesen – und um ihre Familie (ihr Haus und ihr Pferd) zu retten, will ein Mädchen das Preisgeld gewinnen und meldet sich an. Ja, es gibt da noch diesen jungen Mann, der die Pferde der Reichen pflegt und sich eigentlich in eins verliebt hat, das er auch in den Rennen immer reitet, das sein Vater schon gerettet hat und das das schnellste von allen ist.

Seid ihr noch dabei? Sehr gut.
Denn die Pferde kommen aus dem Meer und sind blutrünstige Monster.

Das einzige gewöhnliche Pony, das überhaupt vorkommt, ist das der Protagonistin. Ansonsten ist die Story tief verschlungen in die Geschichte einer Insel, um die herum Monster leben, die aber gleichzeitig vom touristischen Highlight eines jährlichen Rennens auf eben diesen Monstern lebt. Alle Inselbewohner*innen sind irgendwie von all dem betroffen und machen etwas daraus, die Welt ist unglaublich dreidimensional gestaltet und wandelt sich sekundenschnell von der belebten Einkaufsstraße zum blutigen Strand.

Und auf einmal las ich über die typischen Sorgen von Figuren in einer Pferdegeschichte nicht einfach nur hinweg – die Charaktere machen sich nämlich auch immer wieder Sorgen um andere, größere Dinge. Diese verschiedenen Ebenen sind wunderbar ineinander verschlungen, und plötzlich saß ich da und las an einem Tag ein Buch einfach so weg. Etwas, das ich mir zu dem Zeitpunkt eher wieder in ein paar Jahren zutraute.

Maggie Stiefvater schreibt wunderschön. Lange Sätze voller Assoziationen, manchmal den Fokus wechselnd – dann wieder ein kurzer Akzent. Ich habe lange keinen Stil mehr gelesen, der mir so das Gefühl gibt, einer Musik zuzuhören. Dabei rutscht nichts ins übermäßig Ästhetische ab, das vom Inhalt ablenken würde. Es zieht die Lesenden einfach tiefer in die Handlung und die Welt hinein.

Ich kann nur eine dringende Leseempfehlung für dieses Buch geben. Es hat mich mitgerissen, zu Tränen gerührt und mal eben meine Ansprüche an Schreibästhetik verändert.

