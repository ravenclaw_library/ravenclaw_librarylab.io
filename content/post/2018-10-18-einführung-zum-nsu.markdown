---
title: 'Rechter Terror mit Unterstützung der Behörden: Eine Einführung zum NSU'
subtitle: 'Der NSU und der Prozess, Teil 1'
date: 2018-10-18
tags: ["Politik"]
bigimg: [{src: "/img/Einführung-NSU.jpg", desc: 'Schriftzug mit "Einführung zum NSU", vor einem Bücherregal'}, {src: "/img/nsu-watch-banner.jpg", desc: 'Schriftzug mit dem Titelthema der NSU-Watch: "Aufklären und Einmischen", auf deutsch und türkisch'}, {src: "/img/nsu-kein-schlussstrich-banner.jpg", desc: 'Schriftzug mit "Kein Schlussstrich", der Initiative für weitere Aufklärung des NSU-Terrors'}]
---

[Content Notice: Tod, Mord, grafische Beschreibungen von Morden (!), Terror, Nazis, Rassismus, Polizei, Suizid, Schusswaffen]

*****
Diese Rezension ist Teil meiner Artikelreihe zum NSU. Alle Artikel dieser Reihe findet ihr unten verlinkt.

Ich habe mich entschieden, den Schwerpunkt meiner Artikel auf den NSU zu legen. Die rechtsradikale Terrorzelle und vor allem der Umgang der Sicherheitsbehörden und Politik damit ist für mich ein Symptom davon, wie weit und in welchen relevanten, handlungsmächtigen Kreisen Rassismus und rechtes Denken vertreten ist. Und es ist nur eines, aber ein sehr eindrucksvolles Beispiel davon, was das für Folgen hat: tödliche.

Ich werde zunächst hier eine kurze Einführung zum NSU, dem Prozess und der Aufarbeitung geben. Dann folgt eine Rezension zu „Schmerzliche Heimat: Deutschland und der Mord an meinem Vater“ von Semiya Şimşek, der Tochter des ersten NSU-Opfers Enver Şimşek. Anschließend rezensiere ich „Kein Schlusswort. Nazi-Terror, Sicherheitsbehörden, Unterstützungsnetzwerk“, das die Abschlussplädoyers von vier Nebenkläger\*innen und acht Anwält\*innen der Nebenklage versammelt. 

*****

Der Nationalsozialistische Untergrund (NSU) hat sich diesen Namen selbst gegeben – in einem Bekennervideo vom 4. September 2011, das von Beate Zschäpe nach dem Abbrennen ihrer Wohnung verschickt wurde. Die beiden weiteren Mitglieder der Terrorzelle, Uwe Böhnhardt und Uwe Mundlos, hatten zu diesem Zeitpunkt nach einem gescheiterten Überfall gerade Suizid begangen. Zschäpe würde sich nach kurzer Flucht vier Tage später den Behörden stellen. Die Fahndung in großem Stil begann erst jetzt. 

Die drei Nazis hatte seit 1998 im Untergrund gelebt (unterstützt von einem riesigen Netzwerk aus der rechten und rechtsradikalen Szene) und Morde und Anschläge geplant. Sie verübten 43 Mordversuche, drei Anschläge mit Sprengstoff und 15 Raubüberfälle. Sie ermordeten zehn Menschen und es ist unklar, ob sie nicht auch weitere Morde begangen haben, die ihnen nicht zugerechnet werden.

Die rassistischen Einstellungen der Behörden arbeiteten dabei genau wie die Einbindung von vielen V-Leuten (vom Verfassungsschutz bezahlte Informanten, hier in der rechtsextremen Szene) der Terrorzelle zu: die Angehörigen der Opfer hatten schon in der Mitte der Mordserie dafür protestiert und demonstriert, eine Anerkennung der Mordserie als eben solche zu erreichen – stattdessen zeigten die Behörden weiter ihre problematischen Seiten und machten damit die weiteren Morde möglich.

*****

Am 11. September 2000 ermordete der NSU Enver Şimşek. 

Am 13. Juni 2001 ermordete der NSU Abdurrahim Özüdoğru. 

Am 27. Juli 2001 ermordete der NSU Süleyman Taşköprü. 

Am 29. August 2001 ermordete der NSU Habil Kılıç.

Am 25. Februar 2004 ermordete der NSU Mehmet Turgut.

Am 5. Juni 2005 ermordete der NSU Ismail Yaşar.

Am 15. Juni 2005 ermordete der NSU Theodoros Boulgarides.

Am 4. April 2006 ermordete der NSU Mehmet Kubaşık. 

Am 6. April 2006 ermordete der NSU Halit Yozgat. 

Am 25. April 2007 ermordete der NSU Michèle Kiesewetter. 

Und die ermittelnden Behörden haben das ermöglicht.

Dass diese Einschätzung keine übertrieben feindliche Haltung gegenüber diesen Behörden (Verfassungsschutz, Innenministerium, lokale Polizei) ist, zeigt die heutige Aufarbeitung in den Untersuchungsausschüssen und im Prozess. Es zeigt sich immer mehr, dass die Behörden in den Zuständigkeiten zerstritten waren und gerade der Verfassungsschutz in Schwierigkeiten geriet: weil sie lauter V-Leute bezahlten und schützten, die straffällig geworden waren und, anders als vom Verfassungsschutz geplant, gar nicht aus der rechtsextremen Szene aussteigen wollten, zahlten sie auf einmal den Lebensunterhalt gefährlicher Nazis.

Um das zu vertuschen, gaben sie einige Informationen gar nicht erst weiter, andere Behörden verwickelten sich untereinander – und die größer angelegte Fahndung nach dem 11. September 2001 beschäftigte die Ermittelnden nicht nur mehr, sondern führte auch zu einem neuen Stapel antimuslimischer Vorurteile. Denn auch wenn die Hinweise auf einen Zusammenhang hartnäckig nicht weitergegeben wurden: Es wurde auch sehr aktiv versucht, sie nicht zu sehen. Immerhin wurde erst ein bezahlter Geisterseher befragt, bevor den zu diesem Zeitpunkt schon demonstrierenden und protestierenden Angehörigen auch nur ein bisschen Glauben geschenkt wurde.

Auch der Prozess zeigte die Schwierigkeiten der Behörden, sich ihr eigenes Versagen einzugestehen: Obwohl hunderte Menschen selbst vernommen oder oft und eindeutig referenziert wurden, wurden die meisten schnell aus ihrer Verantwortung entlassen. Auch das gesprochene Urteil hat die (ohnehin schon viel zu wenigen) Mitangeklagten von Beate Zschäpe ungewöhnlich milde behandelt. Die These des isolierten Trios wird damit aufrechterhalten – angesichts der im Prozess und in den Untersuchungsausschüssen aufgedeckten riesigen Netzwerke der Nazis sollte sie aber unhaltbar sein.

Ich möchte in meinen Beiträgen einen Überblick darüber geben, weshalb ich den NSU für weitaus wichtiger als eine kleine und jetzt eben tote oder verurteilte Gruppe Serienmörder halte. Ich sehe den Komplex und vor allem den Prozess als ein Fenster in eine unangenehme Wahrheit an: Der NSU war nicht zu dritt. Wir haben ein Problem mit einem riesigen Netzwerk gewaltbereiter Nazis in Deutschland. Und wir müssen endlich etwas dagegen tun.

*****

Enver Şimşek war Blumenhändler mit einem Fachgeschäft mit Verteilung als Großhändler und mehreren mobilen Ständen. Seine Kinder Abdulkerim und Semiya waren zum Zeitpunkt der Tat 13 und 14 Jahre alt.

Abdurrahim Özüdoğru war Änderungsschneider. Er hatte das Geschäft nach der Trennung von seiner Frau übernommen. Seine Tochter war zum Zeitpunkt der Tat 19 Jahre alt. Sein Bild wurde in das Bekennervideo eingebaut und zum Teil eines grausamen Cartoons.

Süleyman Taşköprü war Lebensmittelhändler und leitete das eigene Geschäft. Sein Vater fand ihn noch lebendig. Er hinterließ eine drei Jahre alte Tochter. Seine Mutter starb, bevor überhaupt in Richtung von Nazis ermittelt wurde.

Habil Kılıç war eigentlich nur als Vertretung seiner Frau im gemeinsamen Laden. Die nächste Polizeistelle ist nur 100 Meter weiter. Weshalb nicht schneller Hilfe kam, ist bis heute nicht geklärt.

Mehmet Turgut war ebenfalls nur als Aushilfe im Imbiss – er wollte den Freunden helfen, die er in Rostock besuchte. Mehmet Turgut ist eines der Opfer, bei dem rekonstruiert ist, dass er auf jeden Fall noch mehrmals in den Kopf geschossen wurde, nachdem er schon am Boden lag.

Ismail Yaşar hatte sich mit seinem Imbiss selbstständig gemacht. Sein Sohn war zum Tatzeitpunkt 15 Jahre alt. Hier gab es eine Zeugin, die Uwe Mundlos und Uwe Böhnhard auf Überwachungsvideos wiedererkennt. Doch die Behörden ermittelten noch in Richtung eines Familiendramas.

Theodoros Boulgarides hatte sich erst vor knapp zwei Wochen mit einem Schlüsseldienst selbstständig gemacht. Dieser Mord verortete durch Indizien Beate Zschäpe als mitplanende und nicht nur mitlaufende Person im Netzwerk.

Mehmet Kubaşık war als Vertretung seiner Frau im Kiosk. Er war Kurde, seinem Asylantrag wurde stattgegeben. Seine drei Kinder waren 20, elf und sechs Jahre alt. 

Halit Yozgat war erst 21, als er in einem Internetcafé erschossen wurde. Niemand von den Gästen im selben Raum berichtete etwas Ungewöhnliches, auch nicht der anwesende Angestellte beim hessischen Amt für Verfassungsschutz. Obwohl dieser als rechtsextrem bekannt war und anders als andere Gäste das Café direkt nach der Tat verließ, wurden die Ermittlungen gegen ihn eingestellt.

Michèle Kiesewetter war ebenfalls als Vertretung für jemanden im Urlaub zum Dienst als Polizistin erschienen. Sie wurde erschossen, weil die Täter an ihre Ausrüstung herankommen wollten. Nach der Tat wurde zunächst lange eine weibliche Täterin gejagt, die sich als Mitarbeiterin der polizeilichen Forensik herausstellte. Weil sie weiblich war, wurde der Mord zunächst als Beziehungstat eingestuft. Weil sie weiß war, wird sie oft als „wichtigstes“ Opfer herausgestellt. 

Das Innenministerium listet die Morde des NSU bis heute nicht als politisch motiviert.

## Weiterlesen

### Weitere Artikel meiner Aktion

Teil 2: [Rezension zu „Schmerzliche Heimat. Deutschland und der Mord an meinem Vater.“](https://ravenclaw-library.berlin/politischeabteilung,/schreibtischderbibliothekarin/2018/10/18/schmerzliche-heimat.html)  
Teil 3: [Rezension zu „Kein Schlusswort. Nazi-Terror, Sicherheitsbehörden, Unterstützungsnetzwerk.“](https://ravenclaw-library.berlin/politischeabteilung,/schreibtischderbibliothekarin/2018/11/07/kein-schlusswort.html)  

### Weitere Artikel im Umfeld 
Eine [Rezension zu „Mit der Faust in die Welt schlagen“](https://skepsiswerke.de/mit-der-faust-in-die-welt-schlagen-lukas-rietzschel/) von Alisha  

### Weitere Informationen zum NSU
Website der [NSU Watch](https://www.nsu-watch.info/), ein Bündnis politischer Gruppen für Bildungsarbeit zum Komplex, das u.a. Prozesse, Untersuchungsausschüsse und Veranstaltungen begleitet und dokumentiert  
[Ein (erstaunlich kritischer) Überblick](http://www.bpb.de/politik/extremismus/rechtsextremismus/167684/der-nationalsozialistische-untergrund-nsu) der Bundeszentrale für politische Bildung

