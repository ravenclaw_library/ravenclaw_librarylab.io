---
title: '[Rezension] Liv Strömquist: Der Ursprung der Liebe'
date: 2018-12-17 15:42:14 -0300
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Der-Ursprung-der-Liebe.jpg", desc: 'Cover von Liv Strömquist: Der Ursprung der Liebe, vor einem Bücherregal'}]
---

[Content Notice: Sexuelle Übergriffe, Patriarchat, Antifeminismus]

*****

Ich habe dieses Jahr ein wunderbares Geburtstagsgeschenk bekommen, und weil es sehr unbekannt ist, muss ich euch unbedingt davon erzählen. Liv Strömquists „Der Ursprung der Liebe“ ist nichts von dem, was der Titel zunächst denken lässt: Statt bekannte Romantik darzustellen, dekonstruiert das Buch genau dieses Konzept nach allen Regeln und in allen Formen der Kunst. Dabei werden kulturelle Normen und ihre Entstehung und Entwicklung über die Jahrhunderte referenziert, die Beziehungen zwischen und Selbstbilder von Männern und Frauen, und ganz nebenbei wird die seltsame Dualität der Mono-Hetero-Hochzeit erklärt, die von Frauen oft gewollt und von Männern gefürchtet wird, obwohl es Frauen finanziell und emotional innerhalb dieser Ehen meistens schlechter und Männern besser als vorher geht. 

Das alles gibt es in spannend gezeichneten Comics – es sind keine perfekten Zeichnungen von Menschen nach allen Regeln des Mittelstufen-Kunstunterrichts, sondern viel menschlichere Formen der Protagonist\*innen. Die lernen wir auch gar nicht näher kennen, sie erleben einfach in einzelnen oder verbundenen Panels eine zwischenmenschliche Situation, in der eine gesellschaftliche Situation abgehandelt wird. Diese Direktheit der gezeigten Emotionen ist ein besonderer Einblick, er hat für mich noch viel mehr Schwung als eine schriftliche Behandlung des jeweiligen Themas. Die Comic-Ästhetik macht die Brutalität der Themen noch ganz anders zugänglich – Probleme von Beziehungen im Kontext von gesellschaftlichen Erwartungen, toxische Maskulinität und Trauer von Personen angesichts ihrer fehlenden Erfüllung der ihr gegenüber allgegenwärtigen Erwartungen, Konsensaushandlung und die Relevanz davon werden dadurch noch viel intensiver. 

Das Buch ist dadurch ganz schön hart und eines von denen, die ich nicht in einem Stück lesen kann. Aber wenn euch die Inhaltswarnungen unten nicht davon abbringen, kann ich es euch nur empfehlen – diese Art der Konfrontation mit gesellschaftlichen Normen und Problemen war für mich sehr eindrucksvoll und hat auch mir noch ein paar aktive Gedanken mit auf den Weg gegeben.  
Die einzigen negativen Punkte für mich sind fehlende Repräsentation von nichtbinären Menschen und die Tatsache, dass einige der Vergleiche, mit denen auf gesellschaftlichen Mist hingewiesen wird, rassistisch, ableistisch oder bodyshamend sind. Da würde ich mich definitiv über eine Überarbeitung freuen.

[CN fürs Buch: creepy Typen, Gewalt, dubious consent, Suizid, Tod; rassistische, ableistische, bodyshamende Sprache]

