---
title:  '[Rezension] Semiya Şimşek: Schmerzliche Heimat. Deutschland und der Mord an meinem Vater'
subtitle: 'Der NSU und der Prozess, Teil 2'
date:   2018-10-21 
tags: ["Politik", "Bücher", "Rezensionen"]
bigimg: [{src: "/img/Schmerzliche-Heimat.jpg", desc: 'Cover von "Schmerzliche Heimat" von Semiya Şimşek, vor einem Bücherregal'}]
---

[Content Notice: Tod, Mord, Terror, Nazis, Rassismus, Polizei, Suizid, Schusswaffen]

Diese Rezension ist Teil meiner Artikelreihe zum NSU. Alle Artikel dieser Reihe findet ihr unten verlinkt.

<center>*****</center>

Ich wusste, was mich erwartet. 

Ich beschäftige mich seit einem Jahr mit dem NSU und seit einem halben Jahr intensiver mit dem Prozess, den Beteiligten und der Aufarbeitung in den Untersuchungsausschüssen. Ich habe hunderte Seiten Bücher, Blogs und Interviews gelesen, ich saß in Untersuchungsausschüssen und ich war am Tag der Urteilsverkündung im Prozess in München. Ich habe Nazis und ihre Rechtfertigungen gehört, protestierende Angehörige gesehen und die Hilflosigkeit ihnen gegenüber von meiner Seite gespürt. Vor der schieren *Masse* an Fehlern in der Aufarbeitung, an verzerrendem und zerstörerischem Rassismus auf Seiten der Täter\*innen und Ermittelnden und an geschehenen und geplanten Grausamkeiten habe ich seitdem nicht mehr die Augen verschließen können. Und ich wusste auch, was ich für ein Buch zu erwarten habe. Ich dachte, ich wüsste alles über diesen Mordfall, was es zu wissen gibt.

Das Buch hat mich trotzdem umgehauen. Es hat mir den Boden weggezogen, mich in U-Bahnen in Tränen ausbrechen und in der Öffentlichkeit zusammenbrechen lassen. Dieses Buch tut so weh, dass es nicht einfach aufhört zu schmerzen, wenn man es aus der Hand legt. Es fühlte sich so schwer in meinem ansonsten normal gefüllten Rucksack an, dass ich Rückenschmerzen bekam. Ich musste es in der Hand tragen – nur so ließ sich die psychische Reaktion in Grenzen halten, mit der mein Kopf irgendwie versuchte, auf all die Gefühlen in mir zu reagieren. 

> Ich erinnere mich gut an den letzten gemeinsamen Urlaub in Salur, das war 1999. Einen Abend sehe ich noch ganz klar vor mir. Meine Mutter und mein Bruder schliefen schon. Mein Vater hatte die Obstbäume im Garten gegossen, dann ein paar Kirschen und Pfirsiche gepflückt und sich auf den Balkon gesetzt. Ich war schon auf meinem Zimmer gewesen […], aber ich war noch wach und bin nochmal raus, ich weiß nicht mehr, warum.
> Jedenfalls sah ich Vaters Silhouette im Dunkeln und setzte mich zu ihm. Ich fragte ihn, warum er nicht im Bett ist, ob er nicht schlafen kann, ob ihn irgendetwas beschäftigt oder bedrückt. Nein, sagte er und deutete auf den Lichtschein in der Ferne, auf das Feuer, das aus dem Dunkel der Berge leuchtete. Und er hat mir erklärt, dass die Hirten in dieser Nacht Feuer machen und miteinander essen. Dass sie danach wieder ins Tal kommen. Er wartete auf die Schafe, wie früher, und ganz leise, irgendwo in der Ferne, konnte man tatsächlich schon ihre Glöckchen hören. Mein Papa hat mir erzählt, was für ein besonderer Tag das für die Hirten ist, der Tag, an dem sie zurückkehren in ihr Heimatdorf. Dass er früher auch einer von ihnen gewesen ist und wie froh er an diesem Tag immer war. Und dass er eines Tages selber hierher zurückkommen wollte, nach Salur. Ich habe gespürt, wie glücklich er in diesem Moment war.
> Ein Jahr später haben sie ihn erschossen.

<center>*****</center>

Semiya Şimşek ist die Tochter des ersten Opfers des NSU, Enver Şimşek. Und so wie diesen Absatz erzählt sie das ganze Buch. Sie berichtet aus ihrer Kindheit, ihrer Jugend, dem Verhältnis zu ihren Eltern und deren Leben mit den aufwachsenden Kindern. Von der Arbeit ihrer Eltern, vom Wechsel ihres Vaters in den Blumengroßhandel, der ihn reich machen und dann an den Ort seines Mordes führen würde. Und der Schimmer des kommenden Mordes hängt nicht nur über diesen erzählenden Seiten, weil Lesende es wissen. Er hängt auch in den schmerzhaft genauen Beschreibungen jedes Moments von Semiya und ihrem Vater, der nicht von der Arbeit oder Sorgen darüber geprägt war. Er hängt in den Anmerkungen über die Kindheitsfreund\*innen der Autorin, die das Konzept von Rassismus noch nicht verinnerlicht hatten und selbstverständlich miteinander spielten. Er hängt in den ausdrücklichen Beschreibungen von Enver Şimşek als gutem Menschen, der genau das ablehnte, was ihm im Laufe der Ermittlungen noch vorgeworfen werden sollte: Glücksspiel und Kriminalität. Und dieser Punkt wird durch einige wenige Fälle von nicht hundertprozentig gesetzestreuen Aktionen für mich noch bestätigt: sie wirken wie bestätigend für manche Medienvertretende und Kommentare zum Prozess. Auch Kontakte zu Glücksspiel und Kriminalität hätten kein Grund sein dürfen, nicht auch in andere Richtungen zu ermitteln. 

Die Autorin erzählt so ruhig, dass die beschriebenen Situationen umso schrecklicher erscheinen. Mehrmals habe ich mich dabei erwischt, sie als tröstende Stimme wahrzunehmen – dabei ist sie diejenige, deren Vater ermordet wurde, und es ist meine Pflicht, gegen die Nazis zu arbeiten, die in großen Teilen immer noch unbestraft und völlig ohne Folgen ihrem Alltag nachgehen. 

<center>*****</center>

Der Mord verblasst bei den Erzählungen des sich neu bildenden Familienalltags ab und zu tatsächlich ein wenig – nur um dann wieder mit einem Knall zurückzukommen, wenn die Polizei doch wieder eine Frage hat. Semiya Şimşek beschreibt, wie ihre Mutter erst nach langem und sichtlich mit Tatverdacht geführten Verhör zu ihrem sterbenden Vater durfte. Sie betont, dass es ein Foto von ihr im Sprinter gab, in dem ihr Vater starb – und beschreibt, wie ihre Verwandten darin abgehört werden, trotz fehlender Genehmigung, weil die Behörden vom Tatverdacht einfach nicht ablassen wollten. Das erste Fax zur Information der Ermittelnden, das direkt nach dem Überbringen der Todesnachricht ein Vernehmen zum Tatverdacht vorsah. Der ignorierte Anruf des Bruders der Witwe, der auf ihren Gesundheitszustand hinwies und um etwas Zeit mit dem Überbringen der Nachricht wartete, bis er angekommen war – der Anruf wurde ignoriert, wenn nicht als Ausrede für einen Tatverdacht und härteres Vorgehen genutzt. Die schrecklichen Lügen der Autorin, die den Verwandten sagen musste, es gehe ihrem bereits gestorbenen Vater besser – damit sie die Nachricht erst bekamen, wenn die Autopsie den Leichnam freigegeben hatte. Die bandagierte Leiche, damit die Wunden nicht zu sehen waren. Das Fotoalbum der Familie, das kurz vorkommt, von der Polizei gestempelt und nur noch mit knappem Inhalt, große Teile der Bilder sind einfach nie zurückgegeben worden. Das Foto von Enver Şimşek, das seine Frau in einer besonders grausamen Befragung zerreißt, weil sie nicht mehr weiß, wohin mit ihren Gefühlen zwischen Liebe und eingeredeten Lügen.

Plötzlich eine Beschreibung, wie die Mutter der Autorin vor einem neuen Besuch der Polizei zu backen anfing: selbst bei so grauenvoller Behandlung würde sie ihre Gastfreundschaft nicht vernachlässigen. Dann eine noch grausamere Taktik der Polizei, die behauptete, Enver Şimşek hätte eine zweite Familie gehabt, damit seine Familie sich gegen ihn wandte und Geständnisse zu Sachverhalten ablegte, die so nie richtig waren. Und so viel mehr schreckliche Dinge, die ich hier gar nicht alle zitieren kann.

Mir war vor dem Lesen klar, dass die Behörden furchtbare, zerstörerische und menschenverachtende Arbeit geleistet haben. Seit dem Lesen weiß ich kaum, wohin mit all der Wut und Verzweiflung.

<center>*****</center>

Manches sticht heraus: Die plötzliche neue Information für mich, dass Enver Şimşek bei seinem Tod auch ein Vertreter für einen Bekannten war. Die Stimmung des letzten gemeinsamen Urlaubs in der Türkei, in den die Autorin nur mitgekommen war, weil ihr Vater es sich sehr wünschte. Die Rede, die Semiya Şimşek bei der ersten offiziellen Gedenkveranstaltung hielt. Sie stellte sich darin die Frage, ob sie in Deutschland noch zu Hause sein konnte. Inzwischen lebt sie in der Türkei, obwohl sie sich dort besonders anfangs ausdrücklich fremd fühlte. Die Beschreibung ihrer Hochzeit ist auch die Beschreibung eines Familienrituals mit einer Leerstelle.

Wie nebenbei stellt die Autorin die Frage in den Raum, wie ihr Vater sich bei der Ankunft im grauen Deutschland geführt haben mochte, und wirft dabei immer wieder kurz den Aspekt davon auf, dass ihr Vater vor allem zum Geld Verdienen in Deutschland war. Wenn dann noch kurz angerissen wird, wie er nicht immer hundertprozentig alle übertriebenen deutschen Vorgaben beim Verkauf seiner Blumen erfüllt hat, ist das keine Darstellung von einem Heiligen. Umso wichtiger ist es zu sehen, dass er es trotzdem nicht mehr oder weniger verdient hatte. Umso tragischer ist die Beschreibung seiner religiösen Läuterung, nach der er sich so sehr wie das Vorbild eines guten Menschen benahm, wie es praktisch möglich ist. Dabei entsteht durch seinen Tod kein größerer Verlust – aber dieser Wandel passierte so dermaßen mit Blick auf eine bessere Zukunft, dass dieser Aspekt genauso wehtut wie die Beschreibung des schon verkauften Blumenhandels, für mehr Zeit für die Familie, oder der immer wieder ausgesprochene Traum von der Rückkehr nach Salur, in das Dorf der eigenen Kindheit.

In der Erzählung von Semiya Şimşek wird vor allem klar, wie menschenverachtend die Ermittlung und jedes Narrativ darüber war. Nicht nur die Vorgehensweisen, auch die Bezeichnung „Halbmond“ der Ermittlungseinheiten, die Berichte über die „Döner-Morde“. Und genau an diese Medien wird die Autorin trotzdem verwiesen, als sie bei der Polizei nach den Berichten über den aufgeflogenen NSU nachfragt. Wie nebenbei lässt sie mehrfach einfließen, wie sie und ihre Familie das mögliche Motiv des Rassismus aufbrachten: sie wurden ignoriert. 

Das Buch erschien vor dem Ende des NSU-Prozesses. Die Hoffnung auf Bestrafung der Drahtziehenden im Hintergrund und einer weiteren Ermittlung gegen mehr als die drei Haupttäter\*innen liest sich heute wie ein tragischer Abschluss, obwohl der Absatz sichtlich hoffnungsvoll gemeint war. Was für ein Sarkasmus.

<center>*****</center> 

Die Beschreibung, wie Semiya Şimşeks Vater zum Blumenhandel kam, schmerzt besonders: 

> Mein Vater spürte schon seit längerem: Mit Blumen ließe sich in diesem grauen Land etwas erreichen.

Seit der Lektüre kommt mir Deutschland noch um einiges grauer vor als zuvor ohnehin schon.

<center>*****</center> 

Ich habe das Buch am Anfang viel in öffentlichen Verkehrsmitteln gelesen. Als ich einmal aus der U-Bahn kam und am Herrmannplatz umsteigen musste, ging ich in eine türkische Bäckerei mit Frühstückscafé, um mir etwas zu Essen zu kaufen. Vor mir in der Schlange warteten zwei Polizisten. Einer stieß den anderen an und freute sich, wie günstig das Essen war, „das bekommen die ja nur mit ihrer Vetternwirtschaft hin“.

Ich wünschte, ich hätte mich beschwert. Stattdessen brach ich heulend zusammen.

[Content Notice fürs Buch: Tod, Mord, grafische Beschreibungen von Morden (!), Terror, Nazis, Rassismus, Polizei, Suizid, Schusswaffen]

___________
erstes Zitat: S. 40  
zweites Zitat: S. 42

<center>*****</center> 

## Weiterlesen

### Weitere Artikel meiner Aktion

Teil 1: [Rechter Terror mit Unterstützung der Behörden: Eine Einführung zum NSU](https://ravenclaw-library.berlin/politischeabteilung/2018/10/18/einf%C3%BChrung-zum-nsu.html)  
Teil 3: [Rezension zu „Kein Schlusswort. Nazi-Terror, Sicherheitsbehörden, Unterstützungsnetzwerk.“](https://ravenclaw-library.berlin/politischeabteilung,/schreibtischderbibliothekarin/2018/11/07/kein-schlusswort.html)  

### Weitere Artikel im Umfeld  
Eine [Rezension zu „Mit der Faust in die Welt schlagen“](https://skepsiswerke.de/mit-der-faust-in-die-welt-schlagen-lukas-rietzschel/) von Alisha  

### Weitere Informationen zum NSU
Website der [NSU Watch](https://www.nsu-watch.info/), ein Bündnis politischer Gruppen für Bildungsarbeit zum Komplex, das u.a. Prozesse, Untersuchungsausschüsse und Veranstaltungen begleitet und dokumentiert  
[Ein (erstaunlich kritischer) Überblick](http://www.bpb.de/politik/extremismus/rechtsextremismus/167684/der-nationalsozialistische-untergrund-nsu) der Bundeszentrale für politische Bildung  

