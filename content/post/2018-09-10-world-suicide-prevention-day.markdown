---
layout: post
title: '[#WSPD18] Statt „Lächel doch mal“ – ein Leitfaden für das Umfeld depressiver und suizidaler Menschen'
date: 2018-09-10
Tags: ["Politik", "Lost and Found"]
bigimg: [{src: "/img/WSPD.jpg", desc: "Schriftzug: WSPD - World Suicide Prevention Day, vor Bücherregal"}, {src: "/img/world-suicide-prevention-day.jpg", desc: "Logo des World Suicide Prevention Day"}]
---

*Dieser Artikel ist ein Beitrag zur Aktion zum World Suicide Prevention Day (#WSPD18). Alle Beiträge zum #WSPD18 habe ich unten verlinkt.*

[Content Notice: Selbstmord/Suizid, Depression, Anxiety/Angststörung, Sucht, Selbstverletzung]

So. Der Blog ist noch nicht einmal ein paar Monate alt und ich haue euch schon um die Ohren, dass ich depressiv bin. Das passt auf den ersten Blick so gar nicht zum Konzept meines Blogs. Oder? Auf den zweiten Blick bemerkt man vielleicht doch etwas, dass ich Buchbesprechungen immer mit dem Hintergrund von Diskriminierung und Machtverhältnissen in der Gesellschaft verbinden will.  
Dazu gehören immer auch die Anerkennung von Tabuthemen und das Aufbrechen dieser Stigmata. Und psychische Krankheiten und Suizidalität gehören dazu. Wie genau meine Depression mein Leben beeinflusst, werde ich vielleicht ein anderes Mal in einem Beitrag beschreiben. Hier möchte ich mich auf ein Thema fokussieren, das ich schon lange vor diesem Blog im Hinterkopf hatte.

Eine Depression bringt das gesellschaftliche Stigma ihr gegenüber leider immer mit sich und dadurch ergeben sich schwierige oder beleidigende Situationen – ich werde mal nicht ernst genommen, mal verletzend stark in Watte gepackt oder mir wird vorgeschlagen, ich soll mich doch gleich in eine unfreiwillige stationäre Therapie begeben. Über solche Dinge spreche ich mit sehr nahen Menschen manchmal, und ich habe gelernt, dass viel davon aus Versehen passiert – aber auch aus Unwissenheit darüber, was denn ein guter Umgang sein könnte.  
Deshalb möchte ich gar nicht lange darüber schreiben, was ihr im Umgang mit depressiven Menschen unbedingt nicht sagen und niemals tun solltet, sondern was tatsächlich gut ist.  
Dabei spreche ich natürlich nur für mich. Ihr solltet immer mit Menschen in eurem Umfeld darüber reden, was für sie okay oder nicht okay, vielleicht sogar gut oder unterstützend ist. Aber ich möchte euch ein paar Vorschläge machen, mit denen ihr vielleicht anfangen oder die ihr auch als Vorschläge in ein Gespräch mit den depressiven Menschen in eurer Umgebung nutzen könnt.

### 1. Nehmt die Messlatte der Betroffenen an
Stellt euch vor, ihr lebt euer Leben, aber irgendjemand hat die Einstellungen auf „Profi“ gesetzt. Irgendwie habt ihr das Tutorial aus Versehen beendet, und jetzt steckt ihr hier drin. In vielen Hinsichten ist das ähnlich wie eine Depression. Alle um mich herum machen sofort ihre wichtigen Anrufe bei Behörden und müssen sich vielleicht vorher einmal zusammenreißen und Mut zusprechen, damit es funktioniert. Ich hatte Phasen, in denen ich wochenlang geweint habt, weil es einfach unmöglich war, den Anruf auch nur zu erwägen. Bestimmte Dinge, oft welche mit Kommunikation, Menschen und Konzentration, können uns extrem schwerfallen. Dazu kann auch „Aufstehen und dem Alltag nachgehen“ gehören.  
Das als „normal“ und „einfach“ abzustempeln, nervt, denn für uns kann es ein echter Erfolg sein. Es ist toll, Menschen im Umfeld zu haben, die sich dann mit uns freuen. Oder nicht enttäuscht sind, wenn es mal nicht geklappt hat, „obwohl es doch so einfach war“. Nehmt nicht einfach eure Messlatte als die einzige an. Hört uns zu, freut euch mit uns über Erfolge und akzeptiert Rückschläge, ohne von euch aus zu bewerten. 

### 2. Akzeptiert Schwankungen
Ich schreibe dauernd „manchmal“ und „an manchen Tagen“. Für viele depressive Menschen ändert sich das Level des Nebels um uns herum und die Energie für verschiedene Dinge relativ oft – mehrmals wöchentlich bis praktisch dauernd. Eine euphorische Zusage für eine Party am selben Abend kann dann schon mal wiederrufen werden – oder wir können trotz einer Absage noch fünf Minuten vor Beginn des Kinofilms doch noch schreiben und total motiviert auftauchen. Natürlich macht das Planungen nicht einfach. Das ist okay.  
Und manchmal muss dann trotzdem die Karte bezahlt werden oder es ist doch kein Platz mehr da. Aber beschwert euch bitte nicht über Absagen nach einer angekündigten Teilnahme (in meinem momentanen Umfeld ist „geht heute doch nicht“ eine valide Absage und ich wünschte, es wäre überall so). Und freut euch trotzdem, wenn wir eigentlich nicht dabei sein wollten und jetzt doch da sind. Dasselbe gilt für Level von Konzentration, Energie für Zeit mit Menschen und anderes (ein „aber gestern ging das doch noch total gut“ zu hören zu bekommen, ist ein Albtraum). Wenn ihr uns einfach zuhört und den Status Quo für diesen Moment akzeptiert, macht ihr es für uns viel einfacher und netter.

### 3. Muntert auf (Nein, nicht so, wie ihr denkt)
Ha! Jetzt denkt ihr bestimmt, endlich kommt der erwartete Punkt. Das gute alte Aufmuntern hilft doch! „Lächel doch mal mehr“ und „Hey, ich bin auch glücklich, lass dich anstecken“ könnte doch helfen! Leider muss ich euch da enttäuschen. Meistens führt es eher zu schlechten Gefühlen, vermittelt Unverständnis, fehlendes Einfühlungsvermögen und reiht sich in das Stigma davon ein, dass Depressive „ja eigentlich selbst schuld sind“. Also Nein, so meinte ich das nicht.  
Was ich aber empfehlen möchte, ist Aufmuntern bei Niederlagen. Das kann ein Ärgern darüber sein, dass ich schon wieder den Arzttermin nicht wahrgenommen habe, weil ich einfach nicht aufstehen konnte. Oder plötzliche Wut darüber, dass es so viel länger dauert und so verdammt intime Gespräche brauchen würde, bis ich einen anerkannten Nachteilsausgleich an der Uni auch nur beantragen könnte. Dann hilft mir Aufmuntern sehr – ein Erinnern an meine Erfolge, eine Umarmung (bitte nur bei Menschen, die Umarmungen auch generell mögen) oder ein Hinweis darauf, dass ich ein toller Mensch bin, auch wenn ich manche Dinge nicht (immer) schaffe.

### 4. Beobachtet Betroffene im Hinblick auf Nebenerscheinungen
Depressionen und besonders so starke, dass sie Suizidgedanken auslösen, kommen selten ohne Nebenerscheinungen aus. Manche entwickeln sich dabei erst über die Zeit hinweg. Sowohl Essstörungen als auch Suchtverhalten sind verbreitet, und Selbstverletzung ist ja auch etwas bekannter (das kann „Ritzen“/Cutting sein, aber auch gezieltes Suchen von Prügeleien, das Schlagen von Wänden oder aktiv unaufmerksames Verhalten in gefährlichen Situationen, zum Beispiel im Straßenverkehr).  
Wie ihr so etwas am besten ansprecht, könnte einen eigenen Blogpost füllen, aber oft gibt es Hinweise auf die Suche nach einem Gespräch oder Anspielungen der betroffenen Person, gerade wenn ihr euch nah seid. Wenn ihr dann vorher gut aufgepasst habt, könnt ihr direkt einsteigen und gezielt Hilfe anbieten, ohne dass sich Betroffene noch ewig erklären müssen. Und glaubt mir, das senkt die Hemmschwelle.

### 5. Bietet eure Hilfe an – aber messbar
Schlechtes Gewissen bei angenommener Hilfe und Angst vor Selbstzerstörung der Helfenden ist das eine – Fragen nach Hilfe in einem spezifischen Fall ist das andere. Es ist total schön, gesagt zu bekommen, dass Menschen „immer für mich da“ sind. Bei spezifischen Problemen um Hilfe bitten muss ich trotzdem. Auch da zahlt sich aufmerksames Zuhören aus.  
Ich beschwere mich über einen anstehenden Arzttermin und mache mir Sorgen, dass ich mich nicht dorthin traue? Es ist super, dann Begleitung angeboten zu bekommen (am besten nur einmal, denn nach einer Ablehnung aus Unsicherheit darüber, ob ich das wirklich so meinte oder nicht nur ein schlechtes Gewissen beim Annehmen hätte, nervt es – aber das ist bei vielen Depressiven verschieden, redet am besten darüber, wie sich die Person das wünscht).  
Mit spezifischer Hilfe könnt ihr viel bewegen und dabei gleichzeitig das Gefühl vermitteln, dass es auch für euch messbar und okay ist.

### 6. Behaltet euch selbst im Auge
Es ist ein gar nicht mal so seltener Fall, dass Unterstützende mit übertriebener Hilfe und vielen angebotenen Ohren für Depressive oder Suizidgefährdete selbst überarbeitet und überfordert sind. Sei es von krassen Erzählungen oder einfach davon, dass auch nach guten, „normalen“ Tagen zwangsläufig immer wieder neue Rückfälle eintreten.  
Als ehemalige Helfende, die sich überarbeitet hat, und als Depressive kann ich euch nur sagen, dass es beiden Seiten hilft, wenn ihr euch auch selbst im Auge behaltet. Ich habe unglaubliche Angst, Menschen in meinem Umfeld zu viel abzuverlangen – und es hilft mir sehr, wenn ich weiß, dass sie sich über Hilfsangebote informiert haben. Hotlines und Beratungsangebote gelten übrigens oft auch für Helfende, und teilweise gibt es auch Angebote, die sich direkt an Helfende richten. Bestimmt auch in eurer Nähe, und definitiv per E-Mail oder Chat erreichbar.  
Unten werde ich solche Angebote auch auflisten. 

### 7. Erzählt von egoistischen Beweggründen, nicht von altruistischen
Es ist verbreitet, ein noch viel schlechteres Gewissen zu haben, wenn Hilfe angenommen werden muss. Depressionen hauen gerne noch einmal darauf und erzeugen zusätzliche Schuldgefühle davon, dass man selbst „ja nur eine Last ist und dafür jetzt ja auch Beweise hat“. Ich habe oft Fragen an nahe Helfende gestellt und wollte wissen, ob es ihnen mit all dem wirklich gut geht.  
Ein „Ich will dir helfen, weil du es brauchst“ hilft zumindest mir dabei nur begrenzt. Die Depression kann daraus nur umso besser mehr schlechtes Gewissen stricken. Aber egoistische Beweggründe – und fast immer gibt es auch die – kann ich annehmen. Gegen „Ich liebe dich und möchte dich glücklich sehen“, „Du hast mir auch schon in Krisen geholfen und ich will dasselbe für dich tun“ oder „Ich finde es so awesome mit dir, wenn es dir gerade gut geht, ich will helfen, dass es bald wieder eine Weile so ist“ kommt selbst die Depression mit ihren Totschlagargumenten nicht mehr so gut an.

### 8. Nehmt Stellung zu wirklich schwarzem Humor
Ab und zu mal über die Möglichkeit von Selbstmord nachzudenken und gefühlt dauernd in Therapie darüber zu reden, verändert den Ton. Und wenn Betroffene davor schon wirklich schwarzem Humor anhingen, kommt gern mal etwas wirklich Düsteres dabei heraus – (bei mir) mir selbst gegenüber, niemals anderen. Zum Vergleich: Einer meiner aktuellen Lieblingswitze ist „Da habe ich mir aber echt ins eigene Fleisch geschnitten“ nach erfolgter Selbstverletzung.  
Für mich ist dabei wichtig, dass ihr auf euch aufpasst. Rede ich auf einmal generell über alle Betroffenen oder über andere Menschen in diesem Ton? Weist mich darauf hin. Noch wichtiger: Macht es euch fertig und könnt ihr damit nicht umgehen? Sagt es. Es ist ein verbreiteter Mechanismus zum Verarbeiten von schlimmen Dingen, aber wir wollen euch echt nicht damit verletzen oder an eigene schlimme Dinge erinnern. 

### 9. Verändert euer Narrativ über Depressionen und Betroffene
Depressionen sind einem unglaublichen gesellschaftlichen Stigma ausgesetzt. Betroffenen helfen könnt ihr deshalb auch in eurem eigenen Umfeld! Wenn es in den Nachrichten um den Suizid einer prominenten Person geht, wenn über die „ja nur stresskranke“ Kollegin gelästert wird oder es um „Männer, die hart sein müssen, nicht so schwächlich und depressiv“ geht – mischt euch ein.  
Fragt nach, woher diese Formulierung kommt und ob die Person das gerade wirklich so sagen will, und die meisten werden ganz schnell unangenehm berührt den Mund schließen. Und es hilft uns unglaublich. 

### 10. Sprecht über den Ernstfall
Zwischen Menschen mit Depressionen und suizidalen Gedanken gehen die Meinungen über das beste Handeln bei vermittelten Suizidabsicht extrem weit auseinander. Während einige unglaublich froh wären, wenn die Polizei gerufen und sie eventuell zwangseingewiesen würden, ist das für andere ein unglaublicher Albtraum. Damit ihr euch nicht auch noch darüber Sorgen machen müsst, sollte eine depressive Person aus eurem nahem Umfeld Suizidabsichten äußern – sprecht darüber, was die Person sich wünscht.  
Allerdings solltet ihr dann auch dringend auf euch selbst achten und im Zweifel andere Personen um Rat fragen, gerade wenn ihr mit der Situation nicht umgehen könnt (was völlig normal wäre). Auch hier noch einmal der Hinweis: Hotlines und Angebote für Menschen in Krisen gelten auch für kurzfristige Krisen und überfordernde Situationen und auch für euch, wenn ihr das Gefühl habt, sie könnten helfen!

### Fazit
Ich hoffe, ich konnte euch damit ein paar Ideen an die Hand geben. Und wie gesagt, sprecht mit euren depressiven Freund*innen und Verwandten darüber, was sie sich wünschen! Wir freuen uns, wenn ihr mit daran arbeiten wollt, dass wir eine angenehmere Zeit haben – und erst recht, wenn ihr schon ein paar Ideen mitbringt, die tatsächlich helfen könnten und nicht nur gut gemeint sind.


## Hilfsangebote

Ich stehe dem Nennen von Hilfsangeboten teilweise sehr kritisch gegenüber. Zu schnell kommt es mir als Leserin vor, die Nummern würden einfach hingeworfen, damit ich mich gefälligst selbst kümmern soll und die sendende Person ein gutes Gewissen hat und „raus ist“.  
Ich mache diese Liste, weil ich für euch eine Liste bieten will, die die Spezifika der einzelnen Angebote zeigt. Und ich mache sie nicht zuletzt, weil ich lange nichts von Angeboten außerhalb von telefonischen Hotlines wusste (die bei Anxiety für mich keine Option sind), und weil einige der Angebote auf beiden Listen mir selbst sehr geholfen haben. 

### Angebote für Depressive, Suizidgefährdete und Menschen in Krisen:
Telefonseelsorge  
Telefonische Beratung, aber auch E-Mail- und Chatberatung  
0800 1110111 (ev.), 0800 1110222 (rk.), 0800 1110333 (für Kinder/Jugendliche),  
[Website](www.telefonseelsorge.de)  

Kirchliche internationale Hotline  
englisch, 18-0 Uhr: 030 44010607  
russisch: 030 44010606

Muslimisches Seelsorgetelefon  
030 443509821

In Krisen und bei der Möglichkeit von Suizid könnt ihr immer auch in eine Notaufnahme gehen oder einen Krankenwagen rufen!

### Angebote für Helfende, Freunde und Angehörige: 
Kieler Fenster  
Organisation für psychisch kranke Menschen und ausdrücklich auch ihre Angehörigen  
persönliche Termine in Kiel, Telefon-, E-Mail- und Chatberatung auch unter Pseudonym, [hier](http://www.kieler-fenster.de/)

Telefonseelsorge  
Telefonische Beratung, aber auch E-Mail- und Chatberatung  
0800 1110111 (ev.), 0800 1110222 (rk.), 0800 1110333 (für Kinder/Jugendliche),  
[Website](www.telefonseelsorge.de)  

Anuas  
Hilfsorganisation für Angehörige von Mord-, Vermissten- und Suizidfällen:  
[Website](www.anuas.de)  

## Weiterlesen

*Hier findet ihr die weiteren Beiträge zum World Suicide Prevention Day (#WSPD18):*

Ein persönlicher und [erklärender Beitrag](https://bluesiren.de/index.php/2018/09/10/world-suicide-prevention-day-2018/) mit Übersicht zur Aktion von Babsi

Ein ebenfalls sehr persönlicher [Beitrag zu Antidepressiva](https://piranhapudel.de/antidepressiva-ein-persoenlicher-bericht-wspd2018/) von Cindy

Eine Rezension zu ["By the time you read this, I'll be dead" ](https://a-winterstory.blogspot.com/2018/09/by-time-you-read-this-ill-be-dead-julie.html) von Vivka

Ein Beitrag zu [Suizid in den Medien und zum Werther-Effekt](https://buechnerwald.wordpress.com/2018/09/10/suizid-in-den-medien-ein-blick-auf-den-umgang-mit-suizid-in-der-popkultur/) von Michelle

Eine Vorstellung der Organisation ["To write love on her arms"](https://skepsiswerke.de/wspd-to-write-love-on-her-arms/) von Laura

Kat stellt [sieben Fragen an eine Betroffene](https://katfromminasmorgul.com/wspd18-7-fragen-an-eine-betroffene/)

Jenny fragt sich, ob man [Selbstmord verhindern kann](https://coloredcube.de/2018/09/kannst-du-einen-selbstmord-verhindern/) (dringende Inhaltswarnung für Suizid und genaue Beschreibungen)

Eine größere [Übersicht über Hilfsangebote](https://buchstabenmagie.wordpress.com/2018/09/10/hilfsangebote-bei-depressionen/) von Nadine