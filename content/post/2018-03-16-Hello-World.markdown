---
layout: post
title: Hello World!
subtitle: Eine Tour durch die Bibliothek
date: 2018-03-16
tags: ["Persönliches", "Lost and Found"]
---

## (Eine Tour durch die Bibliothek)


Liebe Lesende,

willkommen auf meinem Blog! Schön, dass ihr den Weg hierher gefunden habt.
In dieser digitalen Bibliothek im weitesten Sinne findet ihr einige Abteilungen. Das Bücherregal ganz vorne stellt, aufgelistet und mit einem Weg zu den vorhandenen Rezensionen, alle von mir gelesenen Bücher aus. Auf dem Tisch der Bibliothekarin liegen Rezensionen zu Büchern ausgelegt, Berichte von Lesemonaten und über Reihen. In der Magischen Abteilung stehen Artikel zu verschiedenen Fandoms bereit – zu Filmen, Serien, Cosplay und zur Magie der gesamten Basis von Fans und all den Menschen, die sich von den Geschichten in bewegten Bildern einfangen lassen.

Die Politische Abteilung ist auf informative und auch mal persönlich gefärbte Artikel zur (aktuellen) politischen Lage ausgelegt – und enthält auch Artikel zu Dingen in der Gesellschaft, die die Bibliothekarin mehr oder weniger betreffen oder eine genauere Betrachtung lohnenswert machen. Für persönliche Texte solltet ihr die Biografische Abteilung aufsuchen. Dort warten Berichte von Veranstaltungen, kleine Einblicke mein Leben und auch mal Gedanken zu diesem Blog.

Schließlich findet ihr hier auch die Verbotene Abteilung – aber seid gewarnt, meine Rants dort enthalten eine stärkere Prise an Wut als andere Artikel, und meine persönliche Meinung färbt dort alles stark.
Wer nach weiteren Inhalten sucht, sollte sich zur LostAndFound-Kiste begeben. Was ihr dort findet, ist nie sicher – aber lasst euch überraschen, es könnte sich lohnen!

Wie in jeder Bibliothek gelten auch hier ein paar Hausregeln. Diese Regeln gelten für Kommentare und die Diskussionen auf diesem Blog sowie für die an die verlinkten Social-Media-Kanäle angeschlossenen Diskussionen. Verstöße dagegen stören das Klima und werden zum Löschen von Kommentaren sowie zum Melden oder Blockieren von Profilen führen.

In dieser Bibliothek herrscht nicht das geringste Verständnis für [Homo-](https://queer-lexikon.net/wp/2017/06/15/homofeindlichkeit/) oder [Bifeindlichkeit]( https://queer-lexikon.net/wp/2017/06/15/bifeindlichkeit/), Fremdenfeindlichkeit oder Rassismus, [Trans-](https://queer-lexikon.net/wp/trans/), [Inter-](https://queer-lexikon.net/wp/inter/) oder [Queerfeindlichkeit]( https://queer-lexikon.net/wp/2017/06/08/queer/), Sexismus oder [Ableismus]( http://www.genderinstitut-bremen.de/glossar/ableismus.html) sowie [Bodyshaming]( https://marshmallow-maedchen.de/blog/body-positivity/was-ist-body-shaming-definition/) (ich bilde Teile dieser Begriffe mit „-feindlichkeit“ statt „-phobie“, um die Phänomene von wirklich unfreiwilligen, psychisch bedingten Phobien abzugrenzen – hier eine [Erklärung](https://geschlechtsneutral.wordpress.com/2018/05/15/es-ist-gewalt-nicht-angst/comment-page-1/) dazu). Die Artikel auf diesem Blog werden gegendert und ich arbeite an [digitaler Barrierefreiheit]( https://www.einfach-teilhaben.de/DE/StdS/Ausb_Arbeit/Berufstaetigkeit/Berufstaetigkeit/Digitale%20Barrierefreiheit/digitaleBarrierefreiheit_node.html). Außerdem versuche ich, [Mono-](https://queer-lexikon.net/wp/tag/polyamouroes/) und [Heteronormativität]( http://queer-lexikon.net/doku.php?id=queer:heteronormativitaet) abzubauen.

Wenn ihr sprachliche Richtigstellungen oder Anregungen dazu habt, wie die Konzepte hier besser umgesetzt werden können, macht das mich sehr glücklich und die Bibliothek zu einem besseren Ort für alle. Wer die Konzepte allerdings für nicht sinnvoll oder mich für übertrieben (feministisch, politisch, unbequem) hält, sollte sich eine andere Bibliothek suchen.

Hier sollte gegenseitiger Austausch auf Basis eines freundlichen, toleranten Umgangs und gegenseitiger Unterstützung gelten – Make this place a magical place! 

Damit ist die geführte Tour beendet. Ich freue mich auf viele tolle Menschen, Besuchende und Lesende, in den Hallen und auf einen regen Austausch – und hoffe, dass ihr hier alle mindestens ein gutes Buch in den Regalen finden werdet.

