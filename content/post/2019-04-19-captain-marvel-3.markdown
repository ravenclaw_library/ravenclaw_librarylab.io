---
layout: post
title: 'Taten einer Superheldin'
subtitle: 'Captain Marvel, Teil 3: Carols Kräfte, Lehrfiguren und Gefühle als Stärke'
date: 2019-04-19
tags: ["Filme"]
bigimg: [{src: "/img/Captain-Marvel-1.jpg", desc: 'Anna vor einem Kinoplakat von Captain Marvel'}]
---

[Content Notice: große Spoiler für den Film! Für die, die ihn nicht kennen, habe ich [hier](https://ravenclaw-library.berlin/post/2019-03-14-captain-marvel-review/) ein Review ohne Spoiler verfasst]

*****
Wer über Captain Marvel schreibt, wird das Ausmaß der Kräfte von Carol Danvers, deren Entwicklung in der Anwendung und dadurch sowohl den Einfluss der Kree als auch den Übergang von Carol zur bewussten Heldin und Beschützerin des Planeten Erde nicht auslassen können. Daher will ich mir einen Artikel Zeit und Platz nehmen, um genau das näher anzusehen: Die Frage, wie ihre Kräfte mit Carol als Person verbunden sind, wie das sich entwickelt und sie auch verändert – und auch die ersten Aspekte davon, was das mit der Figur im Kontext einer feministischen Interpretation macht.

Dieser Artikel hat in den thematischen Ansätzen große Parallelen zum nächsten Artikel dieser Reihe: „Feminismus einer Heldin“. Ich habe an einem relativ willkürlich und hoffentlich zum Ausmaß der einzelnen Artikel passend eine Linie gezogen und werde hier schon aus dem Bezug zu Carols Kräften heraus auf die Beziehung zu Lehr- und Vorbildpersonen eingehen – und auf die Interaktionen und die Frage nach den Emotionen darin. Beides hat auch einen feministischen Aspekt, aber ich habe noch einige weitere Themen dazu, die ich dann im nächsten Artikel weiter betrachten werde.

#### Ausmaß der Kräfte, Ausmaß der Taten: With great power comes great responsibility

Hier geht es erst einmal um Carols Kräfte. Sie waren einer der am meisten diskutierten Aspekte des Films, besonders durch Typen (vor allem aus Prinzip und durch das bahnbrechende Erlebnis, mal nicht im Mittelpunkt der Welt, ähm, des Films zu stehen). Ich will auch gar nicht auf diese Kritik eingehen, aber das und viele andere Punkte in großen und kleineren Rezensionen zeigten, dass das Ausmaß der Kräfte von Carol ungewöhnlich schienen – genau wie die Verteilung gegenüber dem Pacing und Storytelling. 

Denn Carol hat diese Kräfte eben schon, als der Film beginnt: Sie wird als lernende Soldatin des Kree-Imperiums eingeführt, die die Chance erhält, sich durch ihre Kräfte zu beweisen. Diese Kräfte hat sie, auch das wird früh geklärt, durch die Kree erhalten, die sie gerettet und in ihren Reihen ausgenommen haben. Gleichzeitig hat sie schon zu Anfang des Films Träume, in denen sie anscheinend in ihrer Vergangenheit diese Kräfte bereits hat – aber das stellt sich ja auch nur als der erste Moment des Rätsels um ihre Herkunft heraus, das schließlich zum Hauptplot des Films wird, mit all seinen Auswirkungen. Es bleibt trotzdem dabei, dass die klassische Held\*innenreise auf den ersten Blick ausbleibt. Carol bekommt nicht plötzlich Kräfte und muss mühsam erlernen, sie anzuwenden, dabei Fehler machen, und erst dann kann sie ihren Charakter als Figur mit diesen Kräften in Einklang bringen, denn with great power comes great responsibility. Laut einigen Typen gibt es daher überhaupt keine Entwicklung in diesem Film. 

Well. Stellt sich heraus, dass diese Typen wohl einfach nicht genau genug hingesehen haben. Captain Marvel ist ein Film, der einen starken Beigeschmack durch Feminismus hat, der auch einen Teil der Handlung durch solche Zusätze unterstützt und daraus zieht – aber selbst wenn dieser Aspekt völlig übersehen wird, besteht eine verdammt durchdachte Entwicklung in der Handlung, in Carols Kräften und in ihrer Beziehung zu sich selbst und zu ihrem Umfeld. Carol hat diese Kräfte von Anfang an, und scheinbar weiß sie auch damit umzugehen. Doch spätestens im Endgame (ja, das Wort musste ich einfach einmal verwenden) des Films stellt sich heraus, dass sie eigentlich doch noch einmal neu lernen muss, wie sie diese Kräfte behauptet. Da ist es auf einmal egal, dass sie weiß, wie sie die Kräfte verwendet. Sie muss sie für sich selbst erobern und sich damit identifizieren – mit allen Pflichten und Möglichkeiten, die das mit sich bringt.

Und wisst ihr was? Bei all diesen Punkten habe ich noch nicht einmal angefangen zu erwähnen, dass Vers als Kree-Soldatin ja noch nicht einmal am Rand der Seite der Bösen steht, sondern in ihrem Mittelpunkt. Ihre langsame Suche nach der Wahrheit über den Krieg zwischen Kree und Skrulls bedeutet auch ein Eingestehen ihrer eigenen Schuld – die langsam und Stück für Stück passiert und eben genau durch die Schwere den Grundstein für ihre Motivation dazu legt, eine Heldin zu werden. Hier entstehen schnell Parallelen zu Tony Stark, aber ich bin sehr froh, dass Carol für die ersten Schritte weitaus weniger lang gebraucht hat. Then again, sie ist kein (weißer) cis hetero Typ. 

Aber ich glaube, langsam wird mein Punkt klar: Die zunächst bei einer unglaublich groben Betrachtung der Plotstruktur angenommene These einer fehlenden Handlungsentwicklung oder eines nur begrenzt ausgebildeten Spannungsbogens ist nicht haltbar, wenn sich ein bisschen mehr mit dem Film beschäftigt wird. Ich bin sogar der Meinung, dass durch die Kombination mehrerer Handlungen hier eine besonders dichte Plotstruktur mit durchdachten und einzigartigen Aspekten angefertigt wurde – die nicht zuletzt auch deshalb so besonders ist, weil sie ab und an durch ironische Brechung kurzzeitig aufgelöst wird.
(Und an die Typen: Eine einzelne Figur, die halt noch etwas stärker ist und die Grenzen von Macht und Stärke einfach noch etwas anhebt, um größere Kämpfe möglich und ausgeglichen zu machen, wollt ihr doch auch. Ihr seid nur sauer, dass das nicht länger Hulk ist, sondern – *gasp!* – eine Frau.)

#### Interaktionen: Powerful women can have feelings!

Ein wichtiger Punkt bei meiner persönlichen Einordnung von Frauenfiguren und auch bei der im größeren Rahmen ist es (hoffentlich), die Rolle der „starken Frauenfigur“ zu kritisieren. Das sind eben solche Frauenfiguren, die auch mal ein paar der Dinge tun können, die sonst Männerfiguren vorbehalten sind: Gewalt anwenden, Alkohol trinken, nicht dauernd emotionale oder unterstützende Arbeit leisten, nicht immer da sein – aber die kommen immer mit dem Zusatz, dass diese Figur nicht emotional sein darf. Denn sie ist ja *not like the other girls*.

Ansonsten wäre sie ja auch nicht wertvoll – all diese Systeme, die über das Einordnen von Frauen und Frauenfiguren funktionieren, funktionieren ja über die Abwertung von allem, das weiblich konnotiert ist. Wenn Männer(figuren) so etwas tun, werden sie sofort abgewertet. Wenn Frauenfiguren das tun, dann „kennen sie halt immerhin ihren Platz“ (und Abweichung wird bestraft). Das macht Carol zu einer Rebellin – die das kombinieren darf, was sonst nur exklusiv mit der passenden Schublade sein darf. Carol darf badass sein, manchmal schlecht gelaunt, sie darf aggressiv kämpfen (unvergessen die Kampfszene, in der sie einen Gegner anknurrt) – und sie darf eben gleichzeitig Zweifel haben, unsicher sein, sich an ihre Jugend erinnern und Maria in den Armen liegen. 

Sie darf einfach beides sein. Und – was mich bei Wonder Women ein bisschen geärgert hat – schwächt das nicht damit ab, dass am Ende eine Seite hervorgehoben wird. Die Carol, die sich am Ende auf ihre Mission aufmacht, ist zu allem bereit und selbstsicher genug, sich das einzugestehen. Aber sie ist auch ein bisschen wehmütig beim Gedanken, die Erde zu verlassen. Und sie darf beides sein, und das macht sie weder weniger badass noch ihre Emotionen weniger wertvoll. 

#### Lehrpersonen, die empowern, und solche, die unten halten 

Lehrpersonen finde ich in Geschichten über Held\*innen immer spannend. Sie treten oft am Rand auf, sind anfangs wichtig und verschwinden dann Stück für Stück, werden teilweise doch lang eingeführt, als mächtig charakterisiert und dann fast immer auf irgendeine Weise überflügelt. Lehrpersonen erzählen nicht immer nur Geschichten über die Held\*innen, die sie unterrichten und beeinflussen. Sie erzählen auch, was die Ideale in der Umgebung sind, wie damit umgegangen wird und wie sie üblicherweise erreicht werden. 

Dabei tauchen immer wieder zwei Arten von Lehrpersonen auf. Die unterstützende Lehrperson, die der Heldenfigur supportive gegenübersteht und eine Art Vorbildfunktion erfüllt – eine Lehrperson, die ein direktes Vorbild für die Entwicklung darstellt. Und die Lehrperson, die sich irgendwann als eigentliches Feindbild entpuppt und dadurch eine Entwicklung bei der Heldenfigur auslöst.  
Carol Danvers hat praktischerweise genau zwei Lehrfiguren, die genau diese beiden Rollen erfüllen: Mar-Vell als konstruktive Lehrfigur, die die Entwicklung von Carol auch so unterstützt, dass sie ihren eigenen Weg finden darf. Und Yon-Rogg (ja, natürlich ist es ein Typ und als solcher wird er auch im nächsten Teil noch einmal stärker betrachtet werden), als Lehrer, der sich spannenderweise zuerst aktiv als solch eine konstruktive Figur darstellt („I want you to be the best version of yourself“). Nur stellt sich dann eben Stück für Stück heraus, dass die beste Version von Carol die ist, die er für sich am besten nutzen kann. Die stärkste, aber auch die am besten zu kontrollierende Version, die sich an ihn als Vorbild und Retter hält und sich nicht daran erinnert, dass er es ist, der sie erst in die Lage versetzt hat, dem Tod überhaupt nahe zu kommen.

Carol reagiert durch die antichronologische Erzählstruktur des Films nicht linear und nacheinander auf diese beiden Lehrfiguren. Im Gegenteil, Yon-Rogg wird als zeitlich späterer Lehrer als erste Lehrfigur im Film eingeführt und Mar-Vell tritt als Gegenentwurf auf – obwohl eigentlich Yon-Rogg in Carols Leben der Gegenentwurf war. Es passt jedoch wunderbar zum Film: Immerhin war Mar-Vell in Carols und eben auch in unserer Welt die Ausnahme. Und neben einer Entscheidung, sich von etwas abzugrenzen, bedarf es eben meistens auch einer aktiven Vision, um Dinge wirklich zu verändern. 

Genau das sehen wir im dritten Akt des Films. Die Weichen dafür werden im zweiten gelegt, in dem die Lehrfiguren in aktiver Kommunikation und Bildern der Erinnerungen nebeneinanderstehen und miteinander konkurrieren. Auch die oben erwähnten Konzepte von ernst zu nehmenden Gefühlen werden hier wichtig. Nur durch diese beiden wichtigen Einflüsse kann sich überhaupt eine Mischung bilden, die die Entscheidung im dritten Akt unterstützt: With great power comes great responsibility. Und wir sind uns ja alle einig, dass Carol die, anscheinend auch nach Infinity War, sehr gut umsetzt. 


### Weiterlesen

Teil 1: [Spoilerfreies Review](https://ravenclaw-library.berlin/post/2019-03-14-captain-marvel-review/)  
Teil 2: [Bilder einer Superheldin](https://ravenclaw-library.berlin/post/2019-04-03-captain-marvel-2/)  
Teil 4: [Feminismus einer Superheldin](https://ravenclaw-library.berlin/post/2019-05-02-captain-marvel-4/)  
Teil 5: The Bigger Picture (in Arbeit)  
Captain-Marvel-Folge der [Hugvengers](https://soundcloud.com/user-345715248/folge-007-captain-marvel-review)  
Review auf [Alpakawolken](https://alpakawolken.de/film-review-captain-marvel/)  