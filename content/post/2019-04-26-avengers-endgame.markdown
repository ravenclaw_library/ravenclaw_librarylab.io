---
layout: post
title: 'Avengers: Endgame'
subtitle: 'Ein spoilerfreies Review'
date: 2019-04-26
tags: ["Filme", "Rezensionen"]
bigimg: [{src: "/img/galaxy.jpg", desc: 'Ansicht einer Galaxie in bunten Farben'}]
---
Dies ist ein spoilerfreies Review zu Avengers: Endgame. Allerdings wird es vermutlich zu Spoilern kommen, wenn ihr das MCU noch nicht bis dorthin gesehen habt. In diesem Fall müsst ihr wohl leider auch einen Bogen um dieses Review machen, um Spoiler ganz zu vermeiden – und euch etwas mit dem Aufholen beeilen, denn die Filme sind toll und ihr wollt Endgame doch noch im Kino erwischen! (Oder? Ansonsten habt ihr noch etwas Zeit, aber ihr verpasst etwas!)

*****

### Die Erwartungen 

Ich hatte einige Erwartungen an, Hoffnungen für und Ängste in Bezug auf Endgame. Zentrale Erwartungen waren vor allem eine tolle Weiterführung der Storyline (kein „Auslaufen“ ohne weiteren Bezug, keine einfache, auf der Hand liegende Lösung für den zentralen Konflikt) und etwas Zeit für die Figuren, vielleicht etwas mehr als in Infinity War. Hoffnungen waren mal etwas mehr Screentime für die Figuren unter den Avengers, die dann mal nicht weiße cis hetero Typen sind – und, hoch gegriffen, und anhand der Comics und aus dem Beschäftigen der [Hugvengers]( https://twitter.com/hugvengers) mit den Trailern, Pepper in einer Rescue Suit, Carol in einer wichtigeren Rolle und coole Interaktion zwischen ihr und Thor. Außerdem habe ich schon ein bisschen auf mehr Steve-Bucky-Content gehofft, und auf Valkyrie, die in Infinity War so überhaupt nicht vorkam und mir etwas fehlte. Und Ängste, die ich hatte… You know. Alle. Death. Destruction. 

### Die Stimmung

Der Film ist… wirklich gut. Ich neige dazu, Filme großer Franchises nach ihrer „Stimmung“ zu bewerten: Wie sehr fühlen sie sich nach diesem Franchise an und nach dem Gefühl, das ich suche? Dabei bin ich (natürlich) nicht die Person, die das Gefühl aus dem ersten Iron Man sucht oder eine Person, die sich beklagt, dass sich das Franchise geändert hat und jetzt nicht mehr nur noch ausschließlich die Geschichten weißer cis Typen erzählt. Ich suche nach Momenten, Musik, Einstellungen, Handlunge, Sätzen – alles, was mir beim Ansehen des Films das Gefühl gibt, zu Hause zu sein. Wenn sich der Film dann noch etwas von diesem Franchise abgrenzt – wie zum Beispiel The Winter Soldier –, dann bin ich vollends glücklich.

Endgame hat das für mich in großen Teilen geschafft. Auf die genaue Handlung und darauf, was die mit mir gemacht hat, werde ich erst im Review mit den Spoilern eingehen, und ich habe viel zu viel dazu zu sagen (Wahrscheinlich werde ich die Analyse wieder teilen müssen). Aber zur generellen Stimmung: Dieser Film ist definitiv ein Marvel-Film, und das schreibt er sich auch auf die Fahnen. Er hat alles, was er braucht, die ikonischen Bilder, die Figuren, die Musik (die Themes, die in den richtigen Momenten spielen, die Themes aus den vergangenen Filmen, die Feels). Der Film setzt das, was er hat, auch sehr gut und gekonnt um. Der Plot ist gut (was mich an einzelnen Stellen stört, erfahrt ihr im Review mit Spoilern), das Pacing angenehm. Und allein, so viele Figuren aus den anderen Filmen wiederzusehen, die lange davor und in Infinity War wenig Screentime hatten (Pepper! Happy! Clint!), macht einfach glücklich.

### Das Ergebnis

Der Film fühlte sich sehr wie ein Marvel-Film an. Aber eben auch nicht wie viel mehr. Ich glaube trotzdem, dass er für mich deshalb wichtig war, weil er eben so viel Figurenentwicklung in sich trägt und dabei auch sehr aktiv die Ära abschließt, die mich in den letzten Jahren begleitet hat. Ich kann euch deshalb nur sehr ans Herz legen, den Film zu schauen, wenn ihr ihn noch nicht gesehen habt: besonders, weil die tollen Momente einfach noch mehr Spaß machen, wenn ihr nicht gespoilert seid. Der Film verdient trotzdem leider eine Content Notice, die ihr unten findet. Die meisten davon hätten auch ohne Verluste in der Handlung oder Tiefe einfach ausfallen können, was mich den Film definitiv gleich ein ganzes Stück weniger mögen lässt.
Trotzdem: Ich saß in der Mitternachtspremiere, völlig frei von Spoilern, und ich habe mit dem Rest des Publikums gerufen, geschrien, aufgeschrien, geheult. Es gibt so viele Szenen zum Jubeln, plötzlichen Aufatmen, gemeinsamen Zusammenzucken. Und so viel Nostalgie. Wenn ihr den Rest des MCUs kennt, werden euch große Teile dieses Films wirklich sehr viel Spaß machen.

…Müsste ich Sterne vergeben, würde ich außerdem noch ein paar abziehen, weil ich im Kino zu schockiert war, um zu weinen. Dieser Film macht *fertig*. Aber ich glaube, das hat er sich eben auch schon auf die Fahnen geschrieben. Also: Geht ihn schauen, wenn ihr wisst, worauf ihr euch einlasst. *You’re in the Endgame now.*

*****

[Content Notice für den Film (abgesehen von den üblichen Cns für Marvel-Filme): übler Ableismus, Fatshaming, Klassismus, Shaming von Alkoholtrinkenden und -süchtigen, abusive Eltern [Referenz], übles Queerbaiting (*grr*)]
