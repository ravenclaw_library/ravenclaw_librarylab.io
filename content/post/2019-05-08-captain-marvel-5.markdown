---
layout: post
title: 'Zukunft einer Superheldin'
subtitle: 'Captain Marvel, Teil 5: Endgame, Ausblick, Pläne'
date: 2019-05-08
tags: ["Filme"]
bigimg: [{src: "/img/Captain-Marvel-1.jpg", desc: 'Anna vor einem Kinoplakat von Captain Marvel'}]
---

[CN Spoiler für Avengers: Endgame und Captain Marvel! Spoilerfreie Reviews gibt es [hier](https://ravenclaw-library.berlin/post/2019-04-26-avengers-endgame/) und [hier](https://ravenclaw-library.berlin/post/2019-03-14-captain-marvel-review/)]

*****

### Captain Marvel in Avengers: Endgame
Captain Marvel hat in „Avengers: Endgame“ dann doch endlich ihren Auftritt innerhalb einer größeren Gruppe Avengers. Sie ist bereits in einem Trailer zu sehen, dann gibt es in der After Credit Scene ihres eigenen Films die Gewissheit darüber, wie sie die Avengers treffen und die Geschehnisse in Infinity War aufnehmen wird. Dann ist sie eine Weile einfach dabei – motiviert die Avengers gar, endlich gegen Thanos vorzugehen – aber bringt damit wohl auch den frustrierendsten Antiklimax aller Filme ins Rollen, und bleibt fortan außerhalb der Erde, um dort irgendwelche heroischen Dinge zu tun, die nicht weiter benannt sind und nicht weiter wichtig werden (und das frustriert ja noch mehr als dieser Plot-Antiklimax, so ein Müll). Ihre Kräfte sind natürlich auch auf einmal viel weniger episch, stark oder übermächtig (laut Canon wären sie das nämlich ziemlich) – und gleichzeitig bekommen Typen große Momente, in denen sie plötzlich mehr Kräfte haben, als es nach Canon möglich wäre. Ein letzter Film für diese Typen und das entsprechende Publikum, bevor Carol dann als neue Figur im Zentrum der neuen Avengers ihren Auftritt hat? Leider bezweifle ich das.

Zu unsichtig haben sie sie in Endgame gemacht – immerhin haben wir bis zum Ende nicht ordentlich erfahren, was für Dinge sie denn jetzt wirklich auf ihren Missionen im weiteren Universum geregelt hat, wir haben nur ein paar Plattitüden. Stattdessen bekam Wanda einen großen Moment, und das ist wunderbar, wirklich. Die Reaktionen auf den großen Social-Media-Plattformen zeigen aber auch schon wieder, weshalb das für Marvel das kleinere Risiko war: Lauter Typen freuen sich, dass endlich eine Frau ins Rampenlicht rückt, mit der sie etwas anfangen können. Denn ihr Plot ist ja nur, um einen Mann zu trauern – Vision wird ja als aktiv männlich dargestellt – und deswegen sauer zu sein.

Carols Wut auf das Patriarchat und ihre Seitenhiebe gegenüber mächtigen Männern sind viel gefährlicher. Und ich will nicht anfangen, Frauenfiguren gegeneinander auszuspielen. Fakt ist, dass sie sowohl Wanda als auch Carol sehr viel schwächer gemacht haben als ihre Comic-Pendants (Comic-Wanda hätte mit Thanos gerade erst *angefangen*). Aber dass Marvel es bei dieser einen Szene beließ und uns ansonsten nur diese Girl Scene gab, in der eine einzige (wirklich coole) Collage darüber hinwegtäuschte, dass diese Figuren ansonsten im Film kaum vorkamen – enttäuschend, für alle Heldinnen in diesem Film (ganz abgesehen davon, dass sie die meisten umgebracht haben, aber Frauenfiguren in Endgame könnten einen eigenen Artikel füllen). Und eben auch für Carol, der mit diesem Film als Nebenprodukt von mehr Zeit für Tony definitiv nicht ausreichend Platz zu irgendeiner Charakterisierung abgesehen von der Starken Frauenfigur™ hatte – ein Trope, von dem ich eigentlich froh war, dass sie ihm entkommen schien.

### Ausblick für weitere Filme 
Aber das war ja nur Endgame. Wir haben eine neue Phase Marvel vor uns, und für Carol bedeutet das diverse weitere Auftritte in Filmen. Das bedeutet Ängste und Hoffnungen für diese weiteren Filme – Ängste nach der (plottechnischen) Umsetzung der Figur Captain Marvel in Endgame (denn die Ästhetik war super, women should do and look like whatever they want), Hoffnungen nach dem eigenen Film und dem Rückbezug zu den Comics und den Ursprüngen der Figur. 

Die bisher bestätigten Filme in Phase IV sind einige, in denen Carol wohl erst einmal nicht zu sehen sein wird. Aber es ist eben auch ihr zweiter Teil angekündigt: Und auch dafür habe ich natürlich einige Hoffnungen. Ich hoffe auf mehr Einbindung ihrer Vergangenheit auf der Erde und eine erneute Einbindung der Erde selbst – statt einem weiteren Plot, der ausschließlich in Outer Space spielt und sie weiter als gegenüber den Avengers als weniger relevant, auch für die eingeweihten Menschen auf der Erde, darstellen soll. Ich hoffe auf mehr Maria, und wenn sie sie altersbedingt rausschreiben sollten, zumindest auf mehr Monica. Und wenn es schon fernab der Erde spielen soll, dann wünsche ich mir eine Fortführung des großen Main Plots. Ich wünsche mir für Carol einen Plot, in dem sie Stück für Stück ihre Stärken und Schwächen nicht nur ausloten, sondern auch anwenden und überwinden kann. Einen Plot, wie es ihn für Tony und für Steve gab (und in Bezug auf Endgame wird Steves Arc mal berechtigterweise ignoriert), und der ausführlich und spannend ist. Der ihr noch mehr Hintergrund gibt und sie nicht einfach übermächtig in den Plot wirft und sie dann raushält, weil sie vermeintlich nur stören würde.

Ich wünsche mir für Carol eben das, was auch andere wichtige Avengers bekommen haben. Und ja, Nat bekommt auch einen Film, aber dieses bisschen Rückblick wird es auch nicht mehr richten. Carol sollte nicht nach einem guten Film ihre Reise beendet haben und dann nur noch Ausreden von weiteren Filmen bekommen, die eigentlich nur das Multiverse voranbringen. Carol sollte weiter im Mittelpunkt dieser Filme stehen – und spätestens Civil War sollte uns allen gezeigt haben, dass so etwas grundsätzlich möglich ist (auch wenn wir über die Umsetzung sicher diskutieren können). Ich habe große Hoffnungen für Carol, und gerade in ein erweitertes Multiverse, mit Inhumans und Avengers und Revengers und so vielen mehr, sollte sie so gut hineinpassen. Ich will einfach, dass sie ihren rechtmäßigen Platz in diesen neuen und kommenden Narrativen bekommt. Hoffen wir, dass das funktionieren wird.

Bis dahin könnt ihr euch eigentlich nur mit der Comic-Version von Captain Marvel beschäftigen – denn die sind größtenteils wirklich gut. Und eine echte Abwechslung zu den meisten anderen, gerade auch den frühen, Marvel-Comics. Ganz abgesehen davon, dass sie euch tausende wunderbare Headcanons für die kommenden Filme und das weitere MCU geben werden.

### Comic-Empfehlungen 

Zu guter Letzt möchte ich euch ein paar Comics ans Herz legen, die sich als cooler Einstieg in die Figur und das Universum darum eignen:  
- Kelly Sue DeConnick, David Lopez: Captain Marvel – Higher, Further, Faster (und die folgende Reihe)  
- Margaret Stohl, Carlos Pacheco: The Life Of Captain Marvel  
- Marvel's Captain Marvel Prelude  

Weitere tolle Comics und Bücher im Umfeld der Figur:  
- Marvel: Powers of a Girl  
- Captain Marvel: What makes a Hero  
- Liza Palmer: Captain Marvel – Higher, Further Faster (Roman)  
- Marvel: Captain Marvel – Monica Rambeau (Yess, in den Comics ist sie auch eingebunden und sehr powerful und cool!)  

Ich hoffe, ihr hattet Spaß an dieser Reihe, Spaß am weiteren Beschäftigen mit dem Film - und findet alle einen Comic oder einen Roman oder eine Gruppe von Mädels, die dieses awesome Captain-Marvel-Feeling für euch einfangen können!  

### Weiterlesen

Teil 1: [Spoilerfreies Review](https://ravenclaw-library.berlin/post/2019-03-14-captain-marvel-review/)  
Teil 2: [Bilder einer Superheldin](https://ravenclaw-library.berlin/post/2019-04-03-captain-marvel-2/)  
Teil 3: [Taten einer Superheldin](https://ravenclaw-library.berlin/post/2019-04-19-captain-marvel-3/)  
Teil 4: [Feminismus einer Superheldin](https://ravenclaw-library.berlin/post/2019-05-02-captain-marvel-4/)  
Captain-Marvel-Folge der [Hugvengers](https://soundcloud.com/user-345715248/folge-007-captain-marvel-review)  
Review auf [Alpakawolken](https://alpakawolken.de/film-review-captain-marvel/)  