---
title: '[Rezension] Raymond Feist: Der Lehrling des Magiers'
date: 2018-09-13
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Raymond-Feist-books.jpg", desc: 'zwei Bücher von Raymond Feist vor einem Bücherregal'}]
---

Über diese Fantasy-Reihe bin ich erst gestolpert, als sie mir jemand empfohlen und mir dann direkt die ersten beiden Bände geliehen hat. Aber anscheinend ist die Midkemia-Saga auch relativ verbreitet, zumindest meinen Recherchen von Goodreads aus zufolge. Ihr habt keine Ahnung und fragt euch, weshalb ihr diese Fantasy-Reihe über Krieg, Kampf und Magie auch noch lesen solltet? Take a seat.

Burgen, Adlige, Armeen und Fürstentümer in einem Königreich – alles schon gesehen. Magie und Machtkämpfe als Kombination sind inzwischen ebenso verbreitet. Aber diese Reihe hat einen besonderen Twist im Plot, der ihr prompt das High-Fantasy-Level abfallen lässt und sie zu einer Geschichte zwischen den Genres macht: die großen, gemeinsamen Feinde der Handelnden kommen – haltet euch fest – von einem anderen Planeten. 

Der Anfang zieht sich etwas und sammelt doch einige Klischees an, vor allem im Werdegang der Hauptfigur dieses ersten Buches. Doch wer durchhält, wird mit für das Setting erstaunlich dreidimensionalen Figuren, durchgeplotteten Intrigen und spannenden Verwicklungen belohnt. Perfekt sind die Bücher gerade auch in Bezug auf die weiblichen Figuren sicher nicht. Aber ich habe bisher nichts Vergleichbares gefunden, das so gut zu lesen ist und dabei zumindest solide Konzepte von modernen Figuren in einem doch sehr Fantasy-lastigen Setting anzubieten hat.

Die älteren Ausgaben sind nicht so ästhetisch aufgemacht, aber ich lege sie euch ausdrücklich ans Herz. Denn die Neuauflagen (die ich im ersten Band zum Vergleich auch gelesen habe) kann man sich leider wirklich sparen. Das Coverdesign ist moderner, aber es will mit einem wirklich schlechten Korrektorat bezahlt werden. Ständige Rechtschreibfehler, fehlende Buchstaben – und ohne Vorwarnung hätte ich nicht verstanden, dass zwei Schreibweisen eines Namens nicht absichtlich sind und doch eigentlich dieselbe Figur bezeichnen sollen. Das hat die Geschichte nicht verdient!

Oh, und: Holt euch direkt auch den zweiten Band. Das Ende ist ein ziemlicher Cliffhanger. 

[Content Notice für die Bücher (verdammtes Fantasy-Genre): Gewalt, Gore, Krieg, Posttraumatische Belastungsstörung, Zwangsheirat, Vergewaltigung, weitere sexualisierte Gewalt, Sexismus, Misogynie]
