---
layout: post
title: 'Bilder einer Superheldin'
subtitle: 'Captain Marvel, Teil 2: Kamera, Farben, Bilder'
date: 2019-04-03
tags: ["Filme"]
bigimg: [{src: "/img/Captain-Marvel-1.jpg", desc: 'Anna vor einem Kinoplakat von Captain Marvel'}]
---

[Content Notice: große Spoiler für den Film! Für die, die ihn nicht kennen, habe ich [hier](https://ravenclaw-library.berlin/post/2019-03-14-captain-marvel-review/) ein Review ohne Spoiler verfasst]

*****

Ich habe Captain Marvel so sehr gemocht, dass ich ihn heute zum sechsten und vermutlich letzten Mal im Kino sehen werde – denn es ist zu vermuten, dass er dann aus dem Programm fliegt. Während dieser Wochen wurde mir oft die Frage gestellt, was ich denn an diesem Film so mochte. Einen Teil der Antwort habe ich bereits im spoilerfreien Review beschrieben, mündlich habe ich mich auch schon im [Hugvengers](https://soundcloud.com/user-345715248)-Podcast ausgelassen. Aber ich werde trotzdem noch die Chance nutzen, mich so über diesen Film auszulassen, wie es sich für eine Literaturstudentin eigentlich gehört: In vielen, vielen Worten. Also: Handys bitte auf lautlos, der Vorhang wird sich gleich öffnen: Ich werde jetzt etwas fangirlen. Diesmal über Kamera, Bilder und Motive.

#### Kamera und Schnitt

Ich glaube, die Kamera und der Schnitt fielen mir erst bei meinem dritten oder vierten Screening richtig auf. Da dann aber ziemlich, spätestens, als ich anfing, mich mit einem der tollen Menschen, die mich begleitet haben, zu unterhalten. Ich habe festgestellt: Captain Marvel ist für mich extrem angenehm geschnitten. Abgesehen von Action-Szenen sind die Schnitte relativ langsam, sie werfen mich nicht ab und zu etwas raus wie bei anderen Avengers-Filmen, und die Übergänge von Szenen mit etwas Dunkelheit oder einer Winzigkeit früher einsetzender Musik haben für mich extrem gut funktioniert.

Die Kameraführung setzte auch das um, was mir schon generell beim Film auffiel und worauf ich noch in einem späteren Teil der Analyse zurückkommen werde: Der Film wurde nicht mit einem und für einen [male gaze](http://feminismus101.de/male-gaze/) [der erklärende Link ist leider [cissexistisch](https://www.trans-inter-beratungsstelle.de/begriffserklaerungen.html)] gedreht. Carol läuft einfach durch die Szenen, tut ihre Dinge und haut Leuten eine rein. Und wenn da mal eine Zeitlupe ist, dann folgt sie Dynamik und nicht Körperformen.  
(Erwähnte ich, dass ich diesen Film dafür liebe?)

#### Farben und Bilder

Im nächsten Absatz werde ich noch einmal kurz auf Orte zu sprechen kommen. Und die beeinflussen natürlich die vorherrschenden Farben des Films. Anfangs Raumschiffe und Space Battles in fast völliger Dunkelheit, und dann die Suche nach Vers‘ wahrer Identität als Carol Danvers in der Wüste voller heller Farben und ein paar schattigen Canyons. Die einzigen bunten Elemente sind schon fast die Zwischensequenz, in der Carol die Skrulls durch die Stadt jagt und dabei von SHIELD verfolgt wird – und dann Marias Haus und die eingerichtete Raumstation.  

Spätestens hier wird klar, dass die Farben auch immer eine Bedeutung haben: Die Wüste ist trostlos, aber Carol findet darin Maria, in deren Haus sie die wichtigen Informationen herausfindet. Die Raumstation bildet voll Farben auch der ersten Präsentation von Carol in ihrem leuchtenden Anzug und ihren leuchtenden, kompletten Kräften eine Bühne – der Showdown findet dann aber im dunklen All statt (und wird dann in der Wüste fortgesetzt, aber das ist ja noch viel mehr als die Konfrontation mit Ronan ein Antiklimax). Und Marias Haus ist mit all der bunten Einrichtung neben der ebenfalls bunten und leuchtenden Bar Pancho’s der Ort, in dem Carol ein Gefühl dafür entwickelt, dass sie an diese Orte gehört und dort ein emotionales Zuhause hatte und haben wird.

Die Wüste darum herum hat auch noch einen weiteren spannenden Aspekt. In einem Literaturseminar an der Uni haben wir einmal Orte und ihre Konnotationen behandelt und dabei in Orte und das unterschieden, was wir „Nicht-Orte“ genannt haben: Orte wie lange Straßen, Flughäfen, Bahnhöfe, an denen Menschen nur sind, um dort in Zukunft nicht mehr zu sein. Auch die Wüste, in der die zweite Hälfte von Captain Marvel spielt, ist so ein „Nicht-Ort“. Die Figuren fahren ewig auf Straßen, dabei passiert wenig Plot und viel Charakterentwicklung, Carol findet Erinnerungen durch Bilder, die im Film auch genauso gezeigt werden – nicht nur die ausgedruckten Fotos, sondern auch die Eindrücke dieser Gegend. Die Wüste stellt den Grund unter dem Anfang der Kampfszenen in der Luft dar, und gleichzeitig den Parcours, in dem er mit Anspielung auf die Vergangenheit von Carol und Maria beendet wird. Und sie wird zum Boden, auf dem Yon-Rogg sie zum finalen Kampf gegen sich, ihren Mentor, auffordert (und den sie dann erstmal ignoriert, weil wichtigere Dinge warten). Gleichzeitig gibt es keinen einzigen Plot, in dem die Wüste aktiv wichtig wird, zum Beispiel durch Wassermangel. Sie bietet einfach den Grund für eine intensive Auseinandersetzung von Carol mit sich selbst und eine Zentralisierung auf ein paar wichtige Figuren, die sich am Ende des Films fast in eine Art Kammerspiel verstricken.  

Die gleichbleibenden Farben bilden auch eine Leinwand für die Veränderungen um sich herum. Carol sticht besonders in ihren späten, helleren Farben sehr aus dieser Wüste heraus. Sie lässt nicht nur die Bodenhaftung hinter sich, als sie anfängt zu fliegen. Sie lässt auch die alte Carol hinter sich, die sich unbemerkt in diese Welt einpasste und zwar ihren Weg in einer männerdominierten Welt fand, aber immer noch nach ihren Regeln spielte. Captain Marvel tut das nicht länger. Und wenn Carol diese Entscheidung allein in der Wüste fällt und sich als leuchtende Superheldin akzeptiert und ihrer echten Aufgabe stellt, ist das noch viel mächtiger als in der Mitte einer Großstadt. Nachdem sie sich in genau dieser Wüste lange durchschlagen musste, weil die Regeln des Patriarchats sie nicht für ihre Ziele zuließen, fällt besonders im Vergleich mit den Rückblicken noch mehr auf, wie sehr jetzt alles um Carol herum nach neuen Regeln spielt – nach ihren. 

### Weiterhören (Musik aus dem Film):

Hätte ich noch mehr Zeit im Leben, hätte ich noch mehr Recherche zur Musik im Film eingearbeitet. So müsst ihr euch damit begnügen, dass ich mich hier noch einmal schrifltich freue, wie viele female Vocals im Hintergrund spielen – und dass sie gerade auch die Kampfszenen und spannenden Momente unterstützen, nicht nur die emotionalen. Für Begeisterte, die wie ich die Songs grob erkannten, aber definitiv nicht zuordnen konnten: Ich kann diverse Spotify- und Youtube-Listen für die allgemeine Zusammenstellung empfehlen. Einziger Nachteil der großen Listen: Da kommt auch viel vor, das im Film oft nur ganz kurz im Hintergrund angespielt wird. 

Die ikonischen Songs:  
- Elastica: Connection (spielt in der Szene, in der Carol das Motorrad nimmt und losfährt)  
- Garbage: Only Happy When It Rains (spielt in der Szene, in der Carol mit dem Motorrad aufs Pancho’s zu fährt und während sie durch die Bar geht und sich erinnert)  
- No Doubt: Just A Girl (spielt in der Kampfszene gegen Carols altes Team)  
- Hole: Celebrity Skin (spielt im Abspann)

### Weiterlesen

Teil 1: [Spoilerfreies Review](https://ravenclaw-library.berlin/post/2019-03-14-captain-marvel-review/)  
Teil 3: [Taten einer Superheldin](https://ravenclaw-library.berlin/post/2019-04-19-captain-marvel-3/)  
Teil 4: [Feminismus einer Superheldin](https://ravenclaw-library.berlin/post/2019-05-02-captain-marvel-4/)  
Teil 5: The Bigger Picture (in Arbeit)  
Captain-Marvel-Folge der [Hugvengers](https://soundcloud.com/user-345715248/folge-007-captain-marvel-review)  
Review auf [Alpakawolken](https://alpakawolken.de/film-review-captain-marvel/)  