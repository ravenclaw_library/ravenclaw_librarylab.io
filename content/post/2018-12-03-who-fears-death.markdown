---
layout: post
title: '[Rezension] Nnedi Okorafor: Who Fears Death'
date: 2018-12-03
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Who-fears-Death.jpg", desc: 'Cover von Nnedi Okorafor: Who Fears Death, vor einem Bücherregal'}]
---

[Content Notice für die Rezension: Nennung von Vergewaltigung, Genitalverstümmelung, Steinigung, Völkermord, Rassismus. Eine Content Notice für das Buch gibt es unten, dabei ergeben sich geringe Spoiler.]

*****

Das Leseverhalten von den meisten von uns beschränkt sich auf einen sehr speziellen Kanon.
Wir lesen Geschichten über weiße cis Protagonist*innen in monogamen Hetero-Pairings, die irgendwo in Europa oder Nordamerika in von Weißen geprägten Gesellschaften spielen. Frauen dürfen höchstens mal Protagonistin sein, Heldenfiguren bleiben männlich, und alles erklären und alles wissen müssen sie sowieso.

Who fears Death bricht daher mit vielen dieser Lesegewohnheiten – auch von mir. Es ist das erste Buch, das ich von einer Liste aus dem Black History Month tatsächlich gekauft habe. Über den Black History Month bin ich auf Twitter bei [Mari](https://twitter.com/CrowandKraken) gestolpert und habe der Aktion nachrecherchiert. Neben dem Anliegen, die Geschichte von BPoC darzustellen (gerade neben der bekannten Geschichte der kolonialisierenden Weißen) kam gerade in der Buchcommunity auch die Forderung durch, besonders weiße Lesende sollten einfach mal den eigenen Kanon erweitern und nicht nur innerhalb der eigenen bekannten Komfortzone lesen, sondern auch die Bücher von People of Colour. Auch dabei gibt es viele, die in den oben genannten Regionen spielen – alltäglicher Rassismus ist aber dabei oft ein Thema für die Protagonist\*innen, so wie er es zwangsläufig auch für die Autor\*innen ist.

Who fears Death fällt für manche Kritiker\*innen unter Afrofuturistik, die Autorin Nnedi Okorafor distanziert sich davon allerdings etwas (ich kann ihren Twitter-Account sehr empfehlen). Klar ist, das Setting ist dystopisch, nimmt daneben aber Sagen und kulturelle Einflüsse auf.
Das stört für europäisch-westlich aufgewachsende Lesende schon mal den Lesefluss. Ich habe noch nie beim Lesen eines Buches so oft Begriffe nachgeschlagen, die selbstverständlich eingeworfen wurden – Begriffe von Essen, Kleidern, Sagengestalten. Das Buch ist auf jeden Fall auch ohne diese Recherche verständlich, es macht also keine „Arbeit“. Für mich war es aber wichtig, meine Wissenslücken zu füllen – auch, um meine Vorstellungen von den einzelnen Szenen dreidimensionaler zu machen.

Denn die Handlung ist mitreißend und spannend, ich konnte mich beim Lesen oft kaum von den Seiten lösen. Gleichzeitig sind viele Themen so schwer, dass ich es ab und zu musste. Gleich zu Anfang gibt es eine längere Beschreibung der Vergewaltigung der Mutter der Protagonistin. Und obwohl es definitiv handlungsrelevant und auch im fiktiven Kontext klar in einen wichtigen historischen Kontext auf der Ebene der Lesenden tritt (immerhin ist es immer noch eine verbreitete Art der Kriegsführung in den Bürgerkriegen in der Region), haut es einfach um. Daneben kommt es zu einer Szene von Genitalverstümmelung, und die Folgen ziehen sich über das gesamte Buch. Außerdem werden Steinigungen zumindest referenziert, und auch Versklavung ist nicht unbedingt mein einfachstes Thema. Das Buch bietet praktisch keine Verschnaufpause, und das will es auch nicht.

Im Gegenteil, es lässt nicht locker – Rassismus wird immer wieder benannt, um dann doch nicht durchbrochen zu werden, Sexismus ist für die Protagonistin und ihre Freundinnen allgegenwärtig, auch noch weit in die Heldenreise hinein, und gerade hier hat sie die Protagonistin auch viel davon verinnerlicht. Ihre Beziehung ist ein toller Mensch, er hilft ihnen und unterstützt sie – und doch hat auch er oft starke Meinungen dazu, was (junge) Frauen (nicht) tun sollten. Machtgefälle äußern sich in jedem Handeln jeder einzelnen Figur – und immer wieder muss sich die Protagonistin selbst in dieser Welt verorten und sich ihre Handlungsmöglichkeiten erst erkämpfen.

Das Buch schockiert. Doch daneben erzählt es eine unglaublich spannende Geschichte, eine klassische Heldenreise, die doch moderner nicht sein könnte – und die Protagonistin schlägt sich tapfer durch all ihre Rückschläge. Nichts davon kam mir vor wie sinnlose Schockeffekte. Die Gesamtheit der benannten und beschriebenen Grausamkeiten schockiert, aber alles wird in die Geschichte verwoben – und hat ja auch leider Vorbilder.

Damit ist Who Fears Death für mich eine klare Leseempfehlung für alle, die einer dreidimensionalen Heldin auf ihrer magischen Reise folgen wollen – aber dabei durchaus etwas abkönnen, was die Grausamkeiten auf dem Weg angeht.

*****

[Content Notice für das Buch - geringer Spoiler: Beschreibung von Vergewaltigung, Genitalverstümmelung, Steinigung, Völkermord, Rassismus, Missbrauch durch Eltern]


