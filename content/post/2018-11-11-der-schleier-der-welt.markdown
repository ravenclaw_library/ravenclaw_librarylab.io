---
layout: post
title: "[Rezension] R.A. Prum und S.C. Kreuer: Der Schleier der Welt"
date: 2018-11-11
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Der-Schleier-der-Welt.jpg", desc: 'Buch "Der Schleier der Welt", vor einem Bücherregal'}]
---

*Vielen, vielen Dank an die beiden Autor\*innen für das Rezensionsexemplar! Ich habe mich sehr, sehr gefreut und freue mich auch immer noch unglaublich über den Austausch mit euch!*

*****

„Der Schleier der Welt“ hätte mich wahrscheinlich auch sehr interessiert, wenn ich nur im Laden darüber gestolpert wäre. Werwölfe, eine sarkastische und schlagfertige Privatdetektivin und dazu mal ein kleiner Hintergrund der Protagonistin, die nicht völlig losgelöst in eine magische Welt übersiedeln kann? Es klang von Anfang an super! Allerdings musste ich gar nicht im Laden darüber stolpern, sondern hatte das Glück, durch den sehr aktiven Twitter-Account einer mitwirkenden Person am halben Entstehungsprozess teilhaben zu dürfen! Dabei habe ich mich ein bisschen so gefühlt, als könnte ich auch der Geschichte selbst und den Charakteren beim Wachsen und Handeln zusehen – und es wäre für mich keine Frage gewesen, dass ich das Buch kaufen möchte. Die beiden Schreibenden dann beim Literaturcamp Hamburg zu treffen und den Roman gar von ihnen als Rezensionsexemplar zu bekommen, war dadurch unglaublich wundervoll! (Ich freue mich immer noch ein bisschen durch die Gegend^^)

Schon die Einführung in die Welt hat mich sehr begeistert – das Tempo passte mir gut, ich habe mich weder gelangweilt noch überfordert gefühlt und konnte auch neue und in Gruppen auftretende Figuren gut auseinanderhalten. Die Protagonistin Kyra in ihrem Alltag als Privatdetektivin zu erleben und dabei eine wirklich durchsetzungsstarke und sarkastische Figur zu erleben, die einmal kein Typ sein muss, hat mich sofort an eine weniger düstere Variante von Jessica Jones erinnert; diese Story funktioniert aber auch ohne die ganz düsteren Themen sehr gut! 

Durch die Arbeit von Kyra als Privatdetektivin hat sich natürlich eine tolle Art der Einführung in die üblichen Rätsel des Übernatürlichen ergeben: anders als in anderen Romanen gibt es keinen Infodump und auch keinen einzigen Punkt, an dem etwas klar wird. Stattdessen werden Lesende in einen wahren Krimi verwickelt und jagen mit Kyra nicht nur Informationen nach, sondern auch dem Versuch des Balancierens des eigenen Wissens und den Daten, die doch immer mehr auf eine übernatürliche Erklärung hindeuten. Dabei hat sich Kyra charakterlich kaum verändert, sondern blieb glaubwürdig, wunderbar wenig bescheiden und harmoniebedürftig – und einfach wunderbar, um ihr durch diese Welt zu folgen.

Auch andere Punkte haben mich wirklich begeistert: Etwa die völlig unbetonte Einführung einer Exfreundin von Kyra, ohne dass noch seitenlang eine Identitätskrise beschrieben wird (ohne die queere Figuren viel zu selten auskommen) – und auch ihre Rolle als wichtige Beschafferin von Informationen hat zwar auch Punkte im Plot wirklich gut erfüllt, aber auch die Diskussion der Beziehung zwischen Ex-Partnerpersonen zu einer neuen Ebene geführt. Diese beiden Ebenen nebeneinander zu sehen und zu lesen, hat großen Spaß gemacht – nicht zuletzt, weil auch Molly eine tolle weibliche Figur ist und weder zu sehr ins Klischee der übermäßig toughen, fast gefühlslosen Frau fällt, die auf Frauen steht, noch als zu beschützendes Attachée von Kyra auftritt.

Wirklich gefreut haben mich auch kleine Fandom-Referenzen (die ihr selbst suchen müsst, das macht sonst keinen Spaß), Repräsentation von Polyamorie (und so gelungen!) – und die Geschwindigkeit der Handlung. Es gibt lauter fiese Cliffhanger, aber keine von denen, die Lesende verständnislos zurücklassen. Im Gegenteil, die Motivation zum Weiterlesen steigt nur immer weiter: Ich habe das Buch einmal im ersten Viertel aus der Hand gelegt, und dann noch nicht einmal angehalten, um mir Notizen für diese Rezension zu machen.

Ein wichtiger Kritikpunkt für mich ist – leider – der Verlag. Das Buch ist beim Bundeslurch Verlag des Bundesamtes für magische Wesen erschienen, und ich bin kein großer Fan dieses Vereins. Dabei kommen bei mir einige Kritikpunkte zusammen, u.a. -feindlichkeiten verschiedenster Art in der Kommunikation auf Social-Media-Kanälen.

Das ist aber auch mein einziger Kritikpunkt. Auf inhaltlicher Ebene war ich sehr begeistert und freue mich auch immer noch über diese einmalige Möglichkeit, die Entstehung des Buches etwas zu verfolgen und dann das Buch lesen und sogar mit den Schreibenden diskutieren zu können!

Ich kann das Buch damit allen Lesenden auf der Suche nach guter Fantasy in einem modernen Setting empfehlen, gerade auch mit mal etwas anderem als Vampiren. 

*****

[Content Notice für das Buch: Gewalt, Gore, Waffen, Polizei]
