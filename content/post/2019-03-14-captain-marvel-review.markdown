---
layout: post
title: '[Review] Higher, further, faster?'
subtitle: 'Captain Marvel, Teil 1: Spoilerfreies Review'
date: 2019-03-14
tags: ["Filme", "Rezensionen"]
bigimg: [{src: "/img/Captain-Marvel-1.jpg", desc: 'Anna vor einem Kinoplakat von Captain Marvel'}]
---
Captain Marvel ist seit einer Woche in den Kinos. Der 21. Film im Marvel Cinematic Universe ist der erste mit einer weiblichen Superheldin in der Titelrolle. Dass das alles viel zu lange gedauert hat, ist eine andere Geschichte, und weiter unten gehe ich auch noch darauf ein, was mir trotz der erfreulich modernen Umsetzung an diesem Film gefehlt hat. Aber: Ich will hier einmal in relativ kurzer Form über diesen Film fangirlen, den ich bisher immerhin dreimal gesehen habe – und euch nebenbei davon überzeugen, dass er sich lohnt.

Ich schreibe hier ein spoilerfreies Review, werde aber auf einige Aspekte eingehen, die ich toll umgesetzt finde. Dass diese vorkommen, wird aus den Trailern klar. Diese könnten also gespoilert werden.

### Die moderne Geschichte einer Heldin

Captain Marvel orientiert sich relativ nah am Canon aus den Comics, ändert aber einige zentrale Punkte aus dem bisherigen Canon. Einige davon sind Überraschungen im Plot, die ich hier nicht vorwegnehmen will. Aber auch die kleinen Dinge sind für mich entscheidend: Carol Danvers trägt ein viel weniger sexualisiertes Kostüm, sie wird erstaunlich oft mit Sexismus konfrontiert – und es fallen viele verbreitete antifeministische Phrasen, die es so in den Comics natürlich auch nicht gibt. Außerdem sind die Bezüge zur US Air Force erstaunlich gering geblieben und konzentrieren sich auf einen Forschungskontext in Zusammenarbeit mit der NASA. Der Plot ist dicht gewebt, verbindet eine solide Backstory etwas antizyklisch, aber fundiert mit dem direkten Problem im Plot und arbeitet dabei eine große Zahl toller Figuren heraus. Ja, das sind keine Männer. Die sind teilweise ganz cool, aber dann eben der Support. Die Rollen gibt es eben auch. (Außerdem kann mir niemand erzählen, dass die Figur Nick Fury nicht schon einen dreidimensionalen Charakter hatte, der hier gar nicht weiter aktiv ausgearbeitet werden musste – der wurde bewusst so und über Unterschiede zu seinem späteren typischen Charakter im MCU eingeführt und funktioniert genauso gut.)

Die Hintergründe von Carols Superkräften werden klar, es gibt logische Abfolgen und dabei immer wieder Überraschungen, wenn Zuschauende automatisch bestimmte Plotelemente erwarten (was ich sehr genossen habe). Und: Das alles wird in ein Setting der 90er verpackt, in dem der größte Feind auch mal das ausfallende Modem im Internet-Café sein kann. Das führt zu herrlichen Momenten der Nerd-Nostalgie, und dass die einmal im Leben in einem Setting ohne ständige Misogynie in Film und Kamera stattfinden, ist einfach awesome! 

### Mehr als eine Prise Feminismus

Ja, der Film thematisiert Feminismus. Darüber, dass auch fast alle anderen Marvel-Filme politisch sind, sind wir uns hier hoffentlich einig, deshalb werde ich darauf nicht weiter eingehen. Kurz aber noch dazu, wie gut der Film Feminismus thematisiert. Für die länger in einem anderen Kontext unterwegs gewesene Carol ergibt sich ein toller Plot daraus, miesen Sexismus und das Unterschätzen von Frauen als vermeintlich neue Erfahrung kennenzulernen – und sie dann in ihrem eigenen, bekannten Umfeld in anderen Formen wiederzufinden. Außerdem erfolgt die Umsetzung von Reaktionen auf diese miesen Kommentare wirklich gut. Ich war lange daran gewöhnt, mich über tolle Comebacks zu freuen, wenn Frauenfiguren auf der Leinwand mit Sexismus und miesen Kommentaren konfrontiert waren. Aber Carol hat darauf eine ganz andere Reaktion und damit auch endlich eine, die nicht wieder am Ende das Patriarchat bedient: Sie ignoriert den Mist, wenn ihr die Typen egal sind – und wenn es Feinde sind, haut sie ihnen eben eine rein. Weil sie eine spezielle Reaktion oder ein Eingehen auf ihren Müll gar nicht verdient haben. Es ist so wunderbar, das zu sehen!

Und: Der Film hat wirklich kaum noch male gaze. Es gibt eine große Kampfszene, die Carol in ihrem Anzug führt, aber ohne ihre Stiefel. Sie brüllt jemanden an, nicht hoch, nicht sexy, einfach wütend. Und ihr Helm lässt zwar ihre Haare frei, aber definitiv nicht so, dass sie frei über ihre Schulter wehen können, sondern in einer Art Iro stehen bleiben. Wenn sie ihre Kräfte einsetzt, schießt sie aus ausgestreckten Armen, nicht aus sexy Posen. Und sie macht Sprünge, Rollen und Drehungen auf dem schnellsten Weg und nicht dem elegantesten, und ohne dass die Kamera dabei noch dreimal jede Bewegung ihrer Oberschenkel mitnehmen muss. Es macht einfach Spaß, sich diesen Film anzusehen, weil nicht dauernd dieser ewige unterschwellige Ärger über so etwas aufkommt.

### Schwächen des Films 

Captain Marvel wird von allen Seiten fortschrittlich und dafür eben awesome genannt. Und ja, er zeigt besonders anhand der miesen Reaktionen aus der Online-Community auf, dass er verdammt wichtig ist. Alleine die Idee, dass auf einer so großen Bühne und Bildfläche die Geschichte einer Superheldin erzählt werden soll, scheint manche Typen kaputtzumachen. Das klingt nicht nach einem großen Verlust, aber es ist krass zu sehen, wie viele es sind. Doch auch dabei, beim Konzept, einem klar umgesetzten female gaze – der Film hat ein paar Schwächen. Und besonders darauf bezogen, dass sich Marvel hier wissentlich mit dem Nerd-Bros, den Fans der ersten Stunde, angelegt hat, hätten sie diese hier wirklich auch noch umgehen und rausschreiben können. 

So ist der Film übel cissexitisch, sowohl in Bemerkungen der Unterschiede zwischen Frauen und Männern, die an Körperteilen festgemacht werden und so teilweise eine Punchline ergeben. Und auch wenn Carol zwei coole PoC Supporters/sidekicks hat, sollte nicht darüber hinweg gesehen werden, dass besonders Maria hier eine weniger „starke“ Rolle hat als viele andere Sidekicks. Sie ist eine tolle Figur, und ihre Rolle als kickass Pilotin und alleinerziehende Mutter ist gut umgesetzt. Zumal diese Mutter eine sehr coole, feministische Tochter aufzieht, die auch mal Fury darin zurechtweist, dass er ruhig Großes von ihr erwarten kann. Trotzdem wäre es toll gewesen, wenn sie Carol auch noch etwas mehr zur Seite gesetzt hätte als nur emotionale Arbeit. Es wird schnell klar, dass beide schon lange Freundinnen sind, was es etwas logischer und weniger problematisch für mich macht – aber da war wirklich noch viel Potential. Auch darin, dass hier eben weiterhin nur cis Personen vorkommen und von mehr als zwei Geschlechtern nie die Rede ist.

### Fazit

Ich kann den Film sehr empfehlen, und habe damit auch schon eine Person begeistert, die Superheld*innen-Filme oft sehr eintönig findet. Wenn ihr unsicher seid, gerade auch, weil ihr schlechte Reviews gelesen habt: Macht euch klar, dass viele der Reviewer sauer sind und den Film schlecht machen wollen – viele Typen aber den Film auch einfach deshalb schlecht bewerten, weil sie nicht daran gewöhnt sind, sich aktiv in eine Figur hineinversetzen zu müssen und dass es einmal nicht um eine Lebensrealität wie ihre geht. 

Daher: Wenn meine Kritikpunkte und die folgende Content Notice euch nicht davon abhalten, geht diesen Film schauen. Die Einnahmen für das Opening Weekend waren schon cool und haben gezeigt, dass der Film langfristig auf hohen Positionen stehen und wichtig sein wird. Das verdient er. Und jetzt muss ich Schluss machen, denn für die größere anstehende Analyse muss ich den noch ein paar Mal sehen.
*****
[Content Notice für den Film: Cissexismus in der Erzählweise, Sexismus in der Handlung (dem begegnet wird), Flugzeugabsturz, US Army, Autounfall]


### Weiterlesen

Teil 2: [Bilder einer Superheldin](https://ravenclaw-library.berlin/post/2019-04-03-captain-marvel-2/)  
Teil 3: [Taten einer Superheldin](https://ravenclaw-library.berlin/post/2019-04-19-captain-marvel-3/)  
Teil 4: [Feminismus einer Superheldin](https://ravenclaw-library.berlin/post/2019-05-02-captain-marvel-4/)  
Teil 5: The Bigger Picture (in Arbeit)  
Captain-Marvel-Folge der [Hugvengers](https://soundcloud.com/user-345715248/folge-007-captain-marvel-review)  
Review auf [Alpakawolken](https://alpakawolken.de/film-review-captain-marvel/)  
