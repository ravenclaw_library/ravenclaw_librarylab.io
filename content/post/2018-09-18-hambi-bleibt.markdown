---
title: '#HambiBleibt!'
subtitle: 'Weshalb der Hambacher Forst uns alle etwas angeht'
date: 2018-09-18
tags: ["Politik"]
bigimg: [{src: "/img/Plakat-Hambi-Bleibt.png", desc: 'Schriftzug: #HambiBleibt, vor Bücherregal'}, {src: "/img/Plakat-Hambi-Bleibt.png", desc: 'Plakat: Hambi Bleibt'}]
---

[Content Notice: Polizeigewalt]

*****

Ich habe mich in den letzten Monaten und Wochen viel mit dem Hambacher Forst und den Protesten dort beschäftigt. RWE, der Konzern, dem das Land unter dem uralten Wald bei Köln gehört, will ihn komplett roden, um mehr Platz für neuen Abbau von Braunkohle zu schaffen. Das Gebiet des Waldes ist dabei in den letzten 30 Jahren von 6.000 Hektar auf weniger als 500 geschrumpft. Dabei ist schon lange klar, dass diese Art der Energiegewinnung absolut schädlich für die Umwelt ist (es wird nur Brachland zurückgelassen, das Jahrzehnte zur Erholung braucht) und dabei noch nicht einmal effizient Energie abwirft – zu viel Energie muss für die Förderung aufgewandt werden.  
Seit Jahren besetzen deshalb Menschen den Wald, und sie wurden immer mehr. Sie wohnen in selbst gebauten Baumhäusern und wollen dadurch das Fällen dieser Bäume aufhalten (was mit Menschen darin natürlich nicht erlaubt ist und so erst eine Räumung erfordert und für Aufmerksamkeit sorft, was den Wald retten könnte). Sie organisieren auch Bildungsarbeit, zum Beispiel Waldspaziergänge mit Erklärungen zum Wald und zum Protest für Menschen aus der Umgebung und andere Interessierte.

Als linke Person habe ich eine grundsätzliche positive Einstellung zu solchen Protesten, auch wenn der Umweltschutz anfangs noch nicht meine erste Agenda war. Meine Leser\*innen sind nicht unbedingt alle so links wie ich. Aber für euch ist dieser Artikel. Denn alle von uns geht das, was gerade im Hambacher Fort passiert, etwas an. Und das möchte ich euch mit diesem Artikel zeigen.  
Ich schreibe den Artikel in der Woche, in der das Camp der Aktivist\*innen aus mehreren Baumhäusern im Hambacher Forst gerade geräumt wird. Es hat viel Kritik an diesem Camp gegeben – kein Hausrecht, zu extreme Protestform, zu viel Gewaltbereitschaft. Und ohnehin haben die meisten von uns davon ja kaum etwas mitbekommen – und wenn, dann während der Räumung. Weshalb sollte die Besetzung des Hambacher Forstes dann für uns als mehr oder weniger linke Menschen aus einer ganz anderen Ecke als dem aktiven Umweltschutz so relevant sein?

Es gibt einige Gründe dafür.

### 1. Umwelt- und Tierschutz sind erstaunlich relevant für Menschenrechte
Natürlich sollte ein Einsetzen für Tierrechte und für die Umwelt nie Hand in Hand damit gehen, das Einsetzen für Menschen nicht als Teil der eigenen Agenda zu sehen. Trotzdem sollte der Umweltschutz bei antifaschistischer und linker Arbeit nicht völlig außer Acht gelassen werden. Immerhin leben wir in einem globalen Kapitalismus, in dem Nahrungsmittel nicht immer bei uns hergestellt werden. Viele Tiere für unser Fleisch werden im Ausland aufgezogen oder geschlachtet, da die Löhne dort niedriger sind. Viele andere Lebensmittel werden dort angebaut, wo der Regenwald in Südamerika gerade abgeholzt wurde.  
Unser Konsum hat immer auch Auswirkungen auf die Lebensrealität anderer Menschen, die oft für weit weniger als den Mindestlohn arbeiten. Auch diesen Menschen ein besseres Leben zu ermöglichen und nicht nur Menschen in unserem direkt spürbaren Umfeld, sollte ein linkes Anliegen sein.

### 2. Der Fokus auf Konzerne statt auf Lebensqualität ist für uns alle relevant
Dass der Hambacher Forst unglaublich wichtig für die Umwelt der Region ist, ist lange bekannt. Und das Jammern der Umweltschützenden über sterbende Arten ist vielleicht tatsächlich mal angebracht, wenn es so einfach zu verhindern wäre. Es ist unglaublich untypisch für eine (Landes-)Regierung, auch gegen so breite Proteste, wie sich beim Hambacher Forst inzwischen gebildet haben, weiterhin die verantwortliche Firma mit solchen Mitteln zu unterstützen. Während bei den Ausschreitungen in Chemnitz kaum Polizei eingesetzt wurde, kommen im Hambacher Forst mehrere Beamt*innen auf jede protestierende Person.  
Das steht durch die Zuständigkeit verschiedener Bundesländer nicht direkt in einem Zusammenhang, zeigt aber eindrucksvoll die Prioritäten der Verantwortlichen. Die Landesregierung in NRW unterstützt RWE dabei, die Lebensqualität aller Menschen in der Region zu ruinieren. Das ist nicht ihre Aufgabe.

### 3. Landesregierung und Polizei setzen illegale Vorhaben durch
Die Landesregierung von NRW unterstützt RWE nicht nur bei einem problematischen und schadenden Projekt. Sie unterstützt dabei mit dem Bereitstellen Tausender Beamt*innen der Polizei vor allem auch illegale Vorhaben. RWE hatte bereits mehrfach durchblicken lassen, notfalls auch vor Beginn der legalen Rodungssaison mit dem Fällen von Bäumen zu beginnen. Vergangene Woche passierte das tatsächlich – umgeben von Polizei. Dabei sollte die es doch als ihre Aufgabe sehen, Straftaten zu bekämpfen, statt sich schützend daneben zu stellen.

### 4. Wo tut euch diese Gruppe von Menschen eigentlich weh?
Die wachsende Popularität der Proteste hat plötzlich eine erstaunliche Zahl von Gegner\*innen hervorgerufen. Auf Twitter, unter Artikeln in Online-Medien und beim Umgang mit den Daten und Geschichten einzelner Protestierender zeigt sich, wie sehr viele (rechte) Menschen gegen den Protest eingestellt sind. Fakt ist, die wenigsten dort unterstützen aktive Gewalt gegen die Polizei. Für Außenstehende kann diese Gruppe Protestierender nur gefährlich werden, weil sie sich proaktiv für Umweltschutz einsetzt und innerhalb der eigenen Gruppe eine möglichst angenehme Community aufbaut – mit einzelnen Spaces für alle außer cis Männer und einer sehr demokratischen Struktur. Wenn das Menschen für vernichtungswert halten, und das auf eine möglichst schmerzhafte Weise, sagt das einiges über sie aus.  
Wie positioniert ihr euch?

### 5. Die Polizei zeigt ihre problematische Seite
Es gab schon einige Proteste von Umweltschützenden – viele kleiner und weniger bekannt, aber es gab sie. Und die Polizei hat Strategien gegen zivilen Ungehorsam, Menschenketten und auch gegen wirklich festgekettete Menschen. Es war in fast allen vergangenen Fällen möglich, so einen Protest ohne eine große Zahl von Verletzten zu räumen. Doch hier zeigt sich, dass die Polizei auf einmal unglaubliche Gefahren für die Protestierenden in Kauf nimmt: es zeigt sich in Bildern von am Boden liegenden und weiterhin geschlagenen Protestierenden, im Entfernen von Menschen, die noch festgekettete oder einbetonierte Arme haben, von Bäumen (es besteht große Gefahr, dass diese Menschen sich den Arm brechen) oder im Durchtrennen von Sicherungsseilen.  
So sollte kein Protest geräumt werden, und so ein Konzept darf nicht akzeptiert werden – genauso wenig wie das Zurückhalten von Vertretenden der Presse und parlamentarischen Beobachtenden. Hier kommen wir auch definitiv in Bereiche von Gesetzesverstößen, und kein Verhalten von Protestierenden darf so etwas in irgendeiner Weise rechtfertigen. 

### 6. Die Berichterstattung ist auffällig
Mittlerweile hat ja auch die Polizei ihre Kanäle, um Infos und lustige Bilder zu verbreiten. Diese Kanäle werden auch gern als Information für andere journalistische Medien oder auch Vertretende der Landesregierung genutzt und sind damit nicht so lustig-casual, wie sie aussehen, sondern immer extrem relevant. Schon früher gab es damit häufig Probleme, zum Beispiel das Verbreiten von einem „Stand der Ermittlung“, der völlig subjektive, aber auch inkorrekte Inhalte wie Einschätzungen zur Nationalität anhand von Hautfarbe enthielt.  
Dieses Problem zeigt sich beim Hambacher Forst erneut, denn besonders auf dem Twitter-Account der zuständigen Polizei kam es mehrfach zum Verbreiten von Bildern, die angeblich Bewaffnung oder Infrastruktur zum Schaden der Beamt\*innen zeigte. Diese Bilder stellten sich immer als völlig andere Gegenstände heraus: Eingegrabene Eimer mit Beton sollten Kletterseile sichern, nicht Beamten verletzen; eine Sammlung von angeblichem Material zum Herstellen von Molotow-Cocktails stellte sich als normale Ausrüstung heraus – Wasserkanister haben viele Menschen zu Hause, sollten die jetzt alle verhaftet werden?

### 7. Reaktionen auf kommende Proteste werfen ihre Schatten voraus
Die Besetzung im Hambacher Forst ist zu einem großen Teil geräumt. Trotzdem müssen wir uns dafür einsetzen, dass diese Art der Räumung so nicht weiter durchgesetzt werden kann – weder bei dieser Besetzung noch bei irgendwelchen anderen Protesten. Besonders die Polizei und ihr Umgang mit Vertretenden der Presse machen mir Sorgen – wird beim nächsten Protest wieder eine Art Guerilla-Krieg erfunden, den es nie gegeben hat? Informiert euch, teilt die Stimmen der Betroffenen, fragt nach, wenn ihr unsicher seid. Denn die Reaktionen auf linke Proteste von Seiten der Polizei und der Politik werden wohl nicht sanfter werden.


## Was könnt ihr tun, um die Proteste zu unterstützen?

Es gibt einige Möglichkeiten, die Proteste zu unterstützen und damit vielleicht auch zu einer Fortsetzung des Widerstands beizutragen, sollte die Räumung durchgezogen und beendet werden. Die meisten davon sind auch nicht mit viel Gefahr oder Anstrengung verbunden!

### 1. Unterstützt die Menschen vor Ort. 
Geht vorbei, bringt etwas zu Essen oder etwas anderes von den online verfügbaren Packlisten mit. Ihr müsst auch keine Polizeikette durchbrechen. Schon da sein und Präsenz zeigen trägt dazu bei, dass die aktiv sichtbare Gruppe der Menschen im Widerstand größer wird. 

### 2. Kündigt eure Verträge bei RWE und den angeschlossenen Firmen. 
Ein einfacherer Schritt von zu Hause: Schaut doch mal nach, ob ihr einen Vertrag bei RWE habt. Da muss auch nicht zwangsläufig RWE im Header des Vertrags stehen, mehrere Firmen haben ihrerseits Anteile an RWE oder sind anders benannte Tochterunternehmen. Wechseln kostet euch meistens nur Arbeit, beim aktiven Suchen findet ihr oft auch ähnlich bezahlbare Tarife. Und wolltet ihr nicht eigentlich immer schon für das Gewissen zu einem Ökostrom-Anbieter wechseln? Es gibt übrigens auch Online-Dienste, die euch die Organisation abnehmen, falls es daran scheitern sollte. Und wenn ihr keinen Vertrag habt, der RWE irgendwie unterstützt: Fragt eure Eltern, Verwandte und Freund\*innen, ob sie einen haben!

### 3. Spendet für diese und weitergehende Proteste.
Es ist aus Gründen absolut keine Option, in den aktiven Protest zu gehen? Das passt. Nicht alle von uns haben die körperlichen und psychischen Möglichkeiten, vor Ort Proteste zu unterstützen. Aber ihr könnt spenden!  
Konto: Spenden und Aktionen Volksbank Mittelhessen  
Betreff: Hambacher Forst  
IBAN: DE29 5139 0000 0092 8818 06  
BIC: VBMHDE5FXXX

### 4. Informiert euch und verbreitet die Informationen
Auch und gerade, wenn ihr euch Spenden nicht leisten könnt: Nutzt eure Plattformen, teilt den Link zur Website, zu Twitter-Accounts zum Thema oder zu Artikeln in den Medien (oder zu diesem Artikel). Erreicht Follower, eure Verwandte, eure Freund\*innen. Denn es betrifft uns alle. 

## Weiterlesen

Hier findet ihr mehr Infos zu den Protesten:

Blog und [Seite](https://hambacherforst.org) der Proteste  
Klimabewegung [„Ausgeco2hlt“](www.ausgeco2hlt.de)
