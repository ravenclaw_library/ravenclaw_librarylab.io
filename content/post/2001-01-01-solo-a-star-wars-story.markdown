---
layout: post
title:  "[Rezension] Solo - A Star Wars Story"
date:   2001-01-01 00:00:00
categories: TischDerBibliothekarin
published: false
---

(Diese Rezension enthält große Spoiler, es war ohne für mich nicht wirklich möglich)

Ich bin mit Star Wars nicht von Anfang an aufgewachsen, aber habe am Anfang meines Lebens als bewusster Teenager die Filme gezeigt bekommen – und fing an, von fernen Sternen, Planeten und Raumschiffen zu träumen. Prinzessin und General Leia Organa war meine erste begeisternde Frauenfigur, die politisch und kämpferisch große Dinge bewirkte und nicht immer nur die Helden unterstütze. Ich trage gern meine Star-Wars-T-Shirts, Rogue One hat meine Ansprüche und Wünsche an cinematische Ästhetik praktisch gesprengt, und Episode VII und VIII haben mich so glücklich gemacht wie kaum eine Entwicklung in einem meiner geliebten Fandoms. Das alles sollte man wissen, wenn ich jetzt über ‚Solo: A Star Wars Story‘ sage, dass der Film ohne gute Kinobegleitung verschwendete Zeit gewesen wäre.

Mir war klar, dass die Zeit vor Han Solos Feststellung, er könnte vielleicht doch eine Moral haben, nicht hundertprozentig meine Story werden würde. Aber ich wurde nicht nur mit einer Besetzung voll zweifelhaft handelnder Figuren konfrontiert, sondern mit einem ganzen Film, der sich klar an eine andere Zielgruppe als mich richtete. Die Story war so dünn, dass man praktisch hindurchschauen konnte, die einzige WoC musste natürlich für den Plot sterben, die als so wichtig angespriesene Frauenfigur an Hans Seite war nur Motivation nach ihrem Verlust und danach die böse, verräterische Femme Fatale – und ich habe noch nie so viel Cultural Appropriation auf so wenigen Filmminuten gesehen. 

Der Humor läuft ausschließlich sexistisch und rassistisch ab, Chewbakka wird zur Figur von als drohend – oder gern auch mal als lächerlich – dargestellter Homosexualität, und neben den Frauenfiguren müssen natürlich auch PoC andauernd gerettet werden, um das Narrativ des weißen Retters zu unterstützen. Und das PoC ohne jeglichen Kontext total angebracht neben Aliens als Gesamtbild einer exotisierten Unterhaltung der handelnden Figuren sein können, sollte auch schon jahrzehntelang überholt sein. 

Ich habe mich im Kino also durchgehend geschämt, diesen Mist in den Kinozahlen unterstützt zu haben.  

Es gibt ein paar tolle Flugszenen. Aber ehrlich, schaut die auf Youtube.
