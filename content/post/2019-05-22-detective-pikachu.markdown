---
layout: post
title: '[Review] Detective Pikachu'
date: 2019-05-22
tags: ["Filme", "Rezensionen"]
bigimg: [{src: "/img/Detective-Pikachu.jpg", desc: 'Ein Pokéball in der Hand einer Person'}]
---

[CN Ableismus]

*****

Ich hatte keine Erwartungen. Ich bin eine Person, die nur in Teilen mit Pokémon aufgewachsen ist, die sehr spät da hineinstolperte und die Verfilmungen, vor allem Life-Action-Verfilmungen, von Zeichentrick- und animierten Quellen grundsätzlich skeptisch gegenübersteht. Vielleicht war ich genau deshalb so positiv überrascht, als ich mich einer Gruppe für einen Kinobesuch in „Detective Pikachu“ angeschlossen habe.

### Genres und Franchises

Der Trailer hat mir nicht unbedingt aktiv Lust auf einen krassen Film gemacht. Voller cooler Pokémon, aber ohne erkennbare Handlung. Diese Handlung habe ich dann auch gar nicht erwartet – ich dachte auch nach der Einführung des Films noch, das würde ein bisschen „Protagonist findet sein Pokémon, yay“. Ganz nett, weiterhin beeindruckende Bilder, mehr nicht. Denn damit hätten sie ja schon abgehakt, was für einen Franchise-Film erforderlich ist: Dieses Franchise irgendwie mit einer kleinen Story kombinieren, und das war’s. 

Detective Pikachu ist ganz anders als das, und ich bin richtig enttäuscht, dass das nicht im Voraus kommuniziert wurde – denn ohne diese Anfrage von außen hätte ich den Film nicht gesehen. Und ja, wenn ich die üblichen Erwartungen an eine Story gehabt hätte, dann wäre ich enttäuscht gewesen, denn der originellste Plot aller Zeiten ist es wirklich nicht. Aber es ist ein toller Plot, der auf mehreren Ebenen wirklich gut funktioniert, der die Pokémon tatsächlich nur sekundär miteinbezieht und dabei einfach auch auf zwischenmenschlicher Ebene für die Figuren eine tolle Geschichte erzählt. Ganz abgesehen davon, dass der Protagonist nicht weiß ist und dabei trotzdem einfach mal einen coolen Action- und Mystery-Plot erleben darf, was viel zu selten passiert. Ich wurde richtig in diese Handlung hineingeworfen, und trotz verschiedener vorhersehbarer Aspekte war es gut möglich, den Plot noch ernst zu nehmen. 

### Repräsentation und Zielgruppen

Ich habe schon den positiven Aspekt erwähnt, der für mich an Repräsentation in diesem Film steckte, leider gibt es auch einen großen negativen. Ein Teil des Plots dreht sich um eine behinderte Figur, die böse ist, auch aus Wut über diese Behinderung. Das ist wieder einmal verdammt schädlich für alle betroffenen Menschen, gerade auch in Bezug darauf, dass der Film trotz seiner einen doch sehr erwachsenen Storyline auf Figurenebene wohl vor allem Kinder und Jugendliche ansprechen soll (und nostalgische Nerds, aber ist das nicht immer so). Und es hat mich sehr geärgert, denn dieser Plot hätte auch ohne diesen Aspekt noch gut funktioniert. Ja, der Film basiert auf einer Story namens Detective Pikachu, die auch in einem Pokémon-Spiel umgesetzt wurde – aber das hätte ja niemanden daran gehindert, ein bisschen mehr drüberzuschauen und noch ein paar mehr kleine Dinge zu ändern als ohnehin schon. Der Plot hätte so nämlich auch ohne diese Behinderung funktioniert, und das macht es für mich noch schlimmer. 

Positiv muss ich vermerken, dass die vorhandene Hetero-Romanze vergleichsweise gesund und von Konsens geprägt ist – und nicht sofort alles einnehmend wird und beide Figuren alles füreinander hinwerfen. Besonders in Bezug auf die (Teil-)Zielgruppe ist das etwas, das ich immer seltener sehe, und es hat mich positiv überrascht. 

### Plots und Sequels

Ich mochte den Plot auch unter anderem deshalb wirklich gern, weil er am Ende des Films auch beendet war. Es gab keinen übergeordneten Plot, die bösen Ideen der Villains müssten zumindest wieder von einer unbekannten neuen Figur aufgenommen werden – und das gibt mir beim Ansehen auch das schöne Gefühl des Abschließens, das inzwischen nur noch so selten aufkommt. 

Drückt einfach mit mir zusammen die Daumen, dass sie daraus nicht nachträglich noch eine Reihe machen, denn der dann hinzugefügte Plot muss schon *sehr* gut sein, um mich zu überzeugen. 

(Die Content Notice unten enthält Spoiler.)

*****

[Content Notice für den Film: Ableismus, Tod von Eltern, Autounfall, Erdbeben]
