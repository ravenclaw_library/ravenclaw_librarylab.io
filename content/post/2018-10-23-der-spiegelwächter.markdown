---
layout: post
title: '[Rezension] Annina Safran: Der Spiegelwächter'
date: 2018-10-23
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Der-Spiegelwächter.jpg", desc: 'Cover von "Der Spiegelwächter" von Annina Safran, vor einem Bücherregal'}]
---

*Mir wurde über NetGalley ein Rezensionsexemplar dieses Buches zur Verfügung gestellt. Obwohl mich das Buch nicht überzeugen konnte, danke ich NetGalley, der Autorin und dem Verlag dafür.*

*****

Ich fand den Klappentext des Buches und die Ästhetik im Cover sehr spannend. Geschichten mit einer anderen Welt kenne ich einige, auch einige mit einer anderen Welt auf einer anderen Seite des Spiegels. Die meisten davon machte eine besondere Eigenschaft dieser anderen Welt aus, die das Buch für mich von den jeweiligen anderen abgehoben hat – oder es haben mich tolle Figuren darüber hinweg getragen, dass ihre Geschichten vielleicht nicht die einfallsreichsten des Jahres waren. 

„Der Spiegelwächter“ hat leider beides nicht geschafft. 

Die Protagonistin Ludmilla hat es tatsächlich sogar schon in den ersten paar Kapiteln geschafft, mich richtig wütend zu machen. Sie wurde nicht nur als unglaublich besonders und „immer anders als die anderen“ beschrieben, ohne dass das näher erklärt wurde, sondern sollte auch sichtlich als übertrieben schlagfertig dargestellt werden. Das führte zu andauernden Wortduellen mit allen anderen Figuren – und das hat sehr schnell nicht nur den Rhythmus genommen und von der Geschichte abgelenkt, sondern auch genervt. Außerdem hat der Schreibstil eher eine Richtung zu mehr Wiederholungen von „Ludmilla“ statt mehr Umschreibungen mit „sie“, und bei diesem langen und auffälligen Namen machte auch das die Geschichte sehr holprig. 

Auch der Plot konnte mich nicht überzeugen. Schon bei knappen 10 Prozent habe ich überlegt, abzubrechen, wollte dem Plot aber noch eine Chance geben. Leider passierte bis zur 20-Prozent-Marke nicht viel mehr als die Initiation der Protagonistin in den Hauptplot, und auch der wurde nur mit den üblichen Rahmenbedingungen (gegenüber der üblichen Auserwählten) umrissen. 

Weiter als bis dorthin bin ich deshalb leider nicht gekommen – an der Stelle direkt nach der Erklärung habe ich das Buch abgebrochen. Ich glaube, Lesende mit weniger Anspruch an Figurentiefe, aber mehr Spaß an Wortgefechten könnten hier mehr Spaß haben als ich. 
