---
title: '[Rezension] J.R.R. Tolkien: Tales from the Perilous Realm'
date: 2018-05-27
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Tales-from-the-perilous-realm.jpg", desc: 'Tales from The Perilous Realm von J.R.R. Tolkien, vor einem Bücherregal'}]
---

*„Was zur Hölle ist denn das? Ich dachte, der Typ hätte nur das mit dem scheiß Ring geschrieben!“ – Typ in der S-Bahn, als ich etwa in der Mitte des Buches war.*

 
J.R.R. Tolkien ist eine\*r dieser Autor\*innen, die viele nur für einen Teil ihres Werkes kennen. Während "Der Herr der Ringe" den meisten noch geläufig ist und sogar Büchern abgeneigte Menschen meistens zumindest eine Grundidee der Filme haben, rückte "Der Hobbit" erst mit den Verfilmungen in den letzten Jahren ins Rampenlicht. Zu den Verfilmungen selbst möchte ich an dieser Stelle gar nichts sagen – aber tatsächlich gibt es bei Tolkien auch andere interessante Literatur als diese populäre Reihe. 

Ich interessiere mich schon länger für die Hintergründe der Literatur Tolkiens, und seit einer Weile auch sehr für sein Leben und Schaffen. Das brachte mich zu Sekundärtexten von und über ihn, seine Arbeit an seiner fiktiven Welt Mittelerde und seine Entwicklung verschiedener Sprachen. Dabei habe ich schnell bemerkt, dass für Tolkien selbst der "Herr der Ringe" zwar eine große Bedeutung hatte, er in seinen Briefen und Essays aber auch viel auf kleinere Prosastücke verwies. Sie alle standen schon länger auf meiner Liste von „sollte ich irgendwann mal lesen“ – und natürlich sind sie auch nur auf dieser Liste geblieben, auch durch eine generelle Leseflaute im letzten Jahr. Doch dann habe ich innerhalb einer Woche eine tolle Buchhandlung für Fantasy und Science-Fiction kennengelernt und wurde von einem Paket erreicht: Darin war "The Letters of J.R.R. Tolkien", das mir Freundinnen in der Blackwell’s Buchhandlung in Oxford gekauft hatten!

Mein Kauf aus der Buchhandlung, die "Tales from the Perilous Realm", stand dann noch eine Weile herum und starrte mich an – bis zu einer Zugfahrt mit viel Zeit und Langeweile. Und von dort an ging es zwar zunächst langsam voran, dann aber immer schneller.
Nur eine der sechs Geschichten aus Tolkiens Feder hat eine Verbindung zu seinem Mittelerde-Kosmos: Die Gedichtsammlung "The Adventures of Tom Bombadill", angeblich aus einem Buch der Hobbits. Doch auch die anderen Geschichten haben mich schnell in ihren Bann gezogen: "Roverandom", die lange Reise eines verzauberten kleinen Hundes, ebenso wie "Farmer Giles of Ham", das Abenteuer eines Bauern, der gegen seinen Willen zum Helden werden soll, und "Smith of Wootton Major" als eine spannende Ausführung der Verbindung zwischen dem Menschen- und Feenreich. "Leaf by Niggle" war am Ende noch einmal etwas ganz anderes: Die Erzählung über einen Maler und seinen Nachbarn wendet sich zur Allegorie über die richtige Art der Lebensführung.

Abgedruckt ist noch ein Essay Tolkiens, "On Fairy-Stories", das ich sehr interessant und gut zu lesen fand – auch als Einstieg in Tolkiens Texte zu seinen Werken und seiner Welt sollte es gut geeignet sein. Während einige seiner Briefe doch viel Hintergrundwissen voraussetzen, gerade zur Welt von Mittelerde, setzt dieses Essay ziemlich gut an Teilen der Geschichten an und bietet damit eine lebendige Hintergrundlektüre. 

Auch wenn sich die Geschichten von Handlung und Ausmaß her natürlich stark von Tolkiens viel bekannterem Mittelerde-Legendarium unterscheiden, kam für mich schnell sein typischer Schreibstil durch. Weniger Ortswechsel in der Handlung führen zu weniger der typischerweise sehr epischen Landschaftsbeschreibungen – die mir beim ersten Lesedurchgang des "Herrn der Ringe" damals doch gehörige Schwierigkeiten bereitet haben. Dafür sind die Spannungsbögen gekonnt gesetzt, ständig bewegt sich etwas, und trotzdem kommt die Figurenerentwicklung nicht zu kurz. Tolkien schreibt für mich einfach fesselnd, und die aufgemachten Welten sind dreidimensional, komplex und voll toller Nebenfiguren.

Damit ist das Buch für mich eine klare Leseempfehlung, gerade und auch an Fans von Tolkiens Mittelerde-Kosmos!
