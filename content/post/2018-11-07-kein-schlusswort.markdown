---
title: "[Rezension] Antonia von der Behrens (Hrsg.): Kein Schlusswort – Nazi-Terror, Sicherheitsbehörden, Unterstützernetzwerk"
subtitle: 'Der NSU und der Prozess, Teil 3'
date: 2018-11-07
tags: ["Politik", "Bücher", "Rezensionen"]
bigimg: [{src: "/img/kein-schlusswort.jpg", desc: 'Cover von "Kein Schlusswort", vor einem Bücherregal'}]
---

[Content Notice: Nazis, Rassismus, Polizei, Gerichte]

<center>*****</center>

Diese Rezension ist Teil meiner Artikelreihe zum NSU. Alle Artikel dieser Reihe findet ihr unten verlinkt. 

Der NSU-Prozess hatte einen hohen symbolischen Stellenwert. Zunächst, weil wichtige und große Verbrechen aufgeklärt werden sollten – aber vor allem, weil auch die Versäumnisse der ermittelnden Behörden aufgearbeitet werden sollten.  
Inzwischen ist der Prozess vorbei und wir wissen, dass genau das nicht passiert ist. Umso wichtiger ist es, die Informationen über die Taten aufzuarbeiten, die für den Prozess zusammengetragen wurden. Die Plädoyers besonders der Nebenklage, der Anwält\*innen der Betroffenen, bleiben in ihren Forderungen unerfüllt, aber als stehende Forderung bestehen.

Dieses Buch vereint Plädoyers und Reden von Betroffenen und Anwält\*innen der Nebenklage. Es ist damit Dokumentation zum Prozess und zu den Vorgängen und Folgen zugleich, bietet einen Überblick über die Sprache und Vorgänge im Prozess – und der letzte Beitrag, das Plädoyer der verstorbenen Opferanwältin Angelika Lex, ist ein wunderbarer Überblick über die genauen Abläufe der Zeit des NSU im Untergrund, die Fehler der ermittelnden Behörden und das wissentliche Vertuschen von Informationen, auch mit Quellenangaben. 

Dass verhältnismäßig viele Betroffene des Nagelbombenanschlags in der Keupstraße in Köln zu Wort kommen, fand ich auf den ersten Blick ungewöhnlich – dann erwischte ich mich dabei, dass ich beim Fokus auf die begangenen Morde des NSU die großflächig versuchten gar nicht als die schlimmen Taten betrachtete, die sie tatsächlich sind. Zumal in den Plädoyers auch zur Sprache kommt, wie sehr auch nach diesem Anschlag von den Betroffenen bereits sehr früh ein mögliches rassistisches Motiv in den Fokus gerückt, diese Informationen aber von den Polizeibeamt\*innen teilweise regelrecht niedergeschrien und von den ermittelnden Behörden ignoriert wurden. 

Lest dieses Buch! Es ist anfangs nicht einfach, in die Gerichtssprache hineinzufinden, aber diese Plädoyers sind so wertvolle Zeitdokumente und so wichtig für den NSU, den Prozess und die Diskussion über die Anschlags- und Mordserie und Rechtsextremismus. Kauft das Buch, wenn ihr könnt, fragt es in eurer Bibliothek an oder leiht es von mir. Aber lest die Stimmen der Betroffenen und der Nebenklage.  
Und lasst sie, nach dem noch nicht einmal ansatzweise gerechtfertigten Prozessurteil, nicht verstummen.

Kein Schlusswort, kein Schlussstrich!

[Content Notice für das Buch: Tod, Mord, Terror, Nazis, Rassismus, Polizei, Suizid, Schusswaffen]

## Weiterlesen

### Weitere Artikel meiner Aktion

Teil 1: [Rechter Terror mit Unterstützung der Behörden: Eine Einführung zum NSU](https://ravenclaw-library.berlin/politischeabteilung/2018/10/18/einf%C3%BChrung-zum-nsu.html)  
Teil 2: [Rezension zu „Schmerzliche Heimat. Deutschland und der Mord an meinem Vater.“](https://ravenclaw-library.berlin/politischeabteilung,/schreibtischderbibliothekarin/2018/10/18/schmerzliche-heimat.html)  

### Weitere Artikel im Umfeld
Eine [Rezension zu „Mit der Faust in die Welt schlagen“](https://skepsiswerke.de/mit-der-faust-in-die-welt-schlagen-lukas-rietzschel/) von Alisha  

### Weitere Informationen zum NSU
Website der [NSU Watch](https://www.nsu-watch.info/), ein Bündnis politischer Gruppen für Bildungsarbeit zum Komplex, das u.a. Prozesse, Untersuchungsausschüsse und Veranstaltungen begleitet und dokumentiert  
[Ein (erstaunlich kritischer) Überblick](http://www.bpb.de/politik/extremismus/rechtsextremismus/167684/der-nationalsozialistische-untergrund-nsu) der Bundeszentrale für politische Bildung  

