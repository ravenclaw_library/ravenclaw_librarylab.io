---
title: '[Rezension] Stephen Chbosky: The Perks of being a Wallflower'
date: 2018-06-05 
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Perks-of-being-a-wallflower.jpg", desc: 'The Perks of being a Wallflower von Stephen Chbosky, vor einem Bücherregal'},  {src: "/img/Perks-of-being-a-wallflower2.jpg", desc: "Buch auf einem Tisch in einem Café"}]
---

[Content Notice für die Rezension: Depressionen, Psychiatrie, Suizidalität]

„A modern classic“ nennt der Penguin-Verlag die aktuelle englische Ausgabe, und sie haben Recht damit. Die Verfilmung hat für einen noch breiteren Bekanntheitsgrad gesorgt, doch auch schon davor war das Buch verbreitet. Aus den Bücherregalen der Nerds und Außenseiter wagt es sich allerdings erst langsam hervor – aber ich freue mich darüber.  
Denn die Geschichte von Charlie wagt sich an die schwierigen Themen, die doch relevant sind für alle, die sich auch nur mit der „grundlegenden“ Unsicherheit auseinandersetzen müssen. Die offenen Fragen an die eigene Zukunft, eine Schulzeit ohne Freunde und voller Unsicherheiten machen Charlie für alle Lesenden zu einem Protagonisten, mit dem sich identifiziert werden kann.  
Über all dem, über Charlies Alltag und seinen Gedanken, dem reflektierten Berichten in Briefform und der Atmosphäre der High School breitet sich sofort eine nachdenkliche Nostalgie aus. Nicht ohne Grund wird das Buch so gern zitiert, und ich kann es wirklich allen allein für die Sprache ans Herz legen!

Depressionen und die ganz schlimmen Unsicherheiten wiederzufinden, hat mir besonders gefallen. Natürlich wird das Buch so auch schwerer als sonst. Aber durch die Verbindung mit den „heavy topics“ ist für mich auch eine unglaublich authentische Atmosphäre entstanden, die ich so selten wiedergefunden habe.  
Wer Lust auf magische Momente im echten Leben, Berichte von Rückschlägen und Leichtigkeit neben der Selbstfindung nach Verletzung und Missbrauch hat, dem sei das Buch wärmstens ans Herz gelegt. Manchmal habe ich da sogar das Gefühl, es hält mich ein bisschen warm.


[Content Notice für das Buch (SPOILER!): Depressionen, Psychiatrie, Suizidalität, Diskussion von sexuellem Missbrauch und (Kindes-)Missbrauch]
