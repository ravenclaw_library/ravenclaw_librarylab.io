---
layout: post
title: '[Rezension] Kristen Roupenian: Cat Person'
date: 2019-03-18
tags: ["Bücher", "Rezensionen"]
bigimg: [{src: "/img/Cat-Person.jpg", desc: 'Buch "Cat Person" auf einem Kindle vor einem Bücherregal'}]
---


*Mir wurde über NetGalley ein Rezensionsexemplar dieses Buches zur Verfügung gestellt. Obwohl mich das Buch nicht überzeugen konnte, danke ich NetGalley, der Autorin und dem Verlag dafür.*


[Content Notice: Vergewaltigung, sexuelle Übergriffe, Victim Blaming, Gewalt gegen Kinder, Misogynie]

„Cat Person“ wurde sehr aktiv auf allen Kanälen um mich herum beworben. Ich habe von anderen Menschen, oft auch von Misogynie Betroffenen gehört, dass sie sich auf das Buch freuen würden, es stand groß in Schaufenstern und auf Bücher- und Thementischen in allen Buchhandlungen, in die ich ging, das Feuilleton nahm sich des Buches an (im Nachhinein ein schlechtes Zeichen). Leider habe ich mir entweder nicht genug Zeit zum Warten genommen und die Menschen, die es kaufen wollten, nach ihrem Eindruck zu fragen – oder ich habe mich einfach zu früh um das Rezensionsexemplar beworben. Auf jeden Fall wurde ich aufs Bitterste enttäuscht. 

Eine Sammlung an Kurzgeschichten, die damit beworben wird, „der Roman zu #metoo“ zu sein. Für mich war das in der Werbung natürlich sofort ein Grund, das Buch lesen zu wollen. Und es hat bestimmte Erwartungen in mir erzeugt: Einblicke in ein Leben von Frauen im Patriarchat, das Thematisieren von Enttäuschungen, die da normal sind (wie auch in der zuvor bekannt gewordenen gleichnamigen Kurzgeschichte), aber auch von aktiv übergriffigem Verhalten und Auswirkungen der bestehenden Rape Culture. Erwartet habe ich auch, dass diese Themen vielleicht hart und graphisch umgesetzt werden könnten, dabei aber auch als das tiefe, strukturelle Problem dargestellt werden, das sie sind – und natürlich, dass die Betroffenen zu Wort kommen und dabei auch als Sympathietragende der Lesenden umgesetzt werden würden.

Bekommen habe ich davon rein gar nichts. Schockierend graphische sexuelle Gewalt? Ja. Betroffene, die immerhin erzählend vorkommen? Ja. Aber die Umsetzung hat für mich alles wieder zunichte gemacht. Ja, ich finde es wichtig zu thematisieren, dass Gewalt in schulischen Kontexten eklig und scheiße ist. Aber das in einer Geschichte, in denen eine Mädchenklasse in einem „Entwicklungsprojekt“ das gegenüber dem armen, jungen Freiwilligen ausnutzt und ihn so und mit emotionaler Manipulation stalkt und drangsaliert, dass er „keine Wahl mehr hat“, als sie zu verprügeln? Ein Kammerspiel von einer Mutter und der neuen Frau des Vaters, die so in den Strukturen gefangen sind, dass es Mordpläne gibt, als sie endlich allein im Wald sind - weil sie einfach nicht damit klarkommen, nicht die strahlende, einzige zu sein?

Es gibt in diesem Buch keine Frauenfigur, die nicht ein absolutes Arschloch zu sein scheint. Und ja, auch solche Betroffene gibt es. Und ja, auch sie verdienen Solidarität. Aber die kommt bei mir nicht aktiv im Kopf hoch, wenn anders als in der zuvor veröffentlichten Story die Typen und Täter immer wieder die Sympathiefiguren sind, die den Strukturen „auch nur ausgeliefert“ sind. (Spoiler: Sind sie in manchen Bezügen wie zum Beispiel toxischer Maskulinität, aber nicht in denen, die thematisiert werden.)

Ich habe das Buch abgebrochen. Ich habe es einfach nicht ausgehalten. Und Nein, ich kann es leider überhaupt nicht empfehlen. 

[Für das Buch gilt ebenfalls die oben genannte Content Notice, aber ich rate euch zum Suchen von mehr Informationen, denn ich habe das Buch nicht ganz gelesen.]

