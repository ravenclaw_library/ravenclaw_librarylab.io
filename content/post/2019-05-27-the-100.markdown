---
layout: post
title: 'Gruppendynamik, romantische Beziehungen und andere wichtige Dinge nach der Apokalypse'
subtitle: 'The 100, Teil 1: Eindrücke aus den ersten 10 Folgen, Erwartungen, Reflektion'
date: 2019-05-22
tags: ["Serien"]
bigimg: [{src: "/img/The-100_1.jpg", desc: 'Ein Wald vor Bergen, dazwischen eine Ruine'}]
---

[CN: Vergewaltigung und sexuelle Übergriffe (beides kurz referenziert)]

*****
Ich finde es super spannend, bei anderen Menschen zu beobachten, wie ihre Meinungen von Serien sich über die Zeit immer wieder verändern, während sie diese Serien schauen. Auch bei mir selbst kann ich oft beobachten, wie sich Erwartungen entwickeln, die dann erfüllt oder zerbrochen werden können, wie mir einzelne Figuren ans Herz wachsen und sich dann mit einer einzigen Aktion wieder von mir lösen oder sich als Arschlöcher herausstellen. 

Ich habe vor einer Weile The 100 angefangen – und beschlossen, da mal bei mir zu verfolgen, wie es mir nach den einzelnen Staffeln so geht. Heute gibt es Folge 1 zu den ersten 10 Folgen, denn nur so kann ich auch am Ende der ersten Staffel Veränderungen aufzeigen und Vergleiche ziehen. 
Ohne Spoiler wird sich die Artikelreihe wohl nicht machen lassen, aber dieser Artikel wird noch ohne Spoiler sein. Und: Bisher kann ich die Serie sehr empfehlen, falls ihr noch einsteigen und dann parallel mitlesen wollt!

### Grausamkeit und jugendliche Protas
Ich mag „Lord of the Flies“-Szenarien. Figuren, gerade auch jugendliche, die in halbwegs größeren Gruppen auf sich allein gestellt sind und in Abwesenheit von regelnder Autorität eine Gesellschaft aufbauen müssen, zeigen mir immer wieder tolle Figurenkonstellationen und -entwicklungen. Ich fangirle sehr, wenn ich die größeren Schritte dabei verfolgen darf, wenn Figuren endlich über ihren Schatten springen und mit einer verhassten anderen Person reden, weil sie sich halt doch nicht aus dem Weg gehen können – es hat Sitcom-Aspekte, aber auch einen Rahmen, und es fehlt im Vergleich zu Sitcoms zumindest ein großer Teil dieses gottverdammten diskriminierenden Humors, der mir die spannenden Figurenkonstellationen dann doch immer wieder zunichtemacht.

Ein Nachteil, der dabei immer wieder auftritt, ist die Frage, wie sehr die (erwartbaren, logischen, realistischen?) Entgleisungen dieser Gruppe, besonders am Anfang, dargestellt werden. Es scheint eine zumindest unter Schreibenden und damit eben auch Writers von Serien, verbreitete Meinung zu sein, dass ein Ausbruch von Gewalt unter der Leitung der Stärksten bei so etwas dann unvermeidlich ist. Auch das finde ich spannend, aber dabei fallen viele Serien in die Falle, diese Phase des anfänglichen ungeordneten Chaos mit sexuellen Übergriffen oder Vergewaltigungen zu illustrieren. Dass The 100 das nur in wenigen Szenen am Anfang getan und es bei kurzen Übergriffen gelassen hat, die in den allermeisten Fällen sofort wieder von den „Guten“ aufgelöst wurden, muss ich der Serie dabei schon fast hoch anrechnen, auch wenn mich der bleibende Aspekt trotzdem gestört hat. Aber mal sehen, wie es weitergeht. Dass diese übliche erste Phase Anarchie überstanden ist, heißt in solchen Narrativen ja nicht, dass es nicht noch eine weitere geben kann – und wir wissen auch alle, dass solche schädlichen Aspekte auch in anderen Plots zum Einsatz kommen können.

### Mainstream und Repräsentation
Ich hatte einige Hoffnungen auf coole Repräsentation in dieser Serie. Immerhin sind die titelgebenden 100 ausgestoßenen jugendlichen Straftäter*innen eine Gruppe, die groß genug für etwas Diversität wäre – doch leider wurden zumindest in diesen ersten zehn Folgen meine Hoffnungen bitter enttäuscht. Die eine wirklich wichtige Figur am Anfang, die nicht weiß ist, stellt sich als Teil der fiesen Elite heraus, eine später auftauchende Person wird immer wieder als Gegenpol zur weißen, blonden Hauptfigur ausgespielt. Und auch bei vielen verschiedenen Plots um viele verschiedene Personen waren diese Plots doch irgendwie alle nur auf hetero Pairings beschränkt und wenn die mal nicht happy sind, gibt es (Überraschung!) ein Love Triangle. Ich kann nur hoffen, dass die weiteren Staffeln mutiger werden, denn ich weiß nicht, wie lange ich diese Serie sonst durchhalte. (Aber: Ein Love Triangle ist in ein anderes übergegangen? Das war zumindest etwas überraschend und witzig.)

Auf die Liste der negativen Repräsentation kommt leider auch die Repräsentation von Menschen, die stark an Natives erinnern – und die den 100 gegenüber feindlich eingestellt und niederträchtig dargestellt werden. Ich habe gerade die Hoffnung, dass das in den weiteren Folgen besser werden wird, aber bisher war ich extrem enttäuscht. Auch hier werde ich wohl die Augen offenhalten und hoffen müssen. Und: Keine der Figuren reflektiert den internalisierten Rassismus, den sie alle diesen Feinden gegenüber haben – aber es gibt immerhin spannende politische Verwicklungen.

### Politik und Figurentiefe
Die Serie hat mich sehr damit überrascht, wie viel Wissen Zuschauende über die Zustände auf der Ark bekommen, die gleichzeitig in politische Handlungen verwickelt wird. Anfangs war noch meine Vermutung, dass erst bei einer Rückkehr von einem Teil der 100 wieder ein Teil der Ark gezeigt werden würde, aber ich freue mich eigentlich viel mehr über diese Umsetzung. Denn dadurch bekommen nicht nur die Figuren der 100 selbst mehr Hintergrund – der Plot wird auch tiefer, und der Cast wird dadurch auf eine Weise erweitert, die nicht mehr nur noch eine Art von Leben und direkter Beeinflussung von Ereignissen zeigt. Ich mag auch die Figuren sehr, die dadurch hinzugefügt werden: um nicht zu viel vorwegzunehmen, belasse ich es mal beim Andeuten von grey moral und coolen Konflikten, die die Figuren sehr dreidimensional machen und eben nicht länger in „gut“ und „böse“ teilen (etwas, das ich sehr mag).

Auch die 100 selbst werden mit mehr Raum durch mehr Folgen etwas ausgestalteter und dadurch weniger die düstere Masse, als die sie mir am Anfang (logischerweise) erschienen. Ich mag nicht alle Entwicklungen sehr gern, aber dafür ist wohl das Review nach Staffel 1 ein geeigneterer Platz zum Ranten und Freuen. Und: Ich finde die Entwicklungen zumindest alle realistisch. Die Figuren weichen größtenteils nicht sofort von einer persönlichen Moral und dem eigenen Hintergrund ab – und wenn, dann gibt es einen logischen Grund, weshalb all das durchgeschüttelt wurde.

### Der Realismus des apokalyptischen Settings
Und das ist auch etwas, das mich am Setting sehr freut: der Realismus der Apokalypse. Ich liebe die Story, wie es zu dieser Apokalypse kam, die Hintergründe der Raumstation, die Art, wie damit umgegangen wird und wie sich an all diese Vorgänge erinnert wird. Ich finde spannend, wie die Umsetzung einer Vorstellung davon aussieht, wie sich die Erde in mehreren hundert Jahren verändert hat – auch wenn ich zu vielen Dingen aus Mangel an Fachinformationen natürlich nicht viel sagen kann. Aber für mich als Person ohne Vorwissen fühle es sich logisch an – ganz abgesehen von der Spannung davon, dass sich viel mehr verändert hat als in den üblichen Darstellungen von Apokalypsen. Hier gibt es keine sicheren Lebensmittel mehr (außer Alkohol), und die gesamte Natur hat sich verändert. 

Floureszente Spezies sind nur ein Teil davon, aber ein beeindruckendes Beispiel dafür, wie sehr hier in die Kreativität investiert wurde – und für mich als Person ohne Vorwissen fühlen sich auch alle Veränderungen so an, als würden sie zeitlich zusammenpassen, also realistisch im Sinne davon, dass sowohl das Wasser als auch die Tiere so lange Zeit hatten, sich nach der nuklearen Katastrophe weiterzuentwickeln. Es ist toll, als Zuschauende diese neue Welt kennenzulernen, denn es ist nicht immer nur fies, sondern oft auch einfach schön. Und gerade anfangs haben Zuschauende über all diese Dinge ja so viel Vorwissen wie die 100: gar keine. 

### Erwartungen an die weitere Serie
Ich bin bereits mit einigen Aspekten der Serie sehr glücklich. Ich mag auch das Pacing und die Struktur der Spannung, also wird sie mich wohl noch eine Weile halten. Ich hoffe für die weiteren Staffeln und das Ende von dieser auf mehr Repräsentation als der von weißen normschönen hetero Figuren, weitere coole politische Hintergründe und Verbindungen der Handlung auf dem Boden zur Handlung auf der Ark – und ich hoffe auf etwas mehr grey moral. Davon gab es schon viel, und ich bin sehr gespannt, auf welche Figuren sich das noch erstrecken wird. 

Ich freue mich auf mehr dreidimensionale Darstellung dieser Apokalypse – und vielleicht irgendwann noch ein paar Bilder oder mehr Infos darüber, wie genau es dazu kam. Aber ich kann mir auch vorstellen, dass sie damit noch ein paar Staffeln warten. Immerhin eignet sich so etwas viel zu gut für eine „full circle“-Effekt in Bezug auf die Handlung der gesamten Serie (Battlestar Galactica, I’m looking at you!).
Auf jeden Fall: Ich bin gespannt.

### Weiterlesen 
(weitere Eindrücke nach den jeweiligen Staffeln werden folgen)

*****

CNs für die Serie gibt es [hier](https://www.doesthedogdie.com/media/13483) und [hier](https://www.unconsentingmedia.org/items/973).
