---
title:  'Alerta!'
subtitle: 'Ein praktischer Einstieg in den Antifaschismus vom Sofa aus und draußen'
date: 2018-09-22
tags: ["Politik"]
bigimg: [{src: "/img/Antifa_Header.jpg", desc: 'Flagge der Antifa, vor Bücherregal'}, {src: "/img/Antifa.jpg", desc: 'Flagge der Antifa'}]
---

[Content Notice: Nazis]
*****

*Dieser Artikel basiert zu Teilen auf der Session von Mareike ([Blog](https://www.crowandkraken.de/), [Twitter](https://twitter.com/CrowandKraken)) auf dem Literaturcamp Hamburg.*

Was sagt euch eigentlich der Begriff Antifaschismus? Gerade gerät er wieder sehr in Misskredit, wird mit sinnloser Gewalt oder zielloser Zerstörungswut gleichgesetzt. Aber tatsächlich steckt im Antifaschismus genau das, was der Begriff aussagt: Eine Gegenbewegung zum Faschismus, die diesen aufhalten und bekämpfen will. Wer in den letzten Jahren nicht völlig die Augen verschlossen hat, hat den Aufstieg der rechten Bewegungen und des aufkommenden Faschismus verfolgt. Damit ist eine antifaschistische Bewegung genau das, was wir brauchen!

Und, ihr habt langsam einen Plan und wollt nicht nur noch darüber reden, dass man „mal was gegen die AfD tun sollte“, sondern auch mal wirklich etwas tun? Der Einstieg in den Antifaschismus ist tatsächlich sehr einfach. Es gibt nicht „die“ Antifa, keine übergeordnete Organisation oder Sonstiges. Ihr könnt euch mehr oder weniger offiziellen Gruppen anschließen (die ich hier gar nicht alle aufzählen kann, aber da ist tatsächlich eine sichere Online-Suche ein guter Anfang) – aber auch allein und im Zusammenhang aller, die etwas tun, könnt ihr Dinge bewegen!

## Wie kann ich mich antifaschistisch einsetzen?

Es gibt eine große Anzahl an Möglichkeiten, sich antifaschistisch einzusetzen. Zwischen dem Blockieren von Nazidemos und dem Teilen von Tweets scheinen Welten zu liegen, alle Beteiligten tun aber ihren Teil dazu, den aufstrebenden Faschismus zu bekämpfen. Die meisten Menschen haben Schwierigkeiten mit manchen Dingen – ob es jetzt schnell rennen oder gut formulieren ist – und so sollten auch alles das tun, was sie können. Gerade zu körperlicher Aktion sind viele Menschen nicht in der Lage, deshalb lasst euch nicht einreden, dass ihr dann „weniger“ tut!

Aber was genau sind denn jetzt Möglichkeiten zum Einsatz?

### Antifaschismus auf Demos
Die wohl bekannteste Art des Antifaschismus findet meistens auf Demonstrationen statt. Ihr könnt zu angemeldeten Demonstrationen gegen Nazis, Rechts und die AfD gehen, selbst welche organisieren und anmelden oder sie unterstützen – zum Beispiel mit Hilfe beim Auf- und Abbau oder mit dem Anbieten von Schlafplätzen. Die genauen Aktionen auf Demos reichen dabei vom Präsenz zeigen, Schwenken von Bannern und dem gemeinsamen Rufen von Slogans (einen davon referenziere ich in der Überschrift) über [ab hier bewegen wir uns in Grauzonen, die ich mit aufzählen möchte, zu denen ich euch aber explizit nicht anstiften möchte] den Einsatz von Pyrotechnik bis hin zu aktiven Blockaden von Nazis (Durchbrechen der trennenden Polizeiketten und Laufen auf die Route, sodass die Nazidemo gestoppt wird und ein anderer Weg gefunden werden muss – findet sich keiner, wird die Demo meist abgebrochen).

Bei Demonstrationen (und besonders bei Blockaden) finden schon manchmal Handgemenge statt. Es ist wichtig, sich vorbereitet zu haben, am besten mit Demotraining (findet ihr oft bei Gruppen in eurer Stadt). Dort findet ihr auch eine Bezugsgruppe, wenn ihr nicht eure Freund\*innen mitnehmen wollt – was auch eine Option ist, aber ausschließlich Unerfahrene bilden keine gute, sichere Gruppe. Bezugsgruppen helfen euch beim Orientieren. Ihr bewegt euch auch zusammen, entscheidet darüber, ob ihr euch bei Ausschreitungen hineinstürzen oder wegbewegen wollt, über Vermummung oder das Ablassen davon – und seid füreinander da, wenn jemand festgenommen wird oder verschwindet (was in den Massen großer Demos und ohne Mobilfunknetz – nehmt bitte ohnehin nur Demohandys mit – schon mal passieren kann). Spätestens nach einer Festnahme werden auch Nachfragen bei der Polizei und Weitergabe von Informationen an andere oft von Menschen in der Bezugsgruppe geregelt. Und allein die Möglichkeit, die persönlichen Daten der fehlenden Person zu haben, kann bei einer Nachfrage bei der Polizei schon den Unterschied zwischen weiterer Ungewissheit oder den wichtigen Informationen über den Verbleib der Person machen. Was bei Festnahmen zu beachten ist, ist eine ganz andere Geschichte – hier empfehle ich die Website und Flyer der [Roten Hilfe](https://www.rote-hilfe.de/) (übrigens eine wunderbare Organisation, deren Vorstellung hier allerdings den Rahmen sprengen würde).

### Antifaschismus im Viertel
Eine andere Art, aber auch sehr wichtige Arbeit im Antifaschismus ist die aktive Arbeit im Viertel und in eurer Umgebung. Ihr habt sicher schon einmal Sticker gesehen – sowohl die mit Slogans der AfD oder ähnlichen Tönen, als auch welche mit der Flagge im Header oder mit Slogans wie „Hier wurde Nazi-Propaganda überklebt“ (einer meiner liebsten Slogans, weil er einfach überall passt). Auch hier bewegen sich Menschen, die „stickern“ nicht völlig im legalen Bereich, stark verfolgt wird es aber auch nicht. Sticker bestellen (natürlich nur, um sie an eure Freund\*innen für deren Laptops zu verteilen) könnt ihr euch auf verschiedenen Websites bestellen. Ich empfehle für den Schutz eurer Daten (die Motive machen euch für Nazis interessant) [DirAction](https://diraction.org/) und für eigene Designs [WirMachenDruck](https://www.wir-machen-druck.de/).  

Eine weitere Möglichkeit, das Viertel weniger rechts zu gestalten, besteht im Abreißen der Sticker von Nazis. Dabei müsst ihr allerdings aufpassen, denn sie verstecken oft Rasierklingen unter den Stickern (leider keine Panikmache von meiner Seite – verwendet Werkzeug oder überklebt die Sticker).  
Wie Sticker von Nazis gehören inzwischen auch Plakate der AfD (und manchmal auch der NPD) zum Bild vieler Viertel. Auch hier werden manche Antifaschist\*innen aktiv [begehen dabei allerdings auch Ordnungswidrigkeiten, die ich hier nur für die Liste von Einsatzbereichen der Antifa aufzähle]. Manche Aktivist\*innen reißen rechte Plakate ab, indem sie die Kabelbinder an den Seiten durchtrennen, andere stickern darauf oder verwenden Farbbeutel und Spraydosen. Gerade dafür haben viele Antifaschist\*innen auch ein paar größere Sticker herumliegen.  
Völlig legal und vielleicht auch etwas für euch ist übrigens der Einsatz von Sprühkreide. Solltet ihr dabei tatsächlich Ärger mit der Polizei bekommen, könnt ihr jederzeit einfach Wasser darüber auskippen und die Meinungsverschiedenheit hat sich erledigt. Aber wenn es nicht regnet, ist ein farbiges Motiv gegen Nazis doch immer wieder nett!

### Antifaschismus zu Hause
Der Schritt direkt ins eigene Umfeld – ja, auch das kann antifaschistische Arbeit gebrauchen! Und dabei müsst ihr eigentlich nur das tun, was ihr sonst auch tut. Hört einfach mal genauer zu. Es gibt eigentlich keine Familie, in der nicht mal ein Kommentar fällt, der dann mit „war ja nur so gesagt“ zurückgenommen wird, aber halt doch rassistisch war. Kaum eine Clique (es muss ja nicht die engste sein – auch Bekannte an der Uni zählen), in der nicht mal über die AfD gesprochen wird und irgendjemand davon anfängt, dass „die ja auch ihre Berechtigung haben“. Wehrt euch. Akzeptiert solche Sprache nicht einfach, sondern weist Leute darauf hin, was sie da eigentlich sagen (manchmal sind sie davon sogar tatsächlich selbst erschrocken und beschimpfen euch vielleicht, aber hören auf). Ihr seid fast nie allein in solchen Situationen. Es muss nur jemand anfangen, diskriminierendes Verhalten und rechtes Gedankengut als genau das zu bezeichnen. Dann werden meistens auch ein paar Leute mit euch aufhorchen. Und dass ihr dem einen Verwandten, der klar die AfD wählt, immer wieder wiedersprechen könnt, auch wenn dabei „nur die anderen mehr Argumente dagegen bekommen“ und es sich trotzdem lohnt, ist klar, oder?

### Antifaschismus von der Couch
Ihr habt Internet? Super! Ihr seid in sozialen Netzwerken unterwegs? Noch besser! Antifaschistische Arbeit war noch nie einfacher. Positioniert euch – Wer sich nicht zum Rechtsruck und zu rechter Gewalt positioniert, kann sich das leisten, und hat die Pflicht, es dann für alle Betroffenen zu tun! Schaut über eure Bubbles hinaus. Folgt PoC, teilt ihre Erlebnisse, gebt ihnen eure Plattform. So tragt ihr dazu bei, dass die Menschen gehört werden, die ansonsten im Diskurs gern übersehen werden. Retweetet auch Betroffene von Gewalt durch Nazis, von Rassismus und Polizeigewalt, Transmisogynie, Homophobie und alle anderen Marginalisierten – und sorgt dafür, dass ihre Erlebnisse bekannt werden, auf die Strukturen dahinter aufmerksam gemacht wird und die Taten von Nazis nicht untergehen. Und wenn dann doch Nazis unterwegs sind – und die tauchen schneller in eurer Bubble auf, als ihr „Antifaschismus“ sagen könnt: meldet sie. Meldet sie, teilt die Kommentare mit Meldeempfehlung, denn mehr Meldungen erhöhen die Chance auf Sperrung. Solidarisiert euch mit Betroffenen von beleidigenden Kommentaren, schreibt anderes über sie und lasst die Nazis nicht ganze Hashtags erobern.  

Von der Couch aus könnt ihr übrigens auch eure Privilegien reflektieren. Seid ihr weiß? Denkt mal darüber nach, was anders sein könnte und würde, wärt ihr PoC. Seid ihr cis? Dann solltet ihr euch überlegen, wie viel einfacher als trans Personen ihr es habt. Ich könnte diese Liste darauf erweitern, ob ihr zum Beispiel heterosexuell, abled, neurotypisch, bürgerlich-reich oder akademisch geprägt seid. Werdet euch darüber klar, dass ihr einfach ein paar Vorteile im Leben hattet, die andere Personen nicht hatten, haben oder in dieser Gesellschaft haben werden – es ist eure Pflicht, für diese marginalisierten Menschen zu kämpfen. Weil man euch, besonders als weiße Personen, im Zweifel eher sehen und zuhören wird (Das gilt auch für die Polizei! Wenn ihr Zeug*\innen von Übergriffen werdet, bietet Hilfe an, wenn die Polizei gerufen wird (fragt die Person vorher!) Sagt aus, lasst eure Daten für weitere Unterstützung dort, seid für die betroffene Person da). Achtet im Alltag darauf, teilt die Stimmen Marginalisierter, werbt für sie als Vortragende und nicht für den nächsten weißen cis Mann. 

### Antifaschismus für Bloggende
Ihr seid nicht nur auf Social Media unterwegs, sondern habt auch einen Blog? Awesome! Ihr könnt noch mehr antifaschistische Arbeit leisten! Abgesehen von allen Möglichkeiten auf Social Media steht euch noch eine größere Plattform für längere Beiträge offen. Warum also nicht auf die Notwendigkeit von Antifaschismus hinweisen (so wie ich es gerade tue)? Anleitungen für Plakate teilen, Termine von Demos in den nächsten Monaten oder Artikel zu aktuellen Ausschreitungen von Nazis? Und auch hier könnt ihr Marginalisierte sprechen lassen. Führt Interviews, lasst Gastbeiträge zu, öffnet euren Kanon von Büchern oder Filmen über das hinaus, was ohnehin nur von weißen cis Menschen erzählt, die heterosexuell, neurotypisch und abled sind. Selbst wenn ihr glaubt, niemand liest eure Texte – ihr habt viel größere Macht, als ihr denkt. 
Nutzt diese Macht. Tut, was euch liegt und was ihr könnt. Zeigen wir es dem Rechtsruck und den Nazis. 

Seid ihr dabei?

## Weiterlesen
(empfohlene Bücher sind aus der Litcamp-Session übernommen)  
Holger Kulick: Das Buch gegen Nazis  
Tupoka Ogette: Exit Racism  
Noah Sow: Deutschland Schwarz-Weiß  
Anatol Stefanowitsch: Eine Frage der Moral  
Mohamed Amjahid: Unter Weißen  

[Flyer](https://www.rote-hilfe.de/downloads/category/3-rechtshilfe-a-was-tun-wenns-brennt?download=2:was-tun-wenns-brennt-rechtshilfetipps-ausgabe-2011) der Roten Hilfe zum Verhalten bei Festnahmen o.Ä.

