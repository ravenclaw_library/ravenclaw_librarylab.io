---
title: "[Rezension] David Petersen: Mouse Guard"
date: 2019-01-24
tags: ["Comics", "Rezensionen"]
bigimg: [{src: "/img/Mouse-Guard.jpg", desc: 'Cover von David Peterson: Mouse Guard (Band 1), vor einem Bücherregal'}]
---

Heute möchte ich eine Comicreihe vorstellen. Vielleicht bin ich nicht ganz unvoreingenommen, da ich in diesem Universum auch eine Weile begeistert Pen&Paper gespielt habe, aber eigentlich ist das ja eher ein gutes Zeichen. 

Mouse Guard erzählt von einer Zivilisation von Mäusen, die sich innerhalb eines Gebiets verschiedene Städte aufgebaut haben und dort leben, untereinander handeln und dazwischen hin und her reisen. Da das Gebiet besonders für Mäuse recht groß ist und sich nicht in einem einzigen abgeschlossenen Ort befindet, gibt es die Wache, die Mouse Guard. Als eine Art Militär stellt die Wache den Schutz der Zivilbevölkerung sicher, begleitet Mäuse auf ihren Wegen zwischen den Städten und regelt größere Meinungsverschiedenheiten. Aber natürlich wehren sie auch Angreifende ab – das sind zum Beispiel Eichhörnchen oder Wiesel. Für Mäuse sind diese Gegner (für menschliche Lesende) ungewohnt riesig und unglaublich gefährlich, und jeder Kampf wird auf den gezeichneten Seiten zu einer epischen Saga. 

Mit den Wieseln gab es sogar einmal einen Krieg – der liegt völlig in der Vergangenheit, wird aber ständig referenziert und hat Auswirkungen. Auch das hat mir noch einmal die Dreidimensionalität der hier entworfenen Welt bewiesen, und ich habe mich schnell von den Schlachtgeschichten einhüllen lassen. Dass man dabei lesend einer Maus zuhört, was sich eigentlich kaum mit dem klassischen Heldentum verbinden lässt, realisiert man schnell gar nicht mehr (mein häufigster Out-of-Character-Fehler beim Pen&Paper war das Referenzieren auf eine Gruppe Mäuse als Menschen). 

Auch die Gesellschaft ist toll entwickelt. Die Städte sind alle unterschiedlich, die Wache hat eine spannende Struktur – und die Bände haben genau das getan, was ich mir von mittelalterlichen Welten immer wünsche: das Beste aus dem Sagenpotential dieser Epoche und dem aufgeklärteren Gesellschaftsbild unserer Zeit. Eine weibliche Maus führt die Wache, und auch innerhalb der Wache laufen weibliche Mäuse einfach mit. Es wird nicht sichtlich unterschieden – was ich so unbetont noch in kaum einer mittelalterlichen Welt gesehen habe, und was mich sowohl beim Lesen als auch beim Entwickeln meiner Pen&Paper-Figur sehr glücklich gemacht hat. 

Und die Artworks sind so unglaublich schön! Die Seiten der Bücher sind mit verschieden großen Bildern gefüllt, die alle bunt, aber nicht schreiend ablenkend gezeichnet sind. Durch Fellfarben, Umhänge, aber auch Gesichtszüge sind die Mäuse gut zu unterscheiden und der Lesefluss wird nicht gestört. Die Hintergründe sind immer schön ausgestaltet, die Buchrücken sind wunderbar und der Text von gelesenen Pergamenten oder erzählten Sagen wird immer noch einmal anders dargestellt, sodass sich gleich die lesende Stimme im Kopf ein bisschen verschiebt.

Die Comics sind wirklich ein echtes Juwel, und ich kann sie nur jeder Person ans Herz legen, die Comics mag oder mal einen ausprobieren möchte! 
Das Pen&Paper-System ist übrigens auch sehr gut entwickelt, kommt mit Bögen für die Figuren und toll gestaltetem Zubehör! Falls ihr also eine eher von Gameplay getriebene Pen&Paper-Welt sucht, in der nicht andauernd nur gewürfelt wird, kann ich euch auch hier Mouse Guard sehr ans Herz legen. Ich hatte großen Spaß in dieser Welt – auch wenn durch Würfelpech fast die gesamte Gruppe im allerersten Kampf gegen ein Eichhörnchen draufgegangen wäre. 
