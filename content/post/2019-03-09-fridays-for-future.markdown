---
layout: post
title:  'Climate Wars: The New Generation'
subtitle: 'Als Studi bei #fridaysforfuture Berlin'
date: 2019-03-09
tags: ["Veranstaltungen", "Politik"]
bigimg: [{src: "/img/climate-justice-now.jpg", desc: 'Demo-Plakat: "Climate Justice Now!"'}]
---

### Der Eindruck

Ich habe mich von Nathan zu den Schulstreiks fürs Klima in Berlin mitschleifen lassen, zu Fridays for Future. Zunächst zweifelnd, nach einer langen Zeit ohne bürgerliche Kundgebung, und ja, leider auch angesichts der Organisation durch Schüler\*innen. Außerdem zögerlich durch die zu erwartende Kälte. Aber es half ja nichts, und tatsächlich hatte ich da gerade keine Präsenzveranstaltung, und irgendwann war der Topf mit Ausreden leer. Immerhin sehe ich so jung aus, dass ich davon ausgehen konnte, den meisten Menschen auf der Demo einfach gar nicht aufzufallen.

Ich fand eine Serie an Überraschungen vor. Es waren wirklich viele Menschen da, und bestimmt 90 Prozent sahen aus, als wären sie noch schulpflichtig. Dabei war es kalt! Und ja, Berlin hat einige Schulen und diese knapp 50 Menschen waren im Verhältnis gar nicht so viele, aber ich war beeindruckt. Das wurde nur noch verstärkt, als zum offiziellen Beginn der Veranstaltung noch mehr Teilnehmende auftauchten – von Seiten der Hauptorga hieß es später, dass 200 auf der folgenden Demo durch die Innenstadt geschätzt wurden. 

### Die Orga

Und die Orga! Eine Hand voll super engagierter Schüler\*innen – die sich sowohl in der Bundes-Orga (ich erfuhr erst auf der ersten Demo, dass es so etwas gibt) als auch in der Berliner Orga große Teile ihrer Freizeit für Fridays for Future investieren. Diese Zeit fließt in Orga-Meetings, Angebote zum gemeinsamen Gestalten von Plakaten, Kommunikation im Inneren und mit den Behörden zur Anmeldung und Regelung der Kundgebungen und Demonstrationen. Außerdem gibt es Austausche zu regeln, denn oft besuchen Orgas aus anderen Städten die Veranstaltungen – bei meinem ersten Besuch war witzigerweise jemand aus der Nähe von Kiel da, wo ich aufgewachsen bin. Schnell kam auch Veranstaltungs-Management zu den Aufgaben der Orga dazu: Die Schüler\*innen in der Berliner Orga haben bereits Termine auf großen Podien und in Gremien im Senat und im Bundestag. Dass die Schüler\*innen in der Orga deshalb so oft in ihren Reden betonen, dass die tatsächlich bestreikte Schulzeit, nämlich die Zeit der Veranstaltungen selber, die kleinste Zeit ihres Engagements ist, finde ich mehr als angebracht. Und abgesehen davon, dass ich die Streiks ohnehin aktiv unterstütze, sehe ich in dieser Realität auch ein gut sichtbares Argument gegen die Argumente der Erwachsenen, die nur „verpasste Schulzeit“ sehen. (Ich glaube, die meisten von uns haben auch, wie ich, die Erfahrung gemacht, dass freitags ohnehin statistisch gesehen die am wenigsten ernsthaften Schultage sind.)

Wie die Berliner Orga das alles in wenigen Wochen auf die Beine gestellt hat und durchzieht, ist wirklich beeindruckend – gerade als älterer Mensch, der dann in den hinteren Reihen steht und sich freut, dass die Jugend anscheinend doch nicht so unpolitisch ist, wie lange auch von mir befürchtet. Die Orga leitet jedes Mal in die Veranstaltung ein, hält Reden, moderiert andere Beiträge und kündigt Veranstaltungen und Aktionen an, und moderiert das Open Mic – in diesem Teil können sich dann alle Interessierten zu Wort melden. Gerade da sind auch oft unsichere und sehr junge Redende zu sehen. Das hat mich bei meinem ersten Besuch unglaublich gefreut, und tatsächlich ist es auch jedes Mal wieder so. 

### Die Stimmung

Das Besondere bei Fridays for Future ist für mich die Stimmung. So viele junge Menschen, und gerade auch vorher nicht aktiv politisierte Jugendliche, die sich damit gegenseitig anstecken, haben eine ganz andere Stimmung auf ihren Veranstaltungen, als ich sie von meinen Klimademonstrationen normalerweise gewohnt bin. Und ja, am Anfang fand ich die Verbindung mit den neuesten Charts etwas gewöhnungsbedürftig, gerade auch wegen teilweise problematischer Texte. Tatsächlich sind diese paar Lieder bei den nächsten Veranstaltungen dann aber nicht wieder gespielt worden – und abgesehen davon ist es einfach schön zu sehen, wie Jugendliche ihre eigene Kultur bauen, fern von Klischees darüber, dass Klimaschutz heißen muss, sich gegenseitig zum Verzicht zu zwingen. Ja, es gibt immer mal wieder coole Inputs zu vegetarischer oder veganer Ernährung, und alle wollen den ÖPNV fördern und rufen zum Abschaffen von eigenen Autos auf (und ja, das ist in Berlin halt auch einfacher als anderswo). Gleichzeitig sind halt auch mal Einweg-Plastikbecher zu sehen oder einzeln verpackte Sandwiches. Manchmal ergibt sich daraus auch sichtbar ein Gespräch, und es sieht zumindest für mich immer nach guten Diskussionen aus. 

Ich gehe ein Stück weit auch aus Begeisterungen für die Plakate hin. Viele sind unter dem Hashtag auf Instagram zu finden – meine Lieblingsmotive bisher waren „The Earth is hotter than young Leonardo DiCaprio“, „The Earth is hotter than my boyfriend and this is not okay” und “Chuck Norris macht Kohlekraft nachhaltig“. Dieses Meme-Format, teilweise auch in entsprechender Bild-Schrift-Sprache zeigt einfach, wie sehr die Jugendlichen neben dem Aufnehmen von Sprüchen aus den 80ern auf großen Bannern auch ihre eigene Kultur mit diesem Interesse verbinden. 

### Das Fazit

Fridays for Future macht mir unglaublich Hoffnung. Und deshalb werde ich weiterhin versuchen, möglichst fast jeden Freitag dabei zu sein. Vielleicht geht ihr auch mal bei eurer lokalen Veranstaltung vorbei?

(So jung sehe ich übrigens anscheinend gar nicht aus. Zwei junge Personen kamen begeistert auf mich zu: „Entschuldigung, könnten Sie vielleicht ein Foto von uns für Instagram machen?“)

### Weiterlesen

[Fridays for Future Deutschland](https://fridaysforfuture.de/)