---
title: Bücherregal
bigimg: [{src: "/img/Mouse-Guard.jpg", desc: 'Bücher in einem Bücherregal'}]
comments: false
---

Aktuelle Lektüre und Lesefortschritt auf [Goodreads](https://www.goodreads.com/user/show/81716772-anna-ravenclaw-library)

### Bücher 2018-2019

[Goodreads-Challenge 2019](https://www.goodreads.com/user_challenges/15917249)  
[Goodreads-Challenge 2018](https://www.goodreads.com/user_challenges/13078356)

Tomi Adeyemi: Children of Blood and Bone  
Becky Albertalli: Leah on the Offbeat  
Isabelle Bendig: Aus der Asche  
Christoper L. Bennett: Star Trek: Rise of the Federation 4 - Prinzipientreue [abgebrochen]  
Serenity Amber Carter: Mapmaker Malique - Iriliquium  
Andrew Chapman: Das Duell der Piraten  
[Stephen Chbosky: The Perks of being a Wallflower](https://ravenclaw-library.berlin/tischderbibliothekarin/2018/09/05/the-perks-of-being-a-wallflower.html)  
Joan Darque: Urban Shadows  
Jenny Erpenbeck: Heimsuchung  
[Raymond Feist: Der Lehrling des Magiers](https://ravenclaw-library.berlin/tischderbibliothekarin/2018/09/13/der-lehrling-des-magiers.html)  
Raymond Feist: Der verwaiste Thron  
Raymond Feist: Die Gilde des Todes  
Raymond Feist: Dunkel über Sethanon  
Daniel Kehlmann: Ruhm  
Yoshida Kenkō: Draußen in der Stille  
Jack Kerouac: Piers of the Homeless Night  
[R.A. Prum, S.C. Kreuer: Der Schleier der Welt](https://ravenclaw-library.berlin/tischderbibliothekarin/2018/11/11/der-schleier-der-welt.html)  
Martin Luther King, Jr.: Letter from Birmingham Jail  
V.T. Melbow: Lory-X  
Christian Milkus: Das Feuer in mir  
Madeline Miller: The Song of Achilles  
Alice Munro: Tricks  
Nagai/Uno/Akutagawa: Three Japanese Short Stories  
Yōko Ogawa: Das Geheimnis der Eulerschen Formel  
Trevor Noah: Born a Crime  
[Nnedi Okorafor: Binti](https://ravenclaw-library.berlin/tischderbibliothekarin/2018/12/05/binti.html)  
[Nnedi Okorafor: Who Fears Death](https://ravenclaw-library.berlin/tischderbibliothekarin/2018/12/03/who-fears-death.html)  
Edgar Allan Poe: The complete Tales and Poems  
Rick Riordan: Magnus Chase - The Ship of the Dead  
[Kristen Roupenian: Cat Person](https://ravenclaw-library.berlin/post/2019-03-18-cat-person/) [abgebrochen]  
Rainbow Rowell: Carry On  
[Rainbow Rowell: Fangirl](https://ravenclaw-library.berlin/tischderbibliothekarin/2018/10/10/fangirl.html)  
[Annina Safran: Der Spiegelwächter](https://ravenclaw-library.berlin/schreibtischderbibliothekarin/2018/10/23/der-spiegelw%C3%A4chter.html) [abgebrochen]  
Andrzej Sapkowski: The Last Wish [abgebrochen]  
Kate Saunders: The Little Secret  
Neil Shusterman: Scythe - Die Hüter des Todes  
Neil Shusterman: Scythe - Der Zorn der Gerechten  
[Semiya Şimşek: Schmerzliche Heimat. Deutschland und der Mord an meinem Vater](https://ravenclaw-library.berlin/politischeabteilung,/schreibtischderbibliothekarin/2018/10/18/schmerzliche-heimat.html)  
Joachim Sohn: Die Zeitagenten  
Zadie Smith: Swing Time  
Benjamin Spang: Blut gegen Blut  
[Maggie Stiefvater: The Scorpio Races](https://ravenclaw-library.berlin/tischderbibliothekarin/2018/08/22/the-scorpio-races.html)  
[Liv Strömquist: Der Ursprung der Liebe](https://ravenclaw-library.berlin/post/2018-12-17-der-ursprung-der-liebe/)  
Angie Thomas: The Hate You Give  
[J.R.R. Tolkien: Tales from the Perilous Realm](https://ravenclaw-library.berlin/schreibtischderbibliothekarin/2018/08/27/tales-from-the-perilous-realm.html)  
[Anna Weydt: Das steinerne Schloss](https://ravenclaw-library.berlin/schreibtischderbibliothekarin/2018/10/17/das-steinerne-schloss.html)  
Natascha Wodin: Sie kam aus Mariupol  
Natascha Wodin: Irgendwo in diesem Dunkel  

#### Sachbücher
Stefan Aust, Dirk Laabs: Heimatschutz. Der Staat und die Mordserie des NSU  
[Antonia von der Behrens (Hrsg.): Kein Schlusswort. Nazi-Terror, Sicherheitsbehörden, Unterstützernetzwerk. Plädoyers im NSU-Prozess](https://ravenclaw-library.berlin/post/2018-11-07-kein-schlusswort/)  
Amina Bile, Sofia Nesrine Srour, Nancy Herz: Schamlos  
Deniz Yücel: Wir sind ja nicht zum Spaß hier  

#### Comics
[David Petersen: Mouse Guard - Fall 1152](https://ravenclaw-library.berlin/post/2019-01-24-mouse-guard/)  
David Petersen: Mouse Guard - Winter 1152  
David Petersen: Mouse Guard - The Black Axe  
David Petersen: Mouse Guard - Legends of the Guard  
