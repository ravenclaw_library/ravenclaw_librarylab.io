---
title: Code of Conduct
bigimg: [{src: "/img/Mouse-Guard.jpg", desc: 'Bücher in einem Bücherregal'}]
comments: false
---

Um diesen Blog als okayen und möglichst frei von beschissenen Momenten bleibenden Space zu erhalten, gelten hier ein paar Regeln.

Ich habe nicht das geringste Verständnis für diverse Formen von Diskriminierung in Sprache, Handlung und vermittelter Haltung, nicht für [Homo-](https://queer-lexikon.net/wp/2017/06/15/homofeindlichkeit/) oder [Bifeindlichkeit]( https://queer-lexikon.net/wp/2017/06/15/bifeindlichkeit/), Fremdenfeindlichkeit oder Rassismus, [Trans-](https://queer-lexikon.net/wp/trans/), [Inter-](https://queer-lexikon.net/wp/inter/) oder [Queerfeindlichkeit]( https://queer-lexikon.net/wp/2017/06/08/queer/), Sexismus oder [Ableismus]( http://www.genderinstitut-bremen.de/glossar/ableismus.html) sowie [Bodyshaming]( https://marshmallow-maedchen.de/blog/body-positivity/was-ist-body-shaming-definition/) (ich bilde Teile dieser Begriffe mit „-feindlichkeit“ statt „-phobie“, um die Phänomene von wirklich unfreiwilligen, psychisch bedingten Phobien abzugrenzen – hier eine [Erklärung](https://geschlechtsneutral.wordpress.com/2018/05/15/es-ist-gewalt-nicht-angst/comment-page-1/) dazu. Ich habe ebenfalls wenig Verständnis dafür, wenn Menschen diese Begriffe mit „-phobie“ bilden oder sich gar beschweren, wenn sie kein Verständnis dafür haben, wenn ich sie auf so einen Fehler hinweise.) Dieser Blog ist ein Blog, der möglichst ohne all diesen diskriminierenden Scheiß auskommt.

Die Artikel auf diesem Blog werden außerhalb des generischen Maskulinums geschrieben und ich verbitte mir Beschwerden darüber (freue mich aber über Anregungen dazu, wie ich das noch besser machen kann oder zu einem nicht ordentlich ausgedrückten Begriff). Auf diesem Blog versuche ich auch, [Mono-](https://queer-lexikon.net/wp/tag/polyamouroes/) und [Heteronormativität](http://queer-lexikon.net/doku.php?id=queer:heteronormativitaet) abzubauen, und ich werde ebenfalls nicht mit Menschen diskutieren, die der Meinung sind, das wäre „anstrengend“.

Verstöße gegen diese Regeln sowie die ausdrückliche Beschwerde darüber in Kommentaren auf diesem Blog oder Interaktionen mit diesem Blog verbundenen oder assoziierten Mailpostfächern, Social-Media-Konten oder in Form von diskriminierenden oder sich über diese Regeln beschwerenden Äußerungen im realen Leben oder auf den eigenen Kanälen werden im besten Fall zu direkten Callouts führen, können aber auch zu öffentlichen Callouts und unbegründeten Blocks führen. 

Ich weiß, dass die Bubble der hier interessierten Menschen voll mit tollen Leuten ist. Lasst uns einfach coole Menschen bleiben, damit auch alle diese Leute hier einen Platz finden können, um diesen coolen Interessen nachzugehen. 
