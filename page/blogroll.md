---
title: Blogroll
bigimg: [{src: "/img/Mouse-Guard.jpg", desc: 'Bücher in einem Bücherregal'}]
comments: false
---

Hier sind einige tolle Blogs, die ich euch ans Herz legen möchte:


- [Skepsiswerke](https://skepsiswerke.de/) 

- [Piranhapudel](https://piranhapudel.de/) 

- [Alpakawolken](https://alpakawolken.de/)

- [Geekgeflüster](https://geekgefluester.de/)

- [Feminismus oder Schlägerei](https://feminismus-oder-schlaegerei.de/)





